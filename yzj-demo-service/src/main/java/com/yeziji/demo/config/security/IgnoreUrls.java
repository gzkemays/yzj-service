package com.yeziji.demo.config.security;

import com.yeziji.security.common.logger.LoggerIgnoreInfo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashSet;
import java.util.Set;

/**
 * 默认放行的地址
 *
 * @author hwy
 * @since 2023/11/12 22:30
 **/
@Data
@ConfigurationProperties(prefix = "yzj.demo.ignore")
public class IgnoreUrls {
    /**
     * 默认放行的地址
     */
    private Set<String> auths = new HashSet<>();

    /**
     * 默认忽略日志的地址
     */
    private Set<LoggerIgnoreInfo> logs = new HashSet<>();

    /**
     * 默认[需要]打印的 SQL 日志
     */
    private Set<String> sqlLogs = new HashSet<>();

    /**
     * 默认忽略监听日志
     */
    private Set<String> monitorIp = new HashSet<>();
}
