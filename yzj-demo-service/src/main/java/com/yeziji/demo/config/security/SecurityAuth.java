package com.yeziji.demo.config.security;

import com.yeziji.common.Platform;
import com.yeziji.common.business.system.dto.SystemRoleReleasesUrlDTO;
import com.yeziji.common.business.system.service.SystemUserRoleRelationService;
import com.yeziji.security.common.logger.LoggerIgnoreInfo;
import com.yeziji.security.service.DynamicSecurityAuth;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 论坛忽略的地址
 *
 * @author hwy
 * @since 2023/11/12 20:46
 **/
@Service
public class SecurityAuth implements DynamicSecurityAuth {
    @Resource
    private IgnoreUrls ignoreUrls;
    @Resource
    private SystemUserRoleRelationService userRoleRelationService;

    @Override
    public Platform getPlatform() {
        return Platform.builder()
                .platformName("测试案例")
                .platformLocale(Locale.CHINA)
                .build();
    }

    @Override
    public Set<String> getIgnoreUrls() {
        return ignoreUrls.getAuths();
    }

    @Override
    public Set<LoggerIgnoreInfo> getIgnoreLogsUrls() {
        return ignoreUrls.getLogs();
    }

    @Override
    public Map<String, List<ConfigAttribute>> getPathRoleMap() {
        List<SystemRoleReleasesUrlDTO> roleReleaseUrls = userRoleRelationService.listSystemRoleReleaseUrls();

        // spring security normative
        Map<String, List<ConfigAttribute>> resultMap = new HashMap<>();
        for (SystemRoleReleasesUrlDTO roleReleaseUrl : roleReleaseUrls) {
            List<String> releaseUrls = roleReleaseUrl.getReleaseUrls();
            for (String releaseUrl : releaseUrls) {
                resultMap.computeIfAbsent(releaseUrl, k -> new ArrayList<>()).add(new SecurityConfig(roleReleaseUrl.getRoleName()));
            }
        }
        return resultMap;
    }
}