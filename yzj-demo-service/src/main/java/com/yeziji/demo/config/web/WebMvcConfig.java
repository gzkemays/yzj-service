package com.yeziji.demo.config.web;

import com.yeziji.config.support.resolver.RequestSingleBodyHandlerMethodArgumentResolver;
import com.yeziji.dynamicConfig.config.YzjConfigWebMvcConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * web mvc config
 *
 * @author hwy
 * @since 2023/09/02 15:27
 **/
@Configuration
@Import({YzjConfigWebMvcConfig.class})
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        registry
                // 设置允许跨域的路由
                .addMapping("/**")
                // 是否允许证书（cookies）
                .allowCredentials(true)
                // 设置允许的请求头
                .allowedHeaders("*")
                // 设置允许的方法
                .allowedMethods("*")
                // 设置允许跨域请求的域名
                .allowedOriginPatterns("*");
    }

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new RequestSingleBodyHandlerMethodArgumentResolver());
        super.addArgumentResolvers(argumentResolvers);
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 添加默认的静态资源访问路径
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:static/", "classpath:META-IFA/resources/", "classpath:resources/", "classpath:/");
    }
}
