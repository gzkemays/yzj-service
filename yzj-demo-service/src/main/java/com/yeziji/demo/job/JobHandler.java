package com.yeziji.demo.job;

import com.yeziji.job.annotation.YzjJob;
import com.yeziji.job.annotation.YzjJobBean;
import com.yeziji.job.common.log.YzjJobLogger;
import lombok.extern.slf4j.Slf4j;

/**
 * 全部定时任务执行的地方
 *
 * @author hwy
 * @since 2024/10/24 10:10
 **/
@Slf4j
@YzjJobBean
public class JobHandler {
    @YzjJob("demo")
    public void demo(String params) {
        YzjJobLogger.info("执行定时任务：{}", params);
        log.info("执行 ing...");
        YzjJobLogger.info("执行定时任务完毕 !!!");
    }
}
