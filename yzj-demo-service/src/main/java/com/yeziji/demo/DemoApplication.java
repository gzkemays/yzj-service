package com.yeziji.demo;

import com.yeziji.annotation.EnableAutowiredPermission;
import com.yeziji.job.annotation.EnabledAutoRegisterYzjJob;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author hwy
 * @since 2024/10/24 11:56
 **/
@SpringBootApplication
@EnableAutowiredPermission
@EnableConfigurationProperties
@EnableAspectJAutoProxy(exposeProxy = true)
@EnabledAutoRegisterYzjJob(basePackages = {
        "com.yeziji.demo.job.**"
})
@ConfigurationPropertiesScan(basePackages = {
        "com.yeziji.demo.config"
})
@ComponentScan(basePackages = {
        "com.yeziji.**",
        "com.yeziji.security.**",
        "com.yeziji.config.**",
        "com.yeziji.devops.**"
})
@MapperScan(basePackages = {
        "com.yeziji.*.business.**.mapper",
})
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
