package com.yeziji.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * 服务测试类
 *
 * @author hwy
 * @since 2024/10/24 12:00
 **/
@Slf4j
@ActiveProfiles("dev")
@SpringBootTest(classes = DemoApplication.class)
public class ServiceTests {
    @Before
    public void before() {
        log.info("before something");
    }
}
