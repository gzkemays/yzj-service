package com.yeziji.devops.common;

import com.yeziji.devops.sql.constructor.InsertSqlConstructor;
import com.yeziji.devops.sql.constructor.UpdateSqlConstructor;

/**
 * jdbc 填充处理策略
 *
 * @author hwy
 * @since 2024/07/27 0:11
 **/
public interface JdbcFillHandler {
    /**
     * insert 策略
     *
     * @param insertSqlConstructor insert SQL 構造器
     */
    void insertFill(InsertSqlConstructor insertSqlConstructor);

    /**
     * update 策略
     *
     * @param updateSqlConstructor update SQL 構造器
     */
    void updateFill(UpdateSqlConstructor updateSqlConstructor);
}
