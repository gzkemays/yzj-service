package com.yeziji.devops.common.expcetion;

import com.yeziji.common.CommonErrorEnum;
import com.yeziji.devops.common.msg.SqlBuilderErrorMsg;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * SQL 构建异常
 *
 * @author hwy
 * @since 2024/07/31 0:34
 **/
@Getter
@NoArgsConstructor
public class SqlBuilderException extends RuntimeException {
    private static final long serialVersionUID = -3167781692515976239L;

    private SqlBuilderErrorMsg errorEnum;

    public SqlBuilderException(SqlBuilderErrorMsg errorEnum) {
        super(Optional.ofNullable(errorEnum).map(CommonErrorEnum::getLangDesc).orElse("system-error.unknown-exception"));
        this.errorEnum = errorEnum;
    }

    public SqlBuilderException(SqlBuilderErrorMsg errorEnum, String message) {
        super(message);
        this.errorEnum = errorEnum;
    }

    public SqlBuilderException(String message) {
        super(message);
    }

    public SqlBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public SqlBuilderException(Throwable cause) {
        super(cause);
    }

    public SqlBuilderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SqlBuilderException(SqlBuilderErrorMsg errorEnum, Throwable cause) {
        // lightweight exception (fill in stack trace)
        super(errorEnum.getLangDesc(), cause, false, true);
    }
}
