package com.yeziji.devops.common.expcetion;

import com.yeziji.common.CommonErrorEnum;
import com.yeziji.common.CommonErrorMsg;
import com.yeziji.devops.common.msg.DevopsErrorMsg;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * devops 异常
 *
 * @author hwy
 * @since 2024/08/15 18:54
 **/
@Getter
@NoArgsConstructor
public class DevopsException extends RuntimeException {
    private static final long serialVersionUID = 200871000498685665L;

    private DevopsErrorMsg errorEnum;

    public DevopsException(DevopsErrorMsg errorEnum) {
        super(Optional.ofNullable(errorEnum).map(CommonErrorEnum::getLangDesc).orElse(CommonErrorMsg.SYSTEM_UNKNOWN_EXCEPTION.getLangDesc()));
        this.errorEnum = errorEnum;
    }

    public DevopsException(DevopsErrorMsg errorEnum, String message) {
        super(message);
        this.errorEnum = errorEnum;
    }

    public DevopsException(String message) {
        super(message);
    }

    public DevopsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DevopsException(Throwable cause) {
        super(cause);
    }

    public DevopsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DevopsException(DevopsErrorMsg errorEnum, Throwable cause) {
        // lightweight exception (fill in stack trace)
        super(errorEnum.getLangDesc(), cause, false, true);
    }
}
