package com.yeziji.devops.common.expcetion;

import com.yeziji.common.CommonErrorEnum;
import com.yeziji.devops.common.msg.JdbcErrorMsg;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * jdbc 异常
 *
 * @author hwy
 * @since 2024/08/01 21:21
 **/
@Getter
@NoArgsConstructor
public class JdbcException extends RuntimeException {
    private static final long serialVersionUID = -4196868774839699943L;
    private JdbcErrorMsg errorEnum;

    public JdbcException(JdbcErrorMsg errorEnum) {
        super(Optional.ofNullable(errorEnum).map(CommonErrorEnum::getLangDesc).orElse("未知异常"));
        this.errorEnum = errorEnum;
    }

    public JdbcException(JdbcErrorMsg errorEnum, String message) {
        super(message);
        this.errorEnum = errorEnum;
    }

    public JdbcException(String message) {
        super(message);
    }

    public JdbcException(String message, Throwable cause) {
        super(message, cause);
    }

    public JdbcException(Throwable cause) {
        super(cause);
    }

    public JdbcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public JdbcException(JdbcErrorMsg errorEnum, Throwable cause) {
        // lightweight exception (fill in stack trace)
        super(errorEnum.getLangDesc(), cause, false, true);
    }
}
