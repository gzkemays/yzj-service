package com.yeziji.devops.common.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * SQL 构造异常信息
 *
 * @author hwy
 * @since 2024/07/31 0:34
 **/
@Getter
@AllArgsConstructor
public enum SqlBuilderErrorMsg implements CommonErrorEnum {
    NOT_SUPPORT_ENGINE(ERROR, "devops-build-sql-error.not-support-engine", "devops-build-sql-show-error.not-support-engine"),
    INDEX_FIELDS_IS_EMPTY(ERROR, "devops-build-sql-error.index-fields-is-empty", "devops-build-sql-show-error.index-fields-is-empty"),
    TABLE_IS_EMPTY(ERROR, "devops-build-sql-error.table-is-empty", "devops-build-sql-show-error.table-is-empty"),
    EDIT_TYPE_IS_EMPTY(ERROR, "devops-build-sql-error.edit-type-is-empty", "devops-build-sql-show-error.edit-type-is-empty"),
    OPERA_NAME_IS_EMPTY(ERROR, "devops-build-sql-error.opera-name-is-empty", "devops-build-sql-show-error.opera-name-is-empty"),
    DATA_EMPTY(ERROR, "devops-build-sql-error.data-empty", "devops-build-sql-show-error.data-empty"),
    PARAMS_EMPTY(ERROR, "devops-build-sql-error.params-empty", "devops-build-sql-show-error.params-empty"),
    WRAPPER_IS_EMPTY(ERROR, "devops-build-sql-error.wrapper-is-empty", "devops-build-sql-show-error.wrapper-is-empty"),
    WRAPPER_IS_ILLEGAL(ERROR, "devops-build-sql-error.wrapper-is-illegal", "devops-build-sql-show-error.wrapper-is-illegal"),
    COLUMN_EMPTY(ERROR, "devops-build-sql-error.column-empty", "devops-build-sql-show-error.column-empty"),
    COLUMN_TYPE_IS_EMPTY(ERROR, "devops-build-sql-error.column-type-is-empty", "devops-build-sql-show-error.column-type-is-empty"),
    VALUES_EMPTY(ERROR, "devops-build-sql-error.values-empty", "devops-build-sql-show-error.values-empty"),
    SIZE_NOT_EQUALS(ERROR, "devops-build-sql-error.size-not-equals", "devops-build-sql-show-error.size-not-equals"),
    NOT_SUPPORT_CONSTRAINT_TYPE(ERROR, "devops-build-sql-error.not-support-constraint-type", "devops-build-sql-show-error.not-support-constraint-type"),
    REFERENCES_TABLE_IS_EMPTY(ERROR, "devops-build-sql-error.references-table-is-empty", "devops-build-sql-show-error.references-table-is-empty"),
    REFERENCES_COLUMN_IS_EMPTY(ERROR, "devops-build-sql-error.references-column-is-empty", "devops-build-sql-show-error.references-column-is-empty"),
    NOT_SUPPORT_CURRENT_INDEX_TYPE(ERROR, "devops-build-sql-error.not-support-current-index-type", "devops-build-sql-show-error.not-support-current-index-type"),
    CONSTRAINT_COLUMN_NAME_IS_EMPTY(ERROR, "devops-build-sql-error.constraint-column-name-is-empty", "devops-build-sql-show-error.constraint-column-name-is-empty"),
    CONSTRAINT_TYPE_IS_EMPTY(ERROR, "devops-build-sql-error.constraint-type-is-empty", "devops-build-sql-show-error.constraint-type-is-empty"),
    FOREIGN_KEY_COLUMN_IS_EMPTY(ERROR, "devops-build-sql-error.foreign-key-column-is-empty", "devops-build-sql-show-error.foreign-key-column-is-empty"),
    NOT_SUPPORT_FOREIGN_RELATION(ERROR, "devops-build-sql-error.not-support-foreign-relation", "devops-build-sql-show-error.not-support-foreign-relation"),
    GET_PROGRAM_EXCEPTION(ERROR, "devops-build-sql-error.get-program-exception", "devops-build-sql-show-error.get-program-exception"),
    ;

    private final int code;
    private final String desc;
    private final String showMsg;
}
