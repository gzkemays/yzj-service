package com.yeziji.devops.common;

/**
 * Sql Builder 构建基础接口
 *
 * @author hwy
 * @since 2024/07/27 0:10
 **/
public interface SqlBuilder {
    /**
     * 执行构建
     *
     * @return {@link String} SQL 字符串信息
     */
    String execute();
}
