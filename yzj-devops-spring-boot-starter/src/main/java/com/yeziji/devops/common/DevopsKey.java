package com.yeziji.devops.common;

import com.yeziji.common.CommonSymbol;

/**
 * devops key
 *
 * @author hwy
 * @since 2024/08/15 19:04
 **/
public interface DevopsKey {
    /**
     * 统一前缀
     */
    String COMMON_PREFIX = "yzj-devops";

    /**
     * redis 统一前缀
     */
    String COMMON_REDIS_PREFIX = COMMON_PREFIX + CommonSymbol.COLON;

    /**
     * 数据源 key
     */
    interface DataSource {
        /**
         * 主数据源(devops 主库)
         */
        String MASTER = "master";
        /**
         * MyBatis-Flex 默认的数据源名称
         */
        String MYBATIS_FLEX_DEFAULT_KEY = "MyBatis-Flex";
    }
}
