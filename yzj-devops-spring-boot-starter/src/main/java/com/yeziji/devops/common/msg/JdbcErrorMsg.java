package com.yeziji.devops.common.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * jdbc 异常信息
 *
 * @author hwy
 * @since 2024/08/01 21:22
 **/
@Getter
@AllArgsConstructor
public enum JdbcErrorMsg implements CommonErrorEnum {
    BUILD_DDL_FAILED(ERROR, "devops-jdbc-error.build-ddl-failed", "devops-jdbc-show-error.build-ddl-failed"),
    EXECUTE_FAILED(ERROR, "devops-jdbc-error.execute-failed", "devops-jdbc-show-error.execute-failed"),
    ;
    private final int code;
    private final String desc;
    private final String showMsg;
}
