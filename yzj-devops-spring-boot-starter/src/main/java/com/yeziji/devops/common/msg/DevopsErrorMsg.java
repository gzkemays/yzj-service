package com.yeziji.devops.common.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * devops 通用的异常信息
 *
 * @author hwy
 * @since 2024/08/15 18:55
 **/
@Getter
@AllArgsConstructor
public enum DevopsErrorMsg  implements CommonErrorEnum {
    REQUIRED_MASTER_DATA_SOURCE(ERROR, "devops-error.required-master-data-source", "devops-show-error.required-master-data-source"),
    DATA_SOURCE_IS_NOT_EXISTS(ERROR, "devops-error.datasource-is-not-exists", "devops-show-error.datasource-is-not-exists"),
    DATA_SOURCE_NAME_IS_DUPLICATE(ERROR, "devops-error.datasource-name-is-duplicate", "devops-show-error.datasource-name-is-duplicate"),
    ;
    private final int code;
    private final String desc;
    private final String showMsg;
}