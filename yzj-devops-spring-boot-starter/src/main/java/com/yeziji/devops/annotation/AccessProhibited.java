package com.yeziji.devops.annotation;

import java.lang.annotation.*;

/**
 * 禁止访问特定库的数据源
 *
 * @author hwy
 * @since 2024/08/01 17:48
 **/
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessProhibited {
}
