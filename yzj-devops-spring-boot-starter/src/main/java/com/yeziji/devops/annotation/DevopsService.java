package com.yeziji.devops.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Service;

import java.lang.annotation.*;

/**
 * devops @Service
 *
 * @author hwy
 * @since 2024/08/15 18:08
 **/
@Service
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DevopsService {
    /**
     * 指定要执行的数据源
     *
     * @return {@link String}
     */
    String name() default "";

    /**
     * service 名称
     *
     * @return {@link String}
     */
    @AliasFor(annotation = Service.class, value = "value")
    String value() default "";
}
