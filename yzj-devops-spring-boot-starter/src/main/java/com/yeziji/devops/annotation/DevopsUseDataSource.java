package com.yeziji.devops.annotation;

import org.springframework.stereotype.Service;

import java.lang.annotation.*;

/**
 * 强制切换某个数据源（优先级最高）
 * <p>
 *     MybatisFlex 自带的优先级不够高，那么就会导致某个方法切换数据源失败
 * </p>
 * @author hwy
 * @since 2024/10/10 16:35
 **/
@Service
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DevopsUseDataSource {
    /**
     * 使用默认数据源
     */
    boolean useDefault() default true;

    /**
     * 指定要执行的数据源
     *
     * @return {@link String}
     */
    String value() default "";
}