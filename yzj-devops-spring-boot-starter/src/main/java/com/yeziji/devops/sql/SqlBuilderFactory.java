package com.yeziji.devops.sql;

import com.yeziji.devops.common.SqlBuilder;
import com.yeziji.devops.constant.SqlExecuteTypeEnum;
import com.yeziji.devops.sql.base.SqlConstructorBase;
import com.yeziji.devops.sql.builder.*;
import com.yeziji.devops.sql.constructor.*;

/**
 * Sql 构建工厂
 *
 * @author hwy
 * @since 2024/07/31 0:25
 **/
public class SqlBuilderFactory {
    public SqlBuilder getSqlBuilder(SqlExecuteTypeEnum typeEnum, SqlConstructorBase constructor) {
        SqlBuilder sqlBuilder = null;
        if (constructor.getType() == null) {
            throw new IllegalArgumentException("必须选择操作类型");
        } else if (typeEnum != null) {
            if (typeEnum == SqlExecuteTypeEnum.SELECT) {
                sqlBuilder = new SelectBuilder((SelectSqlConstructor) constructor);
            } else if (typeEnum == SqlExecuteTypeEnum.INSERT) {
                sqlBuilder = new InsertBuilder((InsertSqlConstructor) constructor);
            } else if (typeEnum == SqlExecuteTypeEnum.DELETE) {
                sqlBuilder = new DeleteBuilder((DeleteSqlConstructor) constructor);
            } else if (typeEnum == SqlExecuteTypeEnum.UPDATE) {
                sqlBuilder = new UpdateBuilder((UpdateSqlConstructor) constructor);
            } else if (typeEnum == SqlExecuteTypeEnum.CREATE) {
                sqlBuilder = new CreateBuilder((CreateSqlConstructor) constructor);
            } else if (typeEnum == SqlExecuteTypeEnum.ALTER) {
                sqlBuilder = new AlterBuilder((AlterSqlConstructor) constructor);
            }
        }
        if (sqlBuilder == null) {
            throw new NullPointerException("不存在指定的 SQL 构造器");
        }
        return sqlBuilder;
    }
}
