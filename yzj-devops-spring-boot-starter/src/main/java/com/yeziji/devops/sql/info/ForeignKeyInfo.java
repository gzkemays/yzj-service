package com.yeziji.devops.sql.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 外键信息
 * <p>
 * 赋予的外键字段信息
 * </p>
 *
 * @author gzkemays
 * @since 2023/01/13 11:45 AM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ForeignKeyInfo {
    /**
     * 外键名称
     */
    private String name;

    /**
     * 创建外键的字段 - 列表
     */
    private List<String> foreignKeyFields;

    /**
     * 外键引用的表
     */
    private String referencesTable;

    /**
     * 外键引用的字段名称 - 列表
     */
    private List<String> referencesFields;

    /**
     * 删除时引用的方式
     * <ul>四种方式
     *     <li>CASECADE(同步 update/delete)</li>
     *     <li>NO ACTION(不允许引用键作 update/delete 操作 - 推迟操作)</li>
     *     <li>RESTRICT(和 no action 相同,但是它是立即执行的)</li>
     *     <li>SET NULL(引用值被删除时,子表设置为 null *必须notnull*)</li>
     * </ul>
     *
     * @see com.yeziji.devops.constant.mysql.MysqlForeignRelationEnum
     */
    private String deleteMethod;

    /**
     * 更新时引用的方式，与删除方法相同
     */
    private String updateMethod;
}
