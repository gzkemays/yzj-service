package com.yeziji.devops.sql.model.mysql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * SQL 字段
 *
 * @author hwy
 * @since 2024/08/01 21:27
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SqlColumn {
    /**
     * 字段名称
     */
    private String name;

    /**
     * 字段类型
     */
    private String type;

    /**
     * 键值类型
     */
    private String key;

    /**
     * 字段说明
     */
    private String desc;

    /**
     * 默认值
     */
    private Object defaultValue;

    /**
     * 额外值
     */
    private String extra;

    /**
     * 是否允许为
     */
    private boolean acceptNull;

    /**
     * 字段编码
     */
    private String encode;
}
