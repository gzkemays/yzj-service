package com.yeziji.devops.sql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;

/**
 * SQL 执行结果
 *
 * @author hwy
 * @since 2024/08/01 20:15
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SqlResult {
    /**
     * 执行结果
     */
    private Object result;

    /**
     * 操作标识
     */
    private Object flag;

    /**
     * 执行耗时
     */
    private long time;

    /**
     * builder start
     * @return {@link SqlResultBuilder}
     */
    public static SqlResultBuilder builder() {
        MDC.put("start", String.valueOf(System.currentTimeMillis()));
        return new SqlResultBuilder();
    }
}
