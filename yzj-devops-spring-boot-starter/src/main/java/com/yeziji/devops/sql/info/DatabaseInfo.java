package com.yeziji.devops.sql.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 数据库信息
 *
 * @author gzkemays
 * @since 2023/01/08 1:37 AM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DatabaseInfo {
    /**
     * 数据库
     */
    private String database;

    /**
     * 数据库表
     */
    private List<String> tables;
}
