package com.yeziji.devops.sql.builder;

import cn.hutool.core.util.StrUtil;
import com.yeziji.devops.common.SqlBuilder;
import com.yeziji.devops.constant.mysql.MysqlKeywordEnum;
import com.yeziji.devops.sql.base.SqlBuilderBase;
import com.yeziji.devops.sql.constructor.UpdateSqlConstructor;

import java.util.Map;
import java.util.StringJoiner;

/**
 * SQL 更新构造器
 *
 * @author gzkemays
 * @since 2023/01/06 7:46 PM
 */
public class UpdateBuilder extends SqlBuilderBase<UpdateSqlConstructor> implements SqlBuilder {
    public UpdateBuilder(UpdateSqlConstructor constructor) {
        super(constructor);
    }

    @Override
    public String execute() {
        // 构建 update table set
        sqlJoiner.add(getType()).add(getTable()).add(MysqlKeywordEnum.SET.getValue());
        // 构建  key=value
        Map<String, Object> data = constructor.getData();
        StringJoiner set = new StringJoiner(", ");
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            set.add(entry.getKey() + "=?");
        }
        sqlJoiner.add(set.toString());
        // 构建 where 条件
        String wrapper = getWrapper();
        if (StrUtil.isNotBlank(wrapper)) {
            sqlJoiner.add(MysqlKeywordEnum.WHERE.getValue()).add(wrapper);
        }
        return sqlJoiner.toString();
    }
}
