package com.yeziji.devops.sql.constructor;

import com.yeziji.devops.constant.SqlExecuteTypeEnum;
import com.yeziji.devops.sql.base.SqlConstructorBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

/**
 * 查询 SQL 构造器
 *
 * @author gzkemays
 * @since 2023/01/06 11:04 PM
 */
@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class SelectSqlConstructor extends SqlConstructorBase {
    public SelectSqlConstructor() {
        super.type = SqlExecuteTypeEnum.SELECT.getValue();
    }

    @Override
    protected void check() {
        super.setType(SqlExecuteTypeEnum.SELECT.getValue());
        super.check();
    }

    @Override
    public String buildSql() {
        return super.buildSql();
    }
}
