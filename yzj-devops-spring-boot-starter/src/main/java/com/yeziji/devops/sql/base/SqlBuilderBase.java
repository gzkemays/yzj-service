package com.yeziji.devops.sql.base;

import com.yeziji.common.CommonSymbol;
import com.yeziji.constant.VariousStrPool;
import com.yeziji.devops.constant.mysql.MysqlKeywordEnum;

import java.util.List;
import java.util.StringJoiner;

/**
 * SQL 构建超类
 *
 * <p>用于约束和检验数据
 *
 * @author hwy
 * @since 2024/07/31 0:57
 **/
public abstract class SqlBuilderBase<T extends SqlConstructorBase> {
    protected T constructor;
    protected StringJoiner sqlJoiner = new StringJoiner(CommonSymbol.SPACE, VariousStrPool.EMPTY, MysqlKeywordEnum.END.getValue());

    public SqlBuilderBase(T constructor) {
        this.constructor = constructor;
    }

    public SqlBuilderBase() {
    }

    protected String getType() {
        return constructor.getType();
    }

    protected String getTable() {
        return constructor.getTable();
    }

    protected String getWrapper() {
        return constructor.getWrapper();
    }

    protected List<String> getColumns() {
        return constructor.getColumns();
    }
}
