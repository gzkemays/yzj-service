package com.yeziji.devops.sql.builder;

import com.yeziji.devops.common.SqlBuilder;
import com.yeziji.devops.constant.mysql.MysqlKeywordEnum;
import com.yeziji.devops.sql.base.SqlBuilderBase;
import com.yeziji.devops.sql.constructor.InsertSqlConstructor;

import java.util.StringJoiner;

/**
 * SQL 新增构造器
 *
 * @author gzkemays
 * @since 2023/01/06 7:46 PM
 */
public class InsertBuilder extends SqlBuilderBase<InsertSqlConstructor> implements SqlBuilder {
    public InsertBuilder(InsertSqlConstructor constructor) {
        super(constructor);
    }

    @Override
    public String execute() {
        // 构造 insert into table(column1,column2) values
        sqlJoiner.add(super.getType())
                .add(MysqlKeywordEnum.INTO.getValue())
                // 构造插入的字段
                .add(getTable() + MysqlKeywordEnum.brackets(getColumns(), true))
                .add(MysqlKeywordEnum.VALUES.getValue());
        // 构造插入的参数设置为 ?
        StringJoiner valuesJoiner = new StringJoiner(", ", "(", ")");
        for (int i = 0; i < getColumns().size(); i++) {
            valuesJoiner.add("?");
        }
        sqlJoiner.add(valuesJoiner.toString());
        // 构造字段
        return sqlJoiner.toString();
    }
}
