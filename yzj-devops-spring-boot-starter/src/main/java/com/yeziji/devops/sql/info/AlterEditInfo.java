package com.yeziji.devops.sql.info;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * alter 修改信息
 *
 * <ol>
 *   主要实现的数据参数，默认固定前缀：ALTER TABLE XXX MODIFY 和 ALTER TABLE XXX CHANGE
 *   <li>修改字段信息(MODIFY)：column_name varchar(100) NULL COMMENT 'asd';
 *   <li>修改字段名称以及类型(CHANGE)：column_name re_column_name int(11);
 *   <li>修改表名(特殊处理)：RENAME TABLE testdata.test6 TO testdata.test7;
 *   <li>修改表信息：ALTER TABLE XXX DEFAULT CHARSET=geostd8 COLLATE=geostd8_bin COMMENT='32'
 *       AUTO_INCREMENT=0;
 * </ol>
 *
 * @author gzkemays
 * @since 2023/02/19 1:29 PM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlterEditInfo {
    /**
     * 字段名称
     */
    private String columnName;

    /**
     * 字段额外值
     */
    private String extra;

    /**
     * 字段类型
     */
    private String type;

    /**
     * 字段默认数值
     */
    private String defaultValue;

    /**
     * 字段简介
     */
    private String comment;

    /**
     * 字段重命名
     */
    private String columnRename;

    /**
     * 数据表重命名
     */
    private String tableRename;

    /**
     * 表字符集
     */
    private String tableCharset;

    /**
     * 排序规则
     */
    private String tableCollate;

    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 表引擎
     */
    private String tableEngine;

    /**
     * 自增长
     */
    private Integer autoIncrement;

    /**
     * 是否支持 null
     */
    private Boolean acceptNull;

    /**
     * 如果对表任意的字符集、排序规则、注释或者自增的进行操作，则被认定为修改表信息
     *
     * @return {@link Boolean} - 是否修改表信息
     */
    public boolean isEditTableInfo() {
        return StrUtil.isNotBlank(this.tableCharset)
                || StrUtil.isNotBlank(this.tableCollate)
                || StrUtil.isNotBlank(this.tableComment)
                || StrUtil.isNotBlank(this.tableEngine)
                || this.autoIncrement != null;
    }

    /**
     * 任意操作字段额外信息、类型、默认值或者注释，被认定为修改字段信息
     *
     * @return {@link Boolean} 是否修改表信息
     */
    public boolean isModifyColumnInfo() {
        return StrUtil.isNotBlank(this.extra)
                || StrUtil.isNotBlank(this.type)
                || StrUtil.isNotBlank(this.defaultValue)
                || StrUtil.isNotBlank(this.comment);
    }
}
