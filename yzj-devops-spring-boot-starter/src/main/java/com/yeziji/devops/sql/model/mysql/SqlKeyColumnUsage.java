package com.yeziji.devops.sql.model.mysql;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 数据键值使用信息
 * <p>SQL 字段键信息</p>
 *
 * @author gzkemays
 * @since 2023/02/26 6:34 PM
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SqlKeyColumnUsage {
    @JsonAlias("CONSTRAINT_CATALOG")
    private String constraintCatalog;

    @JsonAlias("CONSTRAINT_SCHEMA")
    private String constraintSchema;

    @JsonAlias("CONSTRAINT_NAME")
    private String constraintName;

    @JsonAlias("TABLE_CATALOG")
    private String tableCatalog;

    @JsonAlias("TABLE_SCHEMA")
    private String tableSchema;

    @JsonAlias("TABLE_NAME")
    private String tableName;

    @JsonAlias("COLUMN_NAME")
    private String columnName;

    /**
     * 列与创建时的默认顺序
     */
    @JsonAlias("ORDINAL_POSITION")
    private Integer ordinalPosition;

    @JsonAlias("POSITION_IN_UNIQUE_CONSTRAINT")
    private String positionInUniqueConstraint;

    @JsonAlias("REFERENCED_TABLE_SCHEMA")
    private String referencedTableSchema;

    @JsonAlias("REFERENCED_TABLE_NAME")
    private String referencedTableName;

    @JsonAlias("REFERENCED_COLUMN_NAME")
    private String referencedColumnName;
}
