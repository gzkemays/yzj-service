package com.yeziji.devops.sql.constructor;

import cn.hutool.core.util.StrUtil;
import com.yeziji.devops.common.expcetion.SqlBuilderException;
import com.yeziji.devops.common.msg.SqlBuilderErrorMsg;
import com.yeziji.devops.constant.SqlExecuteTypeEnum;
import com.yeziji.devops.sql.SqlBuilderFactory;
import com.yeziji.devops.sql.base.SqlConstructorBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 插入 SQL 构造器
 *
 * @author gzkemays
 * @since 2023/01/06 10:25 PM
 */
@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InsertSqlConstructor extends SqlConstructorBase {
    /**
     * 插入的值
     * <p>
     * 新增数据时必须传入 values，顺序与 columns 对应
     * </p>
     */
    private List<List<Object>> values;

    /**
     * 是否自增
     * <p>
     * 设置为 true 时，必须确保数据表的主键为自增主键
     * </p>
     */
    private Boolean autoAdd;

    /**
     * 自动生成日期
     * <p>
     * 输入需要生成日期的字段名称
     * </p>
     */
    private String genDate;

    @Override
    protected void check() {
        super.setType(SqlExecuteTypeEnum.INSERT.getValue());
        super.check();
        // 插入数据时 columns 和 values 不能为空
        if (CollectionUtils.isEmpty(super.columns)) {
            throw new SqlBuilderException(SqlBuilderErrorMsg.COLUMN_EMPTY);
        }
        if (CollectionUtils.isEmpty(this.values)) {
            throw new SqlBuilderException(SqlBuilderErrorMsg.VALUES_EMPTY);
        }
        // 长度必须一致
        final int columnSize = super.columns.size();
        for (List<Object> value : this.values) {
            if (value.size() != columnSize) {
                throw new SqlBuilderException(SqlBuilderErrorMsg.SIZE_NOT_EQUALS);
            }
        }
    }

    @Override
    public String buildSql() {
        if (StrUtil.isNotBlank(super.sql)) {
            return super.sql;
        }

        this.check();
        return new SqlBuilderFactory().getSqlBuilder(SqlExecuteTypeEnum.INSERT, this).execute();
    }

    public static void main(String[] args) {
        List<String> columns = new ArrayList<>();
        columns.add("a");
        columns.add("b");
        List<List<Object>> values = new ArrayList<>();
        List<Object> value = new ArrayList<>();
        value.add("e");
        value.add("e2");
        List<Object> value2 = new ArrayList<>();
        value2.add("f");
        value2.add("g");
        values.add(value);
        values.add(value2);
        InsertSqlConstructor constructor =
                InsertSqlConstructor.builder()
                        .type("insert")
                        .table("yzj_forum")
                        .columns(columns)
                        .values(values)
                        .build();
        System.out.println("constructor = " + constructor.buildSql());
    }
}
