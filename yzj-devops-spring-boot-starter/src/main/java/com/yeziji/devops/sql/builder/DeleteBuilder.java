package com.yeziji.devops.sql.builder;

import cn.hutool.core.util.StrUtil;
import com.yeziji.devops.common.SqlBuilder;
import com.yeziji.devops.constant.mysql.MysqlKeywordEnum;
import com.yeziji.devops.sql.base.SqlBuilderBase;
import com.yeziji.devops.sql.constructor.DeleteSqlConstructor;

/**
 * SQL 删除构造器
 *
 * @author hwy
 * @since 2024/08/01 22:17
 **/
public class DeleteBuilder extends SqlBuilderBase<DeleteSqlConstructor> implements SqlBuilder {
    public DeleteBuilder(DeleteSqlConstructor constructor) {
        super(constructor);
    }

    @Override
    public String execute() {
        // 构建 delete from table
        sqlJoiner.add(getType())
                .add(MysqlKeywordEnum.FROM.getValue())
                .add(getTable())
                .add(MysqlKeywordEnum.WHERE.getValue());
        // 构建 wrapper
        if (StrUtil.isNotBlank(getWrapper())) {
            sqlJoiner.add(getWrapper());
        }
        // 如果 id != null 构建 primaryKey(主键的 id 字段名称) = getId();
        if (this.constructor.getId() != null) {
            sqlJoiner.add(MysqlKeywordEnum.wrapper(this.constructor.getPrimaryKey(), this.constructor.getId()));
        }
        return sqlJoiner.toString();
    }
}
