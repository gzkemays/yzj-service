package com.yeziji.devops.sql.model.mysql;

import cn.hutool.core.annotation.Alias;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Sql 的 fields 原型结构
 *
 * @author hwy
 * @since 2024/08/01 21:28
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SqlCaseInsensitive {
    @Alias("Field")
    private String field;

    @Alias("Type")
    private String type;

    @Alias("Collation")
    private String collation;

    @Alias("Null")
    private boolean none;

    @Alias("Key")
    private String key;

    @Alias("Default")
    private Object def;

    @Alias("Extra")
    private String extra;

    @Alias("Privileges")
    private String privileges;

    @Alias("Comment")
    private String comment;

    /**
     * 根据 map 构建 case 原型结构
     *
     * @param map 待转换对象
     * @return {@link SqlCaseInsensitive}
     */
    public static SqlCaseInsensitive convertBy(Map<String, Object> map) {
        return BeanUtil.mapToBean(map, SqlCaseInsensitive.class, false, CopyOptions.create().ignoreNullValue());
    }

    /**
     * 转 Sql Column
     *
     * @return {@link SqlColumn}
     */
    public SqlColumn convertToSqlColumn() {
        return SqlColumn.builder()
                .name(this.field)
                .type(this.type)
                .desc(this.comment)
                .key(this.key)
                .acceptNull(this.none)
                .extra(this.extra)
                .encode(this.collation)
                .defaultValue(this.def)
                .build();
    }
}
