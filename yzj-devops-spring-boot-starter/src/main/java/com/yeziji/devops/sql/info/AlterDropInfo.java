package com.yeziji.devops.sql.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * alter 删除信息
 *
 * <ol>
 *   主要实现的数据参数，默认固定前缀：ALTER TABLE XXX DROP ...
 *   <li>删除约束：CONSTRAINT test6_check;
 *   <li>删除外键：FOREIGN KEY test3_fk_id2_id;
 *   <li>删除索引：INDEX test3_fk_id2_id;
 *   <li>删除字段：COLUMN id2;
 * </ol>
 *
 * @author gzkemays
 * @since 2023/02/19 1:22 PM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlterDropInfo {
    public static final int COLUMN = 0, KEY = 1, CONSTRAINT = 2, FOREIGN = 3, INDEX = 4;
    public static final Set<Integer> MODES = Set.of(KEY, CONSTRAINT, FOREIGN, INDEX, COLUMN);

    /**
     * 删除类型
     * <p>0：字段；1：键；2：约束；3：外键；4：索引</p>
     */
    private Integer mode;

    /**
     * 删除的名称
     * <p>
     * 后续追加的字段、索引、外键或者约束的名称
     * </p>
     */
    private String name;
}
