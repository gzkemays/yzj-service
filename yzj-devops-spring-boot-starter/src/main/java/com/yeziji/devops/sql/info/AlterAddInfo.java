package com.yeziji.devops.sql.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * alter 新增、追加键信息
 *
 * <ol>
 *   主要实现的数据参数，默认固定前缀：ALTER TABLE XXX ADD ...
 *   <li>新增字段:
 *       <p>column_name INT(11) DEFAULT 0 COMMENT 'XXX' -- 默认值为 null 时 DEFAULT 改为 NULL
 *       <p>column_name INT(25) DEFAULT 1 auto_increment NOT NULL COMMENT 'XXX';
 *       <p>column_name INT(25) NULL COMMENT 'XXX';
 *   <li>追加外键: CONSTRAINT foreign_key_name FOREIGN KEY(column_name) REFERENCES
 *       references_database.references_table_name(references_column_name) -- 此处 references
 *       的数据库表要处于同一数据源下
 *   <li>追加约束: CONSTRAINT constraint_name UNIQUE KEY(column_name) -- 追加约束还有 PRIMARY KEY(column_name)
 *       以及 CHECK(表达式)类型
 * </ol>
 *
 * @author gzkemays
 * @since 2023/01/26 11:50 AM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlterAddInfo {
    /**
     * 字段名称
     * <p>
     * 该字段是当前数据源的字段
     * </p>
     */
    private String columnName;

    /**
     * 字段类型
     * <p>
     * 字段的属性类型
     * </p>
     * <pre>
     *     INT(11)
     * </pre>
     */
    private String columnType;

    /**
     * 字段的默认值
     * <p>
     * 如果该值为空，那么在新增字段时默认追加 NULL
     * </P>
     */
    private Object defaultValue;

    /**
     * 额外值
     * <p>
     * 如果 default 存在，那么紧跟 default，否则紧跟类型
     * </p>
     */
    private String extra;

    /**
     * 是否支持空
     * <p>
     * 是否为 NULL 或者不能为 NULL, false -- NOT NULL; true -- NULL，默认为支持 NULL
     * </p>
     */
    @Builder.Default
    private boolean acceptNull = true;

    /**
     * 备注
     * <p>
     * 新增字段时用于备注
     * </p>
     */
    private String comment;

    /**
     * 外键名称
     * <p>
     * 当入参补充该属性时，则默认为开始追加外键 ddl
     * </p>
     */
    private String foreignKeyName;

    /**
     * 外键字段
     * <p>
     * 当前表的外键字段
     * </p>
     */
    private List<String> foreignKeyColumnName;

    /**
     * 删除时引用的方式
     * <ul>四种方式
     *     <li>CASECADE(同步 update/delete)</li>
     *     <li>NO ACTION(不允许引用键作 update/delete 操作 - 推迟操作)</li>
     *     <li>RESTRICT(和 no action 相同,但是它是立即执行的)</li>
     *     <li>SET NULL(引用值被删除时,子表设置为 null *必须notnull*)</li>
     * </ul>
     *
     * @see com.yeziji.devops.constant.mysql.MysqlForeignRelationEnum
     */
    private String foreignKeyDeleteMethod;

    /**
     * 更新时引用的方式
     * <p>
     * 与删除方法相同
     * </p>
     */
    private String foreignKeyUpdateMethod;

    /**
     * 外键关联数据库
     * <p>
     * 该数据表必须是同一数据源下，如果没指定数据库，那么默认为当前数据库
     * </p>
     */
    private String referencesDatabase;

    /**
     * 外键关联表
     * <p>
     * 该数据表必须是同一数据源下
     * </p>
     */
    private String referencesTable;

    /**
     * 外键关联字段名称
     */
    private List<String> referencesColumnName;

    /**
     * 约束名称
     */
    private String constraintName;

    /**
     * 约束字段
     */
    private String constraintColumnName;

    /**
     * 约束字段类型
     * <p>
     * 为 UNIQUE KEY(column)、PRIMARY KEY(column) 和 CHECK(表达式)
     * </p>
     */
    private String constraintType;

    /**
     * check 表达式
     */
    private String checkExpressions;
}
