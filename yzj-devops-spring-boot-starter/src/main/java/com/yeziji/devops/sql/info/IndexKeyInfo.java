package com.yeziji.devops.sql.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 索引键值信息
 * <p>
 * 赋予字段索引
 * </p>
 *
 * @author gzkemays
 * @since 2023/01/13 12:19 PM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndexKeyInfo {
    /**
     * 索引名称
     */
    private String name;

    /**
     * 索引键
     */
    private List<String> fields;

    /**
     * 索引方法
     * <p>
     * 这里将指定字段的索引方法，如果是主键则直接修改 PRIMARY KEY(name) USING XXX，默认值为 BTREE
     * </p>
     * <pre>
     *     HASH
     * </pre>
     */
    private String indexMethod;

    /**
     * 索引描述
     */
    private String comment;
}
