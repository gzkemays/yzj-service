package com.yeziji.devops.sql.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Sql 创建信息
 *
 * <p>一般作用于 table
 *
 * @author gzkemays
 * @since 2023/01/13 11:31 AM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateTableInfo {
    /**
     * 字段名称
     */
    private String name;

    /**
     * 字段类型
     * <pre>
     *     varchar
     * </pre>
     */
    private String type;

    /**
     * 字段大小
     * <pre>
     *     255
     * </pre>
     */
    private int size;

    /**
     * 是否为主键
     */
    private boolean primary;

    /**
     * 是否允许为空，默认允许
     */
    private boolean acceptNull = true;

    /**
     * 是否自增
     */
    private boolean autoIncrement;

    /**
     * 是否为当前时间戳
     */
    private boolean currentTimestamp;

    /**
     * 字段编码
     */
    private String character;

    /**
     * 排序规则
     */
    private String collate;

    /**
     * 默认值
     */
    private Object defaultValue;

    /**
     * 自定义额外值
     */
    private String extra;

    /**
     * 字段备注
     */
    private String comment;
}
