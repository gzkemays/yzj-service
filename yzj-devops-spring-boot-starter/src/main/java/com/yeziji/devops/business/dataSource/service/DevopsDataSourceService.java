package com.yeziji.devops.business.dataSource.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.devops.business.dataSource.dto.DevopsDataSourceOperateDTO;
import com.yeziji.devops.business.dataSource.entity.DevopsDataSourceEntity;

/**
 * devops 管理数据源 服务层。
 *
 * @author system
 * @since 2024-08-15
 */
public interface DevopsDataSourceService extends IService<DevopsDataSourceEntity> {
    /**
     * 创建或更新数据源驱动
     *
     * @param operateDTO 操作对象
     * @return {@link Boolean} 操作结果
     */
    boolean saveOrUpdate(DevopsDataSourceOperateDTO operateDTO);
}
