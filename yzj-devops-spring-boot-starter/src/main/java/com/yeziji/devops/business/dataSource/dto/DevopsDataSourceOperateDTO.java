package com.yeziji.devops.business.dataSource.dto;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSONObject;
import com.yeziji.common.CommonOperate;
import com.yeziji.devops.business.dataSource.entity.DevopsDataSourceEntity;
import com.yeziji.utils.DataUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Map;
import java.util.Objects;

/**
 * 创建 devops 数据源
 *
 * @author hwy
 * @since 2024/10/10 10:08
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DevopsDataSourceOperateDTO extends CommonOperate {
    /**
     * 创建者名称
     */
    private String creator;

    /**
     * 数据源名称说明
     */
    private String name;

    /**
     * 数据源地址
     */
    private String url;

    /**
     * 数据源驱动
     */
    private String driverClassName;

    /**
     * 数据源账号
     */
    private String username;

    /**
     * 数据源密码
     */
    private String password;

    /**
     * 数据源连接参数
     */
    private Map<String, Object> properties;

    /**
     * 数据源简介
     */
    private String desc;

    /**
     * 转换为 entity 对象
     *
     * @return {@link DevopsDataSourceEntity}
     */
    public DevopsDataSourceEntity convertToEntity() {
        return DataUtils.convertBean(this, DevopsDataSourceEntity.class,
                (value, clazz, context) -> {
                    if (Objects.equals(context, "setProperties") && MapUtil.isNotEmpty(this.properties)) {
                        return JSONObject.toJSONString(this.properties);
                    }
                    return new DataUtils.BeanCopierDefaultConverter().convert(value, clazz, context);
                });
    }
}