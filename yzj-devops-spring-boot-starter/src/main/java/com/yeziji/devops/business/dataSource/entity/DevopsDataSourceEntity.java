package com.yeziji.devops.business.dataSource.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * devops 管理数据源 实体类。
 *
 * @author system
 * @since 2024-08-15
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table("devops_data_source")
public class DevopsDataSourceEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = -3265716397115332106L;

    /**
     * 创建者名称
     */
    private String creator;

    /**
     * 数据源名称说明
     */
    private String name;

    /**
     * 数据源地址
     */
    private String url;

    /**
     * 数据源驱动
     */
    private String driverClassName;

    /**
     * 数据源账号
     */
    private String username;

    /**
     * 数据源密码
     */
    private String password;

    /**
     * 数据源连接参数
     */
    private String properties;

    /**
     * 数据源简介
     */
    private String desc;

}
