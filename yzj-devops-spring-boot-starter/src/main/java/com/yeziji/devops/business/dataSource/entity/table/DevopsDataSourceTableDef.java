package com.yeziji.devops.business.dataSource.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;


/**
* devops 管理数据源 表定义层。
*
* @author system
* @since 2024-08-15
*/
public class DevopsDataSourceTableDef extends TableDef {

    private static final long serialVersionUID = 1L;

    /**
     * devops 管理数据源
     */
    public static final DevopsDataSourceTableDef DEVOPS_DATA_SOURCE = new DevopsDataSourceTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 数据源地址
     */
    public final QueryColumn URL = new QueryColumn(this, "url");

    /**
     * 数据源驱动
     */
    public final QueryColumn DRIVER_CLASS_NAME = new QueryColumn(this, "driver_class_name");

    /**
     * 数据源简介
     */
    public final QueryColumn DESC = new QueryColumn(this, "desc");

    /**
     * 数据源名称说明
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 创建者名称
     */
    public final QueryColumn CREATOR = new QueryColumn(this, "creator");

    /**
     * 数据源密码
     */
    public final QueryColumn PASSWORD = new QueryColumn(this, "password");

    /**
     * 数据源账号
     */
    public final QueryColumn USERNAME = new QueryColumn(this, "username");

    /**
     * 数据源连接参数
     */
    public final QueryColumn PROPERTIES = new QueryColumn(this, "properties");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, CREATOR, NAME, URL, DRIVER_CLASS_NAME, USERNAME, PASSWORD, PROPERTIES, DESC, CREATE_TIME, UPDATE_TIME};

    public DevopsDataSourceTableDef() {
        super("", "devops_data_source");
    }

    private DevopsDataSourceTableDef(String schema, String name, String alisa) {
        super(schema, name, alisa);
    }

    public DevopsDataSourceTableDef as(String alias) {
        String key = getNameWithSchema() + "." + alias;
        return getCache(key, k -> new DevopsDataSourceTableDef("", "devops_data_source", alias));
    }

}