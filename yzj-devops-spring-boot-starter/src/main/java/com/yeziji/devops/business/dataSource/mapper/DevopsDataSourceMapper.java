package com.yeziji.devops.business.dataSource.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.devops.business.dataSource.entity.DevopsDataSourceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * devops 管理数据源 映射层。
 *
 * @author system
 * @since 2024-08-15
 */
@Mapper
public interface DevopsDataSourceMapper extends BaseMapper<DevopsDataSourceEntity> {

}
