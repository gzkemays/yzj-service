package com.yeziji.devops.constant;

import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Opt2;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 关键字替换枚举
 *
 * @author hwy
 * @since 2024/07/18 1:07
 **/
@Getter
@AllArgsConstructor
public enum KeywordReplaceEnum implements CommonEnum {
    DATE_KEY(0, "#{DATE}", "动态替换为日期的关键值"),
    TABLE_KEY(1, "#{TABLE}", "动态替换 table 关键字"),
    DATABASE_KEY(2, "#{DATABASE}", "动态替换 database 关键字"),
    ;

    private final int code;
    private final String value;
    private final String desc;

    /**
     * 替换对应的值
     *
     * @param txt    待替换值
     * @param target 目标值
     * @return {@link String}
     */
    public String replace(String txt, String target) {
        return txt.replace(this.getValue(), Opt2.nullEmpty(target));
    }
}
