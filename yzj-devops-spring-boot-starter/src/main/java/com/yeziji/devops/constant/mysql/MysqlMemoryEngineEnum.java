package com.yeziji.devops.constant.mysql;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Mysql 存储引擎
 *
 * @author hwy
 * @since 2024/07/18 1:14
 **/
@Getter
@AllArgsConstructor
public enum MysqlMemoryEngineEnum implements CommonEnum {
    ARCHIVE(0, "ARCHIVE", "ARCHIVE 存储引擎"),
    BLACKHOLE(1, "BLACKHOLE", "BLACKHOLE 存储引擎"),
    CSV(2, "CSV", "CSV 存储引擎"),
    INNODB(3, "INNODB", "INNODB 存储引擎"),
    MEMORY(4, "MEMORY", "MEMORY 存储引擎"),
    MRG_MYISAM(5, "MRG_MYISAM", "MRG_MYISAM 存储引擎"),
    MYISAM(6, "MYISAM", "MYISAM 存储引擎"),
    RERFORMANCE_SCHEMA(7, "RERFORMANCE_SCHEMA", "RERFORMANCE_SCHEMA 存储引擎"),
    ;

    private final int code;
    private final String value;
    private final String desc;

    public static MysqlMemoryEngineEnum getByValue(String value) {
        if (value == null) {
            return null;
        }
        for (MysqlMemoryEngineEnum mysqlMemoryEngineEnum : MysqlMemoryEngineEnum.values()) {
            if (mysqlMemoryEngineEnum.getValue().equals(value.toUpperCase())) {
                return mysqlMemoryEngineEnum;
            }
        }
        return null;
    }
}
