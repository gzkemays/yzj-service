package com.yeziji.devops.constant.mysql;

import com.yeziji.common.CommonEnum;
import com.yeziji.common.CommonSymbol;
import com.yeziji.devops.constant.KeywordReplaceEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.StringJoiner;

/**
 * Mysql 固定语句枚举
 * <p>
 * 固定 sql，用于查询特定的状态
 * </p>
 *
 * @author hwy
 * @since 2024/07/18 1:04
 **/
@Getter
@AllArgsConstructor
public enum MysqlFixedStatementEnum implements CommonEnum {
    SELECT_VERSION(0, "SELECT VERSION();", "查询 mysql 版本"),
    SHOW_CHARACTER_SET(1, "SHOW CHARACTER SET;", "查询 mysql 服务器支持的字符集"),
    SHOW_COLLATION(2, "SHOW COLLATION;", "查询 mysql 字符集校对规则"),
    SHOW_CURRENT_DATABASE_CHARACTER(3, "SHOW VARIABLES LIKE 'CHARACTER%';", "查看当前应用数据库的字符集"),
    SHOW_CURRENT_DATABASE_COLLATION(4, "SHOW VARIABLES LIKE 'COLLATION%';", "查看当前应用数据库的字符集校对规则"),
    SHOW_ENGINES(5, "SHOW ENGINES;", "获取支持的存储引擎"),
    SHOW_TABLE_INDEX(6, "SHOW INDEX FROM #{TABLE}", "获取数据表的索引字段 - 可以通过该方法筛选出主键"),
    SHOW_FULL_PROCESSLIST(7, "SHOW FULL PROCESSLIST", "获取数据库当前所有操作进程"),
    SHOW_INNODB_TRX(8, "SELECT * FROM information_schema.innodb_trx", "查看 DDL 进程"),
    SHOW_TABLES(9, "SHOW TABLES", "查看数据库的数据表"),
    SHOW_DATABASES(10, "SHOW DATABASES", "查看所有数据库"),
    SHOW_FULL_FIELDS_FROM(11, "SHOW FULL FIELDS FROM", "查看全字段"),
    SELECT_KEY_COLUMN_USAGE(12, "SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu", "查看指定数据源下数据表的所有键信息"),
    ;

    private final int code;
    private final String value;
    private final String desc;

    /**
     * 根据 character 查询字符集校对规则
     *
     * @param character 字符集
     * @return 查询 sql 语句
     */
    public static String getShowCollation(String character) {
        return "SHOW COLLATION LIKE '" + character + "';";
    }

    /**
     * 获取数据表的全字段信息
     *
     * @param table 数据表
     * @return {@link String}
     */
    public static String getFullFieldsFromTable(String table) {
        return MysqlFixedStatementEnum.SHOW_FULL_FIELDS_FROM + " " + table;
    }

    /**
     * 根据 where 条件获取 table 的 index 信息
     *
     * @param table 数据表
     * @param where where 条件
     * @return {@link String} index sql ddl
     */
    public static String getTableKeyInfoByWhere(String table, String where) {
        StringJoiner sqlJoiner = new StringJoiner(CommonSymbol.SPACE);
        sqlJoiner.add(KeywordReplaceEnum.TABLE_KEY.replace(SHOW_TABLE_INDEX.getValue(), table))
                .add(MysqlKeywordEnum.WHERE.getValue())
                .add(where);
        return sqlJoiner.toString();
    }

    /**
     * 获取所有键信息
     *
     * @param where 查询条件
     * @return {@link String}
     */
    public static String getKeyColumnUsageByWhere(String where) {
        StringJoiner sj = new StringJoiner(CommonSymbol.SPACE, CommonSymbol.EMPTY, MysqlKeywordEnum.END.getValue());
        sj.add(SELECT_KEY_COLUMN_USAGE.getValue()).add(MysqlKeywordEnum.WHERE.getValue()).add(where);
        return sj.toString();
    }

    /**
     * 获取表的主键字段 sql
     *
     * @param table 数据表
     * @return {@link String} 返回查询 sql 语句
     */
    public static String getTablePrimaryKeySql(String table) {
        return getTableKeyInfoByWhere(table, MysqlKeywordEnum.wrapper("Key_name", MysqlKeywordEnum.PRIMARY.getValue()));
    }

    /**
     * 指定数据库源信息获取字段使用信息
     *
     * @param database 数据库名称
     * @param table    数据表名称
     * @return {@link String} 指定数据库表的字段使用信息
     */
    public static String getKeyColumnUsageByDbTable(String database, String table) {
        return getKeyColumnUsageByWhere("TABLE_SCHEMA='" + database + "' AND TABLE_NAME='" + table + "'");
    }

    /**
     * 根据约束名称获取键信息
     *
     * @param constraintName 约束名称
     * @return {@link String} 指定约束名称获取数据库字段信息
     */
    public static String getKeyColumnUsageByConstraintName(String constraintName) {
        return getKeyColumnUsageByWhere("CONSTRAINT_NAME='" + constraintName + "'");
    }
}