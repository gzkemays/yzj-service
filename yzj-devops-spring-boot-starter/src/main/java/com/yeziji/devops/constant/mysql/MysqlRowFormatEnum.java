package com.yeziji.devops.constant.mysql;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Mysql 行格式枚举
 *
 * @author hwy
 * @since 2024/07/18 1:15
 **/
@Getter
@AllArgsConstructor
public enum MysqlRowFormatEnum implements CommonEnum {
    COMPACT(0, "COMPACT", "COMPACT 行格式"),
    COMPRESSED(1, "COMPRESSED", "COMPRESSED 行格式"),
    DEFAULT(2, "DEFAULT", "DEFAULT 行格式"),
    DYNAMIC(3, "DYNAMIC", "DYNAMIC 行格式"),
    FIXED(4, "FIXED", "FIXED 行格式"),
    REDUNDANT(5, "REDUNDANT", "REDUNDANT 行格式"),
    ;
    private final int code;
    private final String value;
    private final String desc;

    public static MysqlRowFormatEnum getByValue(String value) {
        if (value == null) {
            return null;
        }
        for (MysqlRowFormatEnum mysqlRowFormatEnum : MysqlRowFormatEnum.values()) {
            if (mysqlRowFormatEnum.getValue().equals(value.toUpperCase())) {
                return mysqlRowFormatEnum;
            }
        }
        return null;
    }
}
