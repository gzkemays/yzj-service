package com.yeziji.devops.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * SQL 外键关系枚举
 *
 * @author hwy
 * @since 2024/07/27 0:04
 **/
@Getter
@AllArgsConstructor
public enum SqlForeignRelationEnum implements CommonEnum {
    CASCADE(0, "CASCADE", "级联约束，父表删除，子表也删除"),
    NO_ACTION(1, "NO ACTION", "如果子表存在值，父表不能进行 update/delete 操作 - 延迟操作"),
    RESTRICT(2, "RESTRICT", "和 NO ACTION 相同，但是它是及时操作"),
    SET_NULL(3, "SET NULL", "子表设置为 NULL"),
    ;
    private final int code;
    private final String value;
    private final String desc;

    public static SqlForeignRelationEnum getByValue(String value) {
        if (value == null) {
            return null;
        }
        for (SqlForeignRelationEnum sqlForeignRelationEnum : SqlForeignRelationEnum.values()) {
            if (sqlForeignRelationEnum.getValue().equals(value.toUpperCase())) {
                return sqlForeignRelationEnum;
            }
        }
        return null;
    }

    public static boolean hasRelation(String value) {
        return getByValue(value) == null;
    }
}
