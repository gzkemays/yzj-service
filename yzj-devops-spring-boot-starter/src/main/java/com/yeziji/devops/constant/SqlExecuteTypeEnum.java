package com.yeziji.devops.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * SQL 执行类型
 *
 * @author gzkemays
 * @since 2023/01/06 7:09 PM
 */
@Getter
@AllArgsConstructor
public enum SqlExecuteTypeEnum implements CommonEnum {
    /**
     * 插入
     */
    INSERT(0, "INSERT", "插入"),
    /**
     * 查询
     */
    SELECT(1, "SELECT", "查询"),
    /**
     * 更新
     */
    UPDATE(2, "UPDATE", "更新"),
    /**
     * 删除
     */
    DELETE(3, "DELETE", "删除"),
    /**
     * 创建
     */
    CREATE(4, "CREATE", "创建"),
    /**
     * 修改
     */
    ALTER(5, "ALTER", "修改"),
    ;
    private final int code;
    private final String value;
    private final String desc;

    public static SqlExecuteTypeEnum getByValue(String value) {
        if (value == null) {
            return null;
        }
        for (SqlExecuteTypeEnum typeEnum : SqlExecuteTypeEnum.values()) {
            if (Objects.equals(typeEnum.value, value.toUpperCase())) {
                return typeEnum;
            }
        }
        return null;
    }
}
