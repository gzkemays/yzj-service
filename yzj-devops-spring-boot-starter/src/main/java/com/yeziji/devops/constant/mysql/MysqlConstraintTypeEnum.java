package com.yeziji.devops.constant.mysql;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * mysql 约束类型枚举
 *
 * @author gzkemays
 * @since 2023/01/27 1:34 PM
 */
@Getter
@AllArgsConstructor
public enum MysqlConstraintTypeEnum implements CommonEnum {
    UNIQUE_KEY(0, "UNIQUE KEY", "唯一约束"),
    PRIMARY_KEY(1, "PRIMARY KEY", "主键约束"),
    CHECK(2, "CHECK", "约束表达式"),
    ;
    final int code;
    final String value;
    final String desc;

    /**
     * 构建约束
     *
     * @param mysqlConstraintTypeEnum mysql约束枚举类型
     * @param str                     字段或者表达式
     * @return {@link String} - 构建约束的 ddl
     */
    public static String buildConstraint(
            MysqlConstraintTypeEnum mysqlConstraintTypeEnum, String str) {
        return mysqlConstraintTypeEnum.getValue() + "(" + str + ")";
    }

    /**
     * 判断是否支持当前的约束类型
     *
     * @param type 约束类型
     * @return {@link Boolean} - 支持或不支持
     */
    public static boolean supportType(String type) {
        for (MysqlConstraintTypeEnum value : MysqlConstraintTypeEnum.values()) {
            if (value.getValue().equals(type.toUpperCase())) {
                return true;
            }
        }
        return false;
    }
}
