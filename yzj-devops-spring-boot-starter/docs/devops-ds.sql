CREATE TABLE `devops_data_source`
(
    `id`                int(11) NOT NULL AUTO_INCREMENT,
    `name`              varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据源名称说明',
    `creator`           varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者名称',
    `url`               longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '数据源地址',
    `driver_class_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  DEFAULT NULL COMMENT '数据源驱动',
    `username`          varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据源账号',
    `password`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '数据源密码',
    `properties`        blob COMMENT '数据源连接参数',
    `desc`              varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '数据源简介',
    `create_time`       datetime                                                      DEFAULT NULL COMMENT '创建时间',
    `update_time`       datetime                                                      DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`         tinyint(1)                                                    DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='devops 管理数据源';