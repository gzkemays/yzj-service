CREATE TABLE `job_info`
(
    `id`                 bigint(20)                                                    NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `name`               varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '任务名称',
    `group_name`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务分组名称',
    `invoke_target`      varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行目标',
    `invoke_params`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci          DEFAULT NULL COMMENT '执行参数',
    `cron`               varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci           DEFAULT NULL COMMENT 'cron 执行表达式',
    `misfire_policy`     tinyint(4)                                                             DEFAULT '0' COMMENT '执行异常策略： 默认 0 （详见：JobMisfirePolicyEnum）',
    `is_concurrent`      tinyint(4)                                                             DEFAULT '0' COMMENT '是否允许并发',
    `status`             tinyint(4)                                                    NOT NULL DEFAULT '0' COMMENT '任务状态：暂停 0（详见：JobStatusEnum）',
    `responsible_person` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci           DEFAULT NULL COMMENT '负责人',
    `remark`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci          DEFAULT NULL COMMENT '任务备注',
    `create_time`        datetime                                                               DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        datetime                                                               DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`          tinyint(1)                                                    NOT NULL DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `job_info_name_IDX` (`name`) USING BTREE,
    KEY `job_info_group_name_IDX` (`group_name`) USING BTREE,
    KEY `job_info_invoke_target_IDX` (`invoke_target`) USING BTREE,
    KEY `job_info_responsible_person_IDX` (`responsible_person`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='定时任务信息';

CREATE TABLE `job_log`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `job_id`              bigint(20) NOT NULL                                           DEFAULT '0' COMMENT '日志的任务 id',
    `msg`                 blob COMMENT '日志信息',
    `execute_ip`          varchar(100) COLLATE utf8mb4_general_ci                       DEFAULT NULL COMMENT '执行 ip',
    `execute_flag`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行标识',
    `execute_params`      varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行参数',
    `execute_result_code` tinyint(4)                                                    DEFAULT NULL COMMENT '执行结果码：JobExecuteResultCodeEnum',
    `retry_count`         int(11)                                                       DEFAULT '0' COMMENT '任务重试次数',
    `notice_status`       tinyint(1)                                                    DEFAULT NULL COMMENT '通知状态',
    `execute_time`        datetime                                                      DEFAULT NULL COMMENT '执行时候的时间',
    `create_time`         datetime                                                      DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         datetime                                                      DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`           tinyint(1) NOT NULL                                           DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='任务日志';