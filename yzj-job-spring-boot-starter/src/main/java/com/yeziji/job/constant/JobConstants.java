package com.yeziji.job.constant;

/**
 * 任务常量
 *
 * @author hwy
 * @since 2024/05/08 15:45
 **/
public interface JobConstants {
    /**
     * 默认组名
     */
    String DEFAULT_GROUP_NAME = "DEFAULT";

    /** 任务类名 */
    String JOB_CLASS_NAME = "JOB_CLASS_NAME";

    /** 任务目标 key */
    String JOB_PARAMS = "JOB_PARAMS";

    /**
     * 任务执行标识
     */
    String JOB_EXECUTE_FLAG = "JOB_EXECUTE_FLAG";

    /**
     * 任务默认标识
     */
    String JOB_DEFAULT_FLAG = "JOB_FLAG_{0}:{1}";
}
