package com.yeziji.job.constant;

import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Lists2;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.quartz.Trigger;

import java.util.Objects;

/**
 * 任务执行结果码
 *
 * @author hwy
 * @since 2024/06/28 16:14
 **/
@Getter
@AllArgsConstructor
public enum JobExecuteResultCodeEnum implements CommonEnum {
    UNKNOWN(-1, "未知"),
    NONE(0, "无"),
    NORMAL(1, "正常"),
    PAUSED(2, "暂停"),
    COMPLETE(3, "已完成"),
    ERROR(4, "异常"),
    BLOCKED(5, "等待"),
    ;
    private final int code;
    private final String desc;

    public static JobExecuteResultCodeEnum getByCode(Integer code) {
        return Lists2.filterFirstOpt(values(), statusEnum -> Objects.equals(statusEnum.getCode(), code)).orElse(UNKNOWN);
    }

    /**
     * 根据 quartz 状态获取 log 枚举状态
     *
     * @param triggerState quartz 任务状态
     * @return {@link JobExecuteResultCodeEnum}
     */
    public static JobExecuteResultCodeEnum getByTriggerState(Trigger.TriggerState triggerState) {
        switch (triggerState) {
            case NONE:
                return NONE;
            case NORMAL:
                return NORMAL;
            case PAUSED:
                return PAUSED;
            case COMPLETE:
                return COMPLETE;
            case ERROR:
                return ERROR;
            case BLOCKED:
                return BLOCKED;
            default:
                return UNKNOWN;
        }
    }
}
