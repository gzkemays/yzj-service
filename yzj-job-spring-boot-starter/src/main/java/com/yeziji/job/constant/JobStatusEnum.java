package com.yeziji.job.constant;

import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Lists2;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务状态
 *
 * @author hwy
 * @since 2024/05/08 15:47
 **/
@Getter
@AllArgsConstructor
public enum JobStatusEnum implements CommonEnum {
    UNKNOWN(-99, "未知"),
    PAUSED(0, "已暂停"),
    START(1, "已启动"),
    RUNNING(2, "执行中"),
    ;

    private final int code;
    private final String desc;

    public static JobStatusEnum getByCode(Integer code) {
        if (code == null) {
            return UNKNOWN;
        }
        return Lists2.filterFirstOpt(JobStatusEnum.values(), statusEnum -> statusEnum.getCode() == code).orElse(UNKNOWN);
    }

    public static boolean isStart(Integer code) {
        return getByCode(code) == START;
    }

    public static boolean isPaused(Integer code) {
        return getByCode(code) == PAUSED;
    }
}
