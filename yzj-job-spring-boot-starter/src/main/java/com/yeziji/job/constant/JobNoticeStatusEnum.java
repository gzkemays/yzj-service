package com.yeziji.job.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务通知状态
 *
 * @author hwy
 * @since 2024/06/28 17:31
 **/
@Getter
@AllArgsConstructor
public enum JobNoticeStatusEnum implements CommonEnum {
    FAILED(-1, "通知失败"),
    NONE(0, "不需要通知"),
    SUCCESS(1, "通知成功"),
    ;

    private final int code;
    private final String desc;
}
