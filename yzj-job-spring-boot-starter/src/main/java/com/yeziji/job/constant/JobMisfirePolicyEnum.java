package com.yeziji.job.constant;

import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Lists2;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.quartz.JobExecutionException;

/**
 * @author hwy
 * @since 2024/05/08 16:03
 **/
@Getter
@AllArgsConstructor
public enum JobMisfirePolicyEnum implements CommonEnum {
    DEFAULT(0, "默认"),
    IGNORE_MISFIRES(1, "忽略并马上执行"),
    FIRE_AND_PROCEED(2, "触发一次执行"),
    DO_NOTHING(3, "不执行"),
    ;
    private final int code;
    private final String desc;

    public static JobMisfirePolicyEnum getByCode(Integer code) {
        return Lists2.filterFirstOpt(values(), item -> item.getCode() == code).orElse(DEFAULT);
    }

    public static boolean isDefault(Integer code) {
        return getByCode(code) == DEFAULT;
    }

    public static boolean isIgnoreMisfires(Integer code) {
        return getByCode(code) == IGNORE_MISFIRES;
    }

    public static boolean isFireAndProceed(Integer code) {
        return getByCode(code) == FIRE_AND_PROCEED;
    }

    public static boolean isDoNothing(Integer code) {
        return getByCode(code) == DO_NOTHING;
    }

    public void jobExecutionExceptionMisfire(JobExecutionException jobExecutionException) {
        switch (this) {
            case DEFAULT:
            case DO_NOTHING:
                jobExecutionException.setUnscheduleAllTriggers(true);
                break;
            case IGNORE_MISFIRES:
                jobExecutionException.setRefireImmediately(true);
        }
    }
}
