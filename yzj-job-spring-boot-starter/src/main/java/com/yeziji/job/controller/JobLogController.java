package com.yeziji.job.controller;

import com.yeziji.common.CommonPage;
import com.yeziji.common.CommonResult;
import com.yeziji.job.business.jobLog.dto.JobLogDTO;
import com.yeziji.job.business.jobLog.dto.params.JobLogQueryDTO;
import com.yeziji.job.business.jobLog.service.JobLogService;
import com.yeziji.job.controller.vo.JobLogVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 任务日志
 *
 * @author hwy
 * @since 2024/06/28 16:32
 **/
@RestController
@RequestMapping("${yzj.job.api:/yzjJob}/log")
public class JobLogController {
    @Resource
    private JobLogService jobLogService;

    @PostMapping("/page")
    public CommonResult<CommonPage<JobLogVO>> page(@RequestBody JobLogQueryDTO queryDTO) {
        return CommonResult.successPage(jobLogService.pageAsDto(queryDTO), JobLogDTO::convertToVo);
    }
}
