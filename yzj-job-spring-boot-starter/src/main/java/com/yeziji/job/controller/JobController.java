package com.yeziji.job.controller;

import com.yeziji.common.CommonPage;
import com.yeziji.common.CommonResult;
import com.yeziji.job.business.jobInfo.base.JobInfo;
import com.yeziji.job.business.jobInfo.dto.params.JobInfoQueryDTO;
import com.yeziji.job.business.jobInfo.service.JobInfoService;
import com.yeziji.job.common.base.JobControllerBase;
import org.springframework.web.bind.annotation.*;

/**
 * 任务控制器
 *
 * @author hwy
 * @since 2024/06/28 0:15
 **/
@RestController
@RequestMapping("${yzj.job.api:/yzjJob}")
public class JobController extends JobControllerBase {
    public JobController(JobInfoService jobInfoService) {
        super(jobInfoService);
    }

    @PostMapping("/page")
    public CommonResult<CommonPage<JobInfo>> page(@RequestBody JobInfoQueryDTO query) {
        return CommonResult.successPage(jobInfoService.pageAsJobInfo(query));
    }

    @PostMapping("/run")
    public CommonResult<Boolean> runJob(@RequestBody JobInfo jobInfo) {
        try {
            return CommonResult.success(super.run(jobInfo));
        } catch (Exception e) {
            return CommonResult.success(false);
        }
    }

    @PostMapping("/update")
    public CommonResult<Boolean> updateJob(@RequestBody JobInfo jobInfo) {
        return CommonResult.success(super.update(jobInfo));
    }

    @PostMapping("/create")
    public CommonResult<Boolean> createJob(@RequestBody JobInfo jobInfo) {
        return CommonResult.success(super.create(jobInfo));
    }

    @DeleteMapping("/delete/{id}")
    public CommonResult<Boolean> deleteJob(@PathVariable("id") Long id) {
        return CommonResult.success(super.delete(id));
    }

    @PutMapping("/paused/{id}")
    public CommonResult<Boolean> pausedJob(@PathVariable("id") Long id) {
        return CommonResult.success(super.paused(id));
    }

    @PutMapping("/resume/{id}")
    public CommonResult<Boolean> resumeJob(@PathVariable("id") Long id) {
        return CommonResult.success(super.resume(id));
    }
}
