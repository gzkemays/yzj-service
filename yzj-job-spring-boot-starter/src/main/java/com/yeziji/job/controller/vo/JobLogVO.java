package com.yeziji.job.controller.vo;

import com.yeziji.job.constant.JobExecuteResultCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 任务日志视图对象
 *
 * @author hwy
 * @since 2024/06/28 17:27
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobLogVO {
    /**
     * 日志 id
     */
    private Long id;

    /**
     * 日志的任务 id
     */
    private Long jobId;

    /**
     * 任务分组
     */
    private String jobGroupName;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 日志信息
     */
    private String msg;

    /**
     * 执行 ip
     */
    private String executeIp;

    /**
     * 执行标识
     */
    private String executeFlag;

    /**
     * 执行参数
     */
    private String executeParams;

    /**
     * 执行结果码：JobExecuteResultCodeEnum
     *
     * @see JobExecuteResultCodeEnum#getDesc()
     */
    private String executeResult;

    /**
     * 任务重试次数
     */
    private Integer retryCount;

    /**
     * 通知状态
     */
    private Integer noticeStatus;

    /**
     * 执行时候的时间
     */
    private Date executeTime;

    /**
     * 日志创建时间
     */
    private Date createTime;
}
