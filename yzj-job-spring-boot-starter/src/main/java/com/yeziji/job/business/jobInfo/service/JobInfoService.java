package com.yeziji.job.business.jobInfo.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import com.yeziji.job.business.jobInfo.base.JobInfo;
import com.yeziji.job.business.jobInfo.dto.params.JobInfoQueryDTO;
import com.yeziji.job.business.jobInfo.entity.JobInfoEntity;

import java.util.List;

/**
 * 定时任务信息 服务层。
 *
 * @author system
 * @since 2024-05-08
 */
public interface JobInfoService extends IService<JobInfoEntity> {
    /**
     * 创建任务
     *
     * @param jobInfo 创建任务信息
     * @return {@link Boolean}
     */
    boolean create(JobInfo jobInfo);

    /**
     * 更新任务
     *
     * @param jobInfo 任务信息
     * @return {@link Boolean}
     */
    boolean update(JobInfo jobInfo);

    /**
     * 执行任务
     *
     * @param jobInfo 执行任务信息
     * @return {@link Boolean}
     */
    boolean run(JobInfo jobInfo);

    /**
     * 暂停任务
     *
     * @param id 任务 id
     * @return {@link Boolean}
     */
    boolean paused(Long id);

    /**
     * 重新恢复任务
     *
     * @param id 任务 id
     * @return {@link Boolean}
     */
    boolean resume(Long id);

    /**
     * 删除任务
     *
     * @param id 任务 id
     * @return {@link Boolean}
     */
    boolean delete(Long id);

    /**
     * 保存或更新信息
     *
     * @param jobInfo        任务信息
     * @param requiredExists 是否必須存在
     * @return {@link Boolean} 结果
     */
    boolean saveOrUpdate(JobInfo jobInfo, boolean requiredExists);

    /**
     * 分页查询
     *
     * @param queryDTO 查询条件
     * @return {@link Page}
     */
    Page<JobInfo> pageAsJobInfo(JobInfoQueryDTO queryDTO);

    /**
     * 查询列表
     *
     * @param queryDTO 查询条件
     * @return {@link List}
     */
    List<JobInfo> listAsJobInfo(JobInfoQueryDTO queryDTO);
}
