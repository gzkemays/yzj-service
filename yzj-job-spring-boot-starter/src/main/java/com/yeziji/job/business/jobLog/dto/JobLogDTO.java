package com.yeziji.job.business.jobLog.dto;

import com.mybatisflex.annotation.TableRef;
import com.yeziji.job.business.jobLog.entity.JobLogEntity;
import com.yeziji.job.constant.JobExecuteResultCodeEnum;
import com.yeziji.job.controller.vo.JobLogVO;
import com.yeziji.utils.GzipUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务日志对象
 *
 * @author hwy
 * @since 2024/06/28 16:20
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableRef(JobLogEntity.class)
public class JobLogDTO implements Serializable {
    private static final long serialVersionUID = -1007588780697620963L;

    /**
     * 日志 id
     */
    private Long id;

    /**
     * 日志的任务 id
     */
    private Long jobId;

    /**
     * 日志信息
     */
    private byte[] msg;

    /**
     * 执行 ip
     */
    private String executeIp;

    /**
     * 执行标识
     */
    private String executeFlag;

    /**
     * 执行参数
     */
    private String executeParams;

    /**
     * 执行结果码：JobExecuteResultCodeEnum
     */
    private Integer executeResultCode;

    /**
     * 任务重试次数
     */
    private Integer retryCount;

    /**
     * 通知状态
     *
     * @see com.yeziji.job.constant.JobNoticeStatusEnum
     */
    private Integer noticeStatus;

    /**
     * 执行时候的时间
     */
    private Date executeTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    // 其他属性
    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务分组信息
     */
    private String jobGroupName;

    public JobLogVO convertToVo() {
        return JobLogVO.builder()
                .id(id)
                .jobId(jobId)
                .jobName(jobName)
                .jobGroupName(jobGroupName)
                .msg(GzipUtils.uncompress(msg))
                .executeIp(executeIp)
                .executeFlag(executeFlag)
                .executeParams(executeParams)
                .executeResult(JobExecuteResultCodeEnum.getByCode(executeResultCode).getLangDesc())
                .retryCount(retryCount)
                .noticeStatus(noticeStatus)
                .executeTime(executeTime)
                .createTime(createTime)
                .build();
    }
}
