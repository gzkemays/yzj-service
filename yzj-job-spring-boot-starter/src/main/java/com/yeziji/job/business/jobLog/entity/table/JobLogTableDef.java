package com.yeziji.job.business.jobLog.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;


/**
* 任务日志 表定义层。
*
* @author system
* @since 2024-06-27
*/
public class JobLogTableDef extends TableDef {

    private static final long serialVersionUID = 1L;

    /**
     * 任务日志
     */
    public static final JobLogTableDef JOB_LOG = new JobLogTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 日志信息
     */
    public final QueryColumn MSG = new QueryColumn(this, "msg");

    /**
     * 日志的任务 id
     */
    public final QueryColumn JOB_ID = new QueryColumn(this, "job_id");

    /**
     * 执行 ip
     */
    public final QueryColumn EXECUTE_IP = new QueryColumn(this, "execute_ip");

    /**
     * 执行标识
     */
    public final QueryColumn EXECUTE_FLAG = new QueryColumn(this, "execute_flag");

    /**
     * 任务重试次数
     */
    public final QueryColumn RETRY_COUNT = new QueryColumn(this, "retry_count");

    /**
     * 通知状态
     */
    public final QueryColumn NOTICE_STATUS = new QueryColumn(this, "notice_status");

    /**
     * 执行参数
     */
    public final QueryColumn EXECUTE_PARAMS = new QueryColumn(this, "execute_params");

    /**
     * 执行结果码：JobExecuteResultCodeEnum
     */
    public final QueryColumn EXECUTE_RESULT_CODE = new QueryColumn(this, "execute_result_code");

    /**
     * 执行时候的时间
     */
    public final QueryColumn EXECUTE_TIME = new QueryColumn(this, "execute_time");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, JOB_ID, MSG, EXECUTE_IP, EXECUTE_FLAG, EXECUTE_PARAMS, EXECUTE_RESULT_CODE, RETRY_COUNT, NOTICE_STATUS, EXECUTE_TIME, CREATE_TIME, UPDATE_TIME};

    public JobLogTableDef() {
        super("", "job_log");
    }

    private JobLogTableDef(String schema, String name, String alisa) {
        super(schema, name, alisa);
    }

    public JobLogTableDef as(String alias) {
        String key = getNameWithSchema() + "." + alias;
        return getCache(key, k -> new JobLogTableDef("", "job_log", alias));
    }

}