package com.yeziji.job.business.jobLog.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import com.yeziji.job.business.jobLog.dto.JobLogDTO;
import com.yeziji.job.business.jobLog.dto.params.JobLogQueryDTO;
import com.yeziji.job.business.jobLog.entity.JobLogEntity;
import com.yeziji.job.common.base.JobContextInfo;

/**
 * 任务日志 服务层。
 *
 * @author system
 * @since 2024-06-27
 */
public interface JobLogService extends IService<JobLogEntity> {
    /**
     * 根据任务上下文信息进行保存
     *
     * @param jobContextInfo 任务上下文信息
     */
    void saveByJobContextInfo(JobContextInfo jobContextInfo);

    /**
     * 查询分页日志
     *
     * @param queryDTO 查询对象
     * @return {@link Page}
     */
    Page<JobLogDTO> pageAsDto(JobLogQueryDTO queryDTO);
}
