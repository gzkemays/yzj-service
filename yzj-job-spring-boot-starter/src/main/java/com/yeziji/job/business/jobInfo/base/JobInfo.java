package com.yeziji.job.business.jobInfo.base;

import com.yeziji.job.business.jobInfo.entity.JobInfoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 任务信息
 *
 * @author hwy
 * @since 2024/05/08 16:37
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class JobInfo implements Serializable {
    private static final long serialVersionUID = 1071771305365534356L;
    /**
     * 任务 id
     */
    private Long id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务分组名称
     */
    private String groupName;

    /**
     * 执行任务的目标
     */
    private String invokeTarget;

    /**
     * 执行参数
     */
    private String invokeParams;

    /**
     * cron 执行表达式
     */
    private String cron;

    /**
     * 执行异常策略： 默认 0 （详见：JobMisfirePolicyEnum）
     */
    private Integer misfirePolicy;

    /**
     * 是否允许并发
     */
    private Boolean isConcurrent;

    /**
     * 负责人
     */
    private String responsiblePerson;

    /**
     * 任务状态：暂停 0（详见：JobStatusEnum）
     */
    private Integer status;

    /**
     * 任务备注
     */
    private String remark;

    /**
     * 拷贝值至 entity
     *
     * @param entity 待拷贝对象
     */
    public void copyToEntity(JobInfoEntity entity) {
        entity.setName(this.name);
        entity.setGroupName(this.groupName);
        entity.setInvokeTarget(this.invokeTarget);
        entity.setInvokeParams(this.invokeParams);
        entity.setCron(this.cron);
        entity.setMisfirePolicy(this.misfirePolicy);
        entity.setIsConcurrent(this.isConcurrent);
        entity.setResponsiblePerson(this.responsiblePerson);
        entity.setStatus(this.status);
        entity.setRemark(this.remark);
    }

    /**
     * 拷贝 entity 的基本属性
     *
     * @param entity entity
     */
    public void copyBaseAttrsByEntity(JobInfoEntity entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.groupName = entity.getGroupName();
        this.invokeTarget = entity.getInvokeTarget();
        this.cron = entity.getCron();
        this.misfirePolicy = entity.getMisfirePolicy();
        this.isConcurrent = entity.getIsConcurrent();
        this.responsiblePerson = entity.getResponsiblePerson();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public JobInfoEntity convertToEntity() {
        return JobInfoEntity.builder()
                .name(this.name)
                .groupName(this.groupName)
                .invokeTarget(this.invokeTarget)
                .invokeParams(this.invokeParams)
                .cron(this.cron)
                .misfirePolicy(this.misfirePolicy)
                .isConcurrent(this.isConcurrent)
                .responsiblePerson(this.responsiblePerson)
                .status(this.status)
                .remark(this.remark)
                .build();
    }

    public static JobInfo convertByEntity(JobInfoEntity entity) {
        return JobInfo.builder()
                .id(entity.getId())
                .name(entity.getName())
                .groupName(entity.getGroupName())
                .invokeTarget(entity.getInvokeTarget())
                .invokeParams(entity.getInvokeParams())
                .cron(entity.getCron())
                .misfirePolicy(entity.getMisfirePolicy())
                .isConcurrent(entity.getIsConcurrent())
                .status(entity.getStatus())
                .remark(entity.getRemark())
                .build();
    }
}
