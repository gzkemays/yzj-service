package com.yeziji.job.business.jobLog.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务日志 实体类。
 *
 * @author system
 * @since 2024-06-27
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table("job_log")
public class JobLogEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = 1686467271850182847L;

    /**
     * 日志的任务 id
     */
    private Long jobId;

    /**
     * 日志信息
     */
    private byte[] msg;

    /**
     * 执行 ip
     */
    private String executeIp;

    /**
     * 执行标识
     */
    private String executeFlag;

    /**
     * 执行参数
     */
    private String executeParams;

    /**
     * 执行结果码：JobExecuteResultCodeEnum
     */
    private Integer executeResultCode;

    /**
     * 任务重试次数
     */
    private Integer retryCount;

    /**
     * 通知状态
     */
    private Integer noticeStatus;

    /**
     * 执行时候的时间
     */
    private Date executeTime;

}
