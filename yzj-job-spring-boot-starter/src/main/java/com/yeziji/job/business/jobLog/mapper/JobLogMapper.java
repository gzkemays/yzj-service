package com.yeziji.job.business.jobLog.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.job.business.jobLog.entity.JobLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 任务日志 映射层。
 *
 * @author system
 * @since 2024-06-27
 */
@Mapper
public interface JobLogMapper extends BaseMapper<JobLogEntity> {

}
