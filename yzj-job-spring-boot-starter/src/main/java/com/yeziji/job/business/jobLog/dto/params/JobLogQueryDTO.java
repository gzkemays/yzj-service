package com.yeziji.job.business.jobLog.dto.params;

import com.yeziji.common.CommonQuery;
import com.yeziji.job.business.jobLog.dto.JobLogDTO;
import lombok.*;

import java.util.Collection;

/**
 * 任务日志查询对象
 *
 * @author hwy
 * @since 2024/06/28 16:34
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class JobLogQueryDTO extends CommonQuery<JobLogDTO> {
    /**
     * 查询任务 id
     */
    private Long jobId;

    /**
     * 批量查询任务 id
     */
    private Collection<Long> jobIds;

    /**
     * 查询 flag
     */
    private String executeFlag;

    /**
     * 查询执行结果码
     * @see com.yeziji.job.constant.JobExecuteResultCodeEnum
     */
    private Integer executeResultCode;

    @Override
    public Class<JobLogDTO> getQueryClass() {
        return JobLogDTO.class;
    }
}
