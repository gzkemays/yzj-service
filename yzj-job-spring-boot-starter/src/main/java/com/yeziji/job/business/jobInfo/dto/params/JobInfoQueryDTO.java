package com.yeziji.job.business.jobInfo.dto.params;

import com.yeziji.common.CommonQuery;
import com.yeziji.job.business.jobInfo.base.JobInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 任务信息查询对象
 *
 * @author hwy
 * @since 2024/05/09 16:13
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class JobInfoQueryDTO extends CommonQuery<JobInfo> {
    /**
     * 任务名称
     */
    private String name;
    /**
     * 查询状态
     * @see com.yeziji.job.constant.JobStatusEnum
     */
    private Integer status;
    /**
     * 任务组名
     */
    private String groupName;
    /**
     * 任务 handler
     */
    private String invokeTarget;
    /**
     * 负责人
     */
    private String responsiblePerson;

    @Override
    public Class<JobInfo> getQueryClass() {
        return JobInfo.class;
    }
}
