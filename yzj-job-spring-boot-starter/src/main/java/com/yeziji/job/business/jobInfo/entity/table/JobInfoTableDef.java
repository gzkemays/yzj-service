package com.yeziji.job.business.jobInfo.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;


/**
 * 定时任务信息 表定义层。
 *
 * @author system
 * @since 2024-05-08
 */
public class JobInfoTableDef extends TableDef {

    private static final long serialVersionUID = 1L;

    /**
     * 定时任务信息
     */
    public static final JobInfoTableDef JOB_INFO = new JobInfoTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 删除标识
     */
    public final QueryColumn IS_DELETE = new QueryColumn(this, "is_delete");

    /**
     * cron 执行表达式
     */
    public final QueryColumn CRON = new QueryColumn(this, "cron");

    /**
     * 任务名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 任务备注
     */
    public final QueryColumn REMARK = new QueryColumn(this, "remark");

    /**
     * 任务状态：暂停 0（详见：JobStatusEnum）
     */
    public final QueryColumn STATUS = new QueryColumn(this, "status");

    /**
     * 任务分组名称
     */
    public final QueryColumn GROUP_NAME = new QueryColumn(this, "group_name");

    /**
     * 执行目标信息
     */
    public final QueryColumn INVOKE_TARGET = new QueryColumn(this, "invoke_target");

    /**
     * 执行目标参数
     */
    public final QueryColumn INVOKE_PARAMS = new QueryColumn(this, "invoke_params");

    /**
     * 是否允许并发
     */
    public final QueryColumn IS_CONCURRENT = new QueryColumn(this, "is_concurrent");

    /**
     * 执行异常策略： 默认 0 （详见：JobMisfirePolicyEnum）
     */
    public final QueryColumn MISFIRE_POLICY = new QueryColumn(this, "misfire_policy");

    public final QueryColumn RESPONSIBLE_PERSON = new QueryColumn(this, "responsible_person");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, NAME, GROUP_NAME, INVOKE_TARGET, INVOKE_PARAMS, CRON, MISFIRE_POLICY, IS_CONCURRENT, STATUS, RESPONSIBLE_PERSON, REMARK, CREATE_TIME, UPDATE_TIME, IS_DELETE};

    public JobInfoTableDef() {
        super("", "job_info");
    }

    private JobInfoTableDef(String schema, String name, String alisa) {
        super(schema, name, alisa);
    }

    public JobInfoTableDef as(String alias) {
        String key = getNameWithSchema() + "." + alias;
        return getCache(key, k -> new JobInfoTableDef("", "job_info", alias));
    }

}
