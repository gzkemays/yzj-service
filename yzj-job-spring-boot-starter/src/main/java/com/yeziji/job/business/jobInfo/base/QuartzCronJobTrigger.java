package com.yeziji.job.business.jobInfo.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.TriggerKey;

/**
 * quartz 定时任务触发器存储对象
 *
 * @author hwy
 * @since 2024/05/09 10:03
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuartzCronJobTrigger {
    /**
     * 任务 detail
     */
    JobDetail jobDetail;

    /**
     * 定时触发器
     */
    CronTrigger trigger;

    public JobKey jobKey() {
        return this.jobDetail.getKey();
    }

    public TriggerKey triggerKey() {
        return this.trigger.getKey();
    }
}
