package com.yeziji.job.business.jobInfo.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 定时任务信息 实体类。
 *
 * @author system
 * @since 2024-05-08
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table("job_info")
public class JobInfoEntity extends CommonEntity implements Serializable {

    private static final long serialVersionUID = 6149066278119570986L;
    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务分组名称
     */
    private String groupName;

    /**
     * 执行参数
     */
    private String invokeParams;

    /**
     * 执行目标
     */
    private String invokeTarget;

    /**
     * cron 执行表达式
     */
    private String cron;

    /**
     * 执行异常策略： 默认 0 （详见：JobMisfirePolicyEnum）
     */
    private Integer misfirePolicy;

    /**
     * 是否允许并发
     */
    private Boolean isConcurrent;

    /**
     * 任务状态：暂停 0（详见：JobStatusEnum）
     */
    private Integer status;

    /**
     * 负责人
     */
    private String responsiblePerson;

    /**
     * 任务备注
     */
    private String remark;

}
