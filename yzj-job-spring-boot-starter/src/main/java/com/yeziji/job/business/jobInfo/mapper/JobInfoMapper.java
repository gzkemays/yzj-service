package com.yeziji.job.business.jobInfo.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.job.business.jobInfo.entity.JobInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务信息 映射层。
 *
 * @author system
 * @since 2024-05-08
 */
@Mapper
public interface JobInfoMapper extends BaseMapper<JobInfoEntity> {

}
