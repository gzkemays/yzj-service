package com.yeziji.job.common.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author hwy
 * @since 2024/05/09 1:08
 **/
@Getter
@AllArgsConstructor
public enum JobErrorMsg implements CommonErrorEnum {
    JOB_VALID_ERROR(ERROR, "job-error.valid-error", "job-show-error.valid-error"),
    JOB_PAUSED_ERROR(ERROR, "job-error.paused-error", "job-show-error.paused-error"),
    JOB_OBTAIN_EXEC_LIST_ERROR(ERROR, "job-error.obtain-exec-list-error", "job-show-error.obtain-exec-list-error"),
    JOB_RESUME_ERROR(ERROR, "job-error.resume-error", "job-show-error.resume-error"),
    JOB_CHECK_ERROR(ERROR, "job-error.check-error", "job-show-error.check-error"),
    JOB_DELETE_ERROR(ERROR, "job-error.delete-error", "job-show-error.delete-error"),
    JOB_EXEC_ERROR(ERROR, "job-error.exec-error", "job-show-error.exec-error"),
    JOB_NAME_IS_NOT_BLANK(ERROR, "job-error.name-is-not-blank", "job-show-error.name-is-not-blank"),
    JOB_NOT_REGISTER(ERROR, "job-error.not-register", "job-show-error.not-register"),
    JOB_NOT_SUPPORT_MISFIRE_POLICY(ERROR, "job-error.not-support-misfire-policy", "job-show-error.not-support-misfire-policy"),
    JOB_CRON_IS_NOT_BLANK(ERROR, "job-error.cron-is-not-blank", "job-show-error.cron-is-not-blank"),
    JOB_CRON_IS_FORMAT_ERROR(ERROR, "job-error.cron-is-format-error", "job-show-error.cron-is-format-error"),
    ;
    private final int code;
    private final String desc;
    private final String showMsg;
}
