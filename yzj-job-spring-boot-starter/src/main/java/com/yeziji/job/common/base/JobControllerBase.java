package com.yeziji.job.common.base;

import com.yeziji.job.business.jobInfo.base.JobInfo;
import com.yeziji.job.business.jobInfo.service.JobInfoService;
import lombok.extern.slf4j.Slf4j;

/**
 * 规范 job 控制器信息
 *
 * @author hwy
 * @since 2024/05/09 14:56
 **/
@Slf4j
public abstract class JobControllerBase {
    public final JobInfoService jobInfoService;

    public JobControllerBase(JobInfoService jobInfoService) {
        this.jobInfoService = jobInfoService;
    }

    public boolean create(JobInfo jobInfo) {
        return jobInfoService.create(jobInfo);
    }

    public boolean update(JobInfo jobInfo) {
        return jobInfoService.update(jobInfo);
    }

    public boolean run(JobInfo jobInfo) {
        try {
            log.info("开始执行任务：{}", jobInfo);
            return jobInfoService.run(jobInfo);
        } catch (Exception e) {
            log.error("执行 {} 异常: {}", jobInfo, e.getMessage(), e);
        }
        return false;
    }

    public boolean paused(Long id) {
        return jobInfoService.paused(id);
    }

    public boolean resume(Long id) {
        return jobInfoService.resume(id);
    }

    public boolean delete(Long id) {
        return jobInfoService.delete(id);
    }
}
