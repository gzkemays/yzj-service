package com.yeziji.job.common.base;

import cn.hutool.core.util.StrUtil;
import com.yeziji.job.business.jobInfo.base.JobInfo;
import com.yeziji.utils.expansion.MessageFormat2;
import com.yeziji.utils.expansion.Str2;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.quartz.Trigger;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.io.Serializable;
import java.util.Date;
import java.util.StringJoiner;

/**
 * 任务上下文信息基础对象
 *
 * @author hwy
 * @since 2024/06/28 9:38
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class JobContextInfo implements Serializable {
    private static final long serialVersionUID = -6455951325977991823L;
    
    /**
     * 任务上下文标识
     */
    private String flag;

    /**
     * 触发 IP
     */
    private String triggerIp;

    /**
     * 日志信息
     */
    private String logMsg;

    /**
     * 错误信息
     */
    private String exceptionMsg;

    /**
     * 任务状态
     * @see Trigger.TriggerState
     */
    private Trigger.TriggerState status;

    /**
     * 任务异常重试次数
     */
    private Integer retryCount;

    /**
     * 执行时间
     */
    private Date executeTime;

    /**
     * 任务信息
     */
    private JobInfo jobInfo;

    /**
     * 追加日志信息
     *
     * @param msg  日志信息
     * @param args 日志信息...
     */
    public void appendLog(String msg, Object... args) {
        if (StrUtil.isBlank(msg) && args.length == 0) {
            return;
        }

        StringJoiner sj = new StringJoiner("\n");
        // 继承之前的信息
        Str2.notBlankThen(this.logMsg, sj::add);
        sj.add(MessageFormat2.formatTuple(msg, args));

        // update
        this.logMsg = sj.toString();
    }

    /**
     * 追加异常信息
     *
     * @param msg  日志信息
     * @param args 日志信息...
     */
    public void appendExceptionLog(String msg, Object... args) {
        if (StrUtil.isBlank(msg) && args.length == 0) {
            return;
        }

        StringJoiner sj = new StringJoiner("\n");
        // 继承之前的信息
        Str2.notBlankThen(this.exceptionMsg, sj::add);
        sj.add(MessageFormat2.formatTuple(msg, args));

        // update
        this.exceptionMsg = sj.toString();
    }

    /**
     * 追加异常信息
     *
     * @param ex 异常信息
     */
    public void appendException(Exception ex) {
        if (ex == null) {
            return;
        }

        StringJoiner sj = new StringJoiner("\n");
        // 继承之前的信息
        Str2.notBlankThen(this.exceptionMsg, sj::add);
        // 异常信息
        String message = Str2.nullEmpty(ex.getMessage());
        sj.add("job error message: " + message);
        // 异常栈信息
        StackTraceElement[] stackTrace = ex.getStackTrace();
        if (stackTrace.length > 0) {
            for (int i = 0; i < stackTrace.length; i++) {
                if (i == 0) {
                    sj.add("job error stack trace: " + stackTrace[0].toString());
                }
                sj.add(stackTrace[i].toString());
            }
        }
        // update
        this.exceptionMsg = sj.toString();
    }

    public static void main(String[] args) {
        FormattingTuple formattingTuple = MessageFormatter.arrayFormat("ni hao: {}", new String[]{"11", "12"});
        System.out.println("formattingTuple = " + formattingTuple.getMessage());
    }
}
