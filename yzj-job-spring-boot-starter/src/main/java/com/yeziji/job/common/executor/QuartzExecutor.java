package com.yeziji.job.common.executor;

import com.yeziji.job.common.base.QuartzJobExecutorBase;
import org.quartz.UnableToInterruptJobException;

/**
 * 允许并发执行任务处理器
 *
 * @author hwy
 * @since 2024/05/08 15:42
 **/
public class QuartzExecutor extends QuartzJobExecutorBase {
    @Override
    public void interrupt() throws UnableToInterruptJobException {

    }
}
