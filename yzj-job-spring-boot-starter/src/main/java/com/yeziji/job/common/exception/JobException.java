package com.yeziji.job.common.exception;

import com.yeziji.common.CommonErrorEnum;
import com.yeziji.common.CommonErrorMsg;
import com.yeziji.job.common.msg.JobErrorMsg;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * Job exception
 *
 * @author hwy
 * @since 2024/05/09 11:18
 **/
@Getter
@NoArgsConstructor
public class JobException extends RuntimeException {
    private static final long serialVersionUID = -1130061000164667193L;
    
    private JobErrorMsg errorEnum;

    public JobException(JobErrorMsg errorEnum) {
        super(Optional.ofNullable(errorEnum).map(CommonErrorEnum::getLangDesc).orElse(CommonErrorMsg.SYSTEM_UNKNOWN_EXCEPTION.getLangDesc()));
        this.errorEnum = errorEnum;
    }

    public JobException(JobErrorMsg errorEnum, String message) {
        super(message);
        this.errorEnum = errorEnum;
    }

    public JobException(String message) {
        super(message);
    }

    public JobException(String message, Throwable cause) {
        super(message, cause);
    }

    public JobException(Throwable cause) {
        super(cause);
    }

    public JobException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public JobException(JobErrorMsg errorEnum, Throwable cause) {
        // lightweight exception (fill in stack trace)
        super(errorEnum.getLangDesc(), cause, false, true);
    }
}
