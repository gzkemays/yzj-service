package com.yeziji.job.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * job 信息支撑器
 *
 * @author gzkemays
 * @since 2024/05/08 2:14
 */
public class JobHolder {
    /**
     * 保存任务 map
     */
    private static final Map<String, Class<?>> JOB_MAP = new ConcurrentHashMap<>();

    public static void put(String jobName, Class<?> jobClass) {
        JOB_MAP.put(jobName, jobClass);
    }

    public static Class<?> get(String jobName) {
        return JOB_MAP.get(jobName);
    }

    public static boolean contain(String jobName) {
        return JOB_MAP.containsKey(jobName);
    }


    public static int size() {
        return JOB_MAP.size();
    }
}
