package com.yeziji.job.common.executor;

import com.yeziji.job.common.base.QuartzJobExecutorBase;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.UnableToInterruptJobException;

/**
 * 非并发执行任务处理器
 *
 * @author hwy
 * @since 2024/05/08 15:40
 **/
@DisallowConcurrentExecution
public class QuartzDisallowConcurrentExecutor extends QuartzJobExecutorBase {
    @Override
    public void interrupt() throws UnableToInterruptJobException {

    }
}
