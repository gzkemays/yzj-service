package com.yeziji.job.annotation;

import java.lang.annotation.*;

/**
 * 标记为 job 的 spring 容器
 *
 * @author hwy
 * @since 2024/05/08 18:12
 **/
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface YzjJobBean {
    String value() default "";
}
