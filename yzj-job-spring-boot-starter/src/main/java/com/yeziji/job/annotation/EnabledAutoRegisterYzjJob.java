package com.yeziji.job.annotation;

import com.yeziji.job.config.support.handler.AutoRegisterJobHandler;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 定义执行的方法
 *
 * @author hwy
 * @since 2024/05/08 17:23
 **/
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({AutoRegisterJobHandler.class})
public @interface EnabledAutoRegisterYzjJob {
    /**
     * 兼容 default setter
     */
    String[] value() default {};

    /**
     * 扫描的路径
     * <p>如果为空默认扫描全部包</p>
     */
    String[] basePackages() default {};
}
