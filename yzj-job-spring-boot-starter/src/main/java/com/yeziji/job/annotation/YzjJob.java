package com.yeziji.job.annotation;

import java.lang.annotation.*;

/**
 * 标记任务方法
 *
 * @author hwy
 * @since 2024/05/08 17:29
 **/
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface YzjJob {
    /**
     * 任务名称
     */
    String value();
}
