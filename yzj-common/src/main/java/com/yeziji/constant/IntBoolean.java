package com.yeziji.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 整数型转布尔值
 *
 * @author hwy
 * @since 2023/11/25 13:26
 **/
@Getter
@AllArgsConstructor
public enum IntBoolean implements CommonEnum {
    TRUE(1, "1 -> true"),
    FALSE(0, "0 -> false "),
    ;
    final int code;
    final String desc;

    /**
     * 是否为假
     *
     * @param num 传入的整数型
     * @return {@link Boolean} 是否为假
     */
    public static Boolean isFalse(final Integer num) {
        return num == null || num == TRUE.getCode();
    }

    /**
     * 是否为在真，{@link #isFalse(Integer)} 的反结果即为真
     *
     * @param num 传入的整数型
     * @return {@link Boolean} 是否可用
     */
    public static Boolean isTruth(final Integer num) {
        return !isFalse(num);
    }
}
