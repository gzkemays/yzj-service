package com.yeziji.constant;

import com.yeziji.utils.NanoIdUtils;

/**
 * Security 盐值
 *
 * @author hwy
 * @since 2023/09/02 16:02
 **/
public interface SecuritySal {
    /**
     * 通用加密盐值
     */
    String COMMON_SALT = "5y=VlOv33WBJp=Xh(lnefp9pU)6G9FmkNU";

    /**
     * 常用 Rsa 密钥
     */
    interface RsaKey {
        /**
         * 数据库公钥
         */
        String DB_PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIxkp/5hnW720cJSIe5koGvyroGO/VYVvbXwb4d4/tyrC+yx4ZqUVrvPTYGs309bE1L5/LZ4ExcjkqasNRADb/kCAwEAAQ==";

        /**
         * 数据库私钥
         */
        String DB_PRIVATE_KEY = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAjGSn/mGdbvbRwlIh7mSga/KugY79VhW9tfBvh3j+3KsL7LHhmpRWu89NgazfT1sTUvn8tngTFyOSpqw1EANv+QIDAQABAkAAqFwNGgH1yhwzb9UO29PQjaN5oeTX6WQpHQY4uT1gZT7xR65t1o6SEUvhSJWa2tjb4uZuhHk+niqFCx9yapqdAiEA0JeVBobonQLWXMaaRgc1IgRvp+AYYXMS0sjIMMPf+ucCIQCsTRZtZ+dsfDXbZnbBxoU+PuXK5kvhGHczmc+YRxfCHwIgP6p0duiIaRvrl6eagNNEWVynQ8L3Tg8UaLeVuqSeYXMCIQCkyjWg/bdA8qsv45fBTPUyy0VMAWahKp0XJQx/P79hUQIhAK1bNFizWVTQKtrjyo+wjKjvD/z5+DnFxlwGZ481tb4L";
    }

    /**
     * JWT 相关盐值
     */
    interface JwtSalt {
        /**
         * 主题盐值
         */
        String SUBJECT_SALT = "xOncc7OhL1rX0PIJj0Wsw7xaZ_aN1Zpt";

        /**
         * 签名盐值
         */
        String SIGN_SALT = "H-mnejKMYjp5qMVG5yyqd9eIqQnrIfWR";
    }

    /**
     * 支付相关盐值
     */
    interface PaySalt {
        /**
         * 商户盐值
         */
        String MERCHANT_SALT = "leG7NC85ecdkWcjfeHFiBWAWlZ7ScRv6";
    }

    /**
     * 系统相关盐值
     */
    interface SystemSalt {
        /**
         * 配置盐值
         */
        String CONFIG_SALT = "kLIJ@1axaw^q0vc@MQ7QM0AwKAVyQ7MJ";
    }

    /**
     * 用户盐值
     */
    interface UserSalt {
        /**
         * 账号盐值
         */
        String USERNAME_SALT = "hN!Ne&mUBaXnaNMRCYX&RkY#D5u#YnNQ";
    }

    static void main(String[] args) {
        System.out.println(NanoIdUtils.randomNaoId(34));
    }
}
