package com.yeziji.constant;

import java.util.Set;

/**
 * 各样固定死的字符串池
 *
 * @author hwy
 * @since 2023/11/12 17:13
 **/
public interface VariousStrPool {
    /**
     * 常用的空字符串
     */
    String EMPTY = "";

    /**
     * 常用的默认时间格式
     */
    String DEFAULT_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 词典
     */
    interface Dictionary {
        /**
         * 主程序应用
         */
        String APPLICATION = "application";

        /**
         * 默认
         */
        String DEFAULT = "default";

        /**
         * 未知的
         */
        String UNKNOWN = "unknown";
    }

    /**
     * 系统各类通用的参数
     */
    interface System {
        /**
         * 栈 id
         */
        String TRACE_ID = "traceId";

        /**
         * 获取用户的当前工作目录
         */
        String USER_DIR = "user.dir";

        /**
         * Java 运行时环境版本
         */
        String JAVA_VERSION = "java.version";

        /**
         * Java 运行时环境供应商
         */
        String JAVA_VENDOR = "java.vendor";

        /**
         * Java 供应商的 URL
         */
        String JAVA_VENDOR_URL = "java.vendor.url";

        /**
         * Java 安装目录
         */
        String JAVA_HOME = "java.home";

        /**
         * Java 虚拟机规范版本
         */
        String JAVA_VM_SPECIFICATION_VERSION = "java.vm.specification.version";

        /**
         * Java 虚拟机规范供应商
         */
        String JAVA_VM_SPECIFICATION_VENDOR = "java.vm.specification.vendor";

        /**
         * Java 虚拟机规范名称
         */
        String JAVA_VM_SPECIFICATION_NAME = "java.vm.specification.name";

        /**
         * Java 虚拟机实现版本
         */
        String JAVA_VM_VERSION = "java.vm.version";

        /**
         * Java 虚拟机实现供应商
         */
        String JAVA_VM_VENDOR = "java.vm.vendor";

        /**
         * Java 虚拟机实现名称
         */
        String JAVA_VM_NAME = "java.vm.name";

        /**
         * Java 运行时环境规范版本
         */
        String JAVA_SPECIFICATION_VERSION = "java.specification.version";

        /**
         * Java 运行时环境规范供应商
         */
        String JAVA_SPECIFICATION_VENDOR = "java.specification.vendor";

        /**
         * Java 运行时环境规范名称
         */
        String JAVA_SPECIFICATION_NAME = "java.specification.name";

        /**
         * Java 类格式版本号
         */
        String JAVA_CLASS_VERSION = "java.class.version";

        /**
         * Java 类路径
         */
        String JAVA_CLASS_PATH = "java.class.path";

        /**
         * 加载库时搜索的路径列表
         */
        String JAVA_LIBRARY_PATH = "java.library.path";

        /**
         * 默认的临时文件路径
         */
        String JAVA_IO_TMPDIR = "java.io.tmpdir";

        /**
         * 要使用的 JIT 编译器的名称
         */
        String JAVA_COMPILER = "java.compiler";

        /**
         * 一个或多个扩展目录的路径
         */
        String JAVA_EXT_DIRS = "java.ext.dirs";

        /**
         * 操作系统的名称
         */
        String OS_NAME = "os.name";

        /**
         * 操作系统的架构
         */
        String OS_ARCH = "os.arch";

        /**
         * 操作系统的版本
         */
        String OS_VERSION = "os.version";

        /**
         * 文件分隔符（在 UNIX 系统中是“/”）
         */
        String FILE_SEPARATOR = "file.separator";

        /**
         * 路径分隔符（在 UNIX 系统中是“:”）
         */
        String PATH_SEPARATOR = "path.separator";

        /**
         * 行分隔符（在 UNIX 系统中是“/n”）
         */
        String LINE_SEPARATOR = "line.separator";

        /**
         * 用户的账户名称
         */
        String USER_NAME = "user.name";

        /**
         * 用户的主目录
         */
        String USER_HOME = "user.home";
    }

    interface MapKey {
        String JDBC_URL = "jdbc-url";
        String JDBC_USERNAME = "jdbc-username";
        String JDBC_PASSWORD = "jdbc-pwd";
    }

    interface Time {
        /**
         * 毫秒
         */
        String MILLIS = "millis";

        /**
         * 秒
         */
        String SECONDS = "seconds";

        /**
         * 分钟
         */
        String MINUTE = "minute";

        /**
         * 小时
         */
        String HOUR = "hour";

        /**
         * 毫秒简写
         */
        String EZ_MILLIS = "ms";

        /**
         * 秒简写
         */
        String EZ_SECONDS = "s";

        /**
         * 分钟简写
         */
        String EZ_MINUTE = "min";

        /**
         * 小时简写
         */
        String EZ_HOUR = "h";
    }

    /**
     * 請求頭
     */
    interface HttpHeaders {
        /**
         * 刷新令牌
         */
        String REFRESH_AUTHORIZATION = "Refresh-Authorization";
        /**
         * 令牌
         */
        String AUTHORIZATION = "Authorization";
        /**
         * 各类 IP 代理的请求头
         */
        String X_FORWARDED_FOR = "X-Forwarded-For";
        String PROXY_CLIENT_IP = "Proxy-Client-IP";
        String WL_PROXY_CLIENT_IP = "WL-Proxy-Client-IP";
        String HTTP_CLIENT_IP = "HTTP_CLIENT_IP";
        String X_REAL_IP = "X-Real-IP";
        String LANGUAGE = "language";
    }

    /**
     * 常见的媒体后缀
     */
    interface NormalMediaFileExt {
        String PERIOD_MP3 = ".mp3";
        String MP3 = "mp3";
        String PERIOD_MP4 = ".mp4";
        String MP4 = "mp4";
        String PERIOD_JPEG = ".jpeg";
        String JPEG = "jpeg";
        String PERIOD_PNG = ".png";
        String PNG = "png";
        String PERIOD_GIF = ".gif";
        String GIF = "gif";
        String PERIOD_AVI = ".avi";
        String AVI = "avi";
        String PERIOD_JPG = ".jpg";
        String JPG = "jpg";
        Set<String> PICTURES_EXT = Set.of(PERIOD_JPG, JPG, PERIOD_PNG, PNG, PERIOD_GIF, GIF, PERIOD_JPEG, JPEG);
    }
}
