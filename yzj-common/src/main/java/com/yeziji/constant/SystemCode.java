package com.yeziji.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态码枚举
 *
 * @author hwy
 * @since 2023/10/17 0:48
 **/
@Getter
@AllArgsConstructor
public enum SystemCode implements CommonEnum {
    SUCCESS(CommonEnum.SUCCESS, "system-success.response"),
    ERROR(CommonEnum.ERROR, "system-error.throw-exception"),
    UNAUTHORIZED(401, "system-error.unauthorized"),
    FORBIDDEN(403, "system-error.forbidden"),
    PAGE_NOT_FOUND(404, "system-error.page-is-not-found"),
    VALIDATE_FAILED(-1, "system-error.validate-params-failed"),
    TOKEN_ERROR(-999, "system-error.token-exception");

    /**
     * 状态码
     */
    final int code;

    /**
     * 状态码描述信息
     */
    final String desc;

    public static Integer getSuccessCode() {
        return SystemCode.SUCCESS.getCode();
    }

    public static Integer getErrorCode() {
        return SystemCode.ERROR.getCode();
    }

    public static Integer getTokenErrorCode() {
        return SystemCode.TOKEN_ERROR.getCode();
    }
}
