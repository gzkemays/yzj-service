package com.yeziji.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 缓存策略
 *
 * @author hwy
 * @since 2024/07/09 20:20
 **/
@Getter
@AllArgsConstructor
public enum CacheStrategy implements CommonEnum {
    NOT_LOCK(0, "无锁读写"),
    TRY_LOCK(1, "有锁读写(防击穿)"),
    ;

    private final int code;
    private final String desc;
}
