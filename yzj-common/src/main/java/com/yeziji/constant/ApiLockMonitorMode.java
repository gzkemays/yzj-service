package com.yeziji.constant;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Api 锁模式
 *
 * @author hwy
 * @since 2024/02/02 20:33
 **/
@Getter
@AllArgsConstructor
public enum ApiLockMonitorMode implements CommonEnum {
    NONE(-1, "不指定监听"),
    IP(0, "监听 ip"),
    USER_ID(1, "监听用户 id"),
    PARAMS(2, "监听入参参数"),
    ;

    private final int code;
    private final String desc;
}
