package com.yeziji.excel;

import com.yeziji.exception.ApiException;

/**
 * 自定义 easy excel 异常
 *
 * @author hwy
 * @since 2023/07/04 17:03
 **/
public class EasyExcelException extends ApiException {
    private static final long serialVersionUID = 3642027147454585164L;

    public EasyExcelException() {
        super();
    }

    public EasyExcelException(String message) {
        super(message);
    }

    public EasyExcelException(String message, Throwable cause) {
        super(message, cause);
    }

    public EasyExcelException(Throwable cause) {
        super(cause);
    }
}
