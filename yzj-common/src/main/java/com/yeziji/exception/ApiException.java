package com.yeziji.exception;

import com.yeziji.common.CommonEnum;
import com.yeziji.common.CommonErrorEnum;
import com.yeziji.common.CommonErrorMsg;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * api 异常
 *
 * @author hwy
 * @since 2023/08/30 0:35
 **/
@Getter
@NoArgsConstructor
public class ApiException extends RuntimeException {
    private static final long serialVersionUID = -2730950349025186221L;
    /**
     * 错误枚举
     */
    private CommonEnum errorEnum;

    public ApiException(CommonEnum errorEnum) {
        super(Optional.ofNullable(errorEnum).map(CommonEnum::getLangDesc).orElse(CommonErrorMsg.SYSTEM_UNKNOWN_EXCEPTION.getLangDesc()));
        this.errorEnum = errorEnum;
    }

    public ApiException(CommonErrorEnum errorEnum) {
        super(Optional.ofNullable(errorEnum).map(CommonErrorEnum::getShowLangMsg).orElse("未知异常"));
        this.errorEnum = errorEnum;
    }

    public ApiException(CommonEnum errorEnum, String message) {
        super(message);
        this.errorEnum = errorEnum;
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
