package com.yeziji.service.cache;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * redis 服务
 *
 * @author hwy
 * @since 2023/11/26 17:23
 **/
public interface RedisService {
    /**
     * 上锁并重试
     *
     * @param key         键值
     * @param expire      过期时间
     * @param retryCount  重试次数
     * @param sleepMillis 每次重试的等待时间
     * @return {@link Boolean} 是否上锁成功
     */
    boolean lock(String key, long expire, int retryCount, long sleepMillis);

    /**
     * 执行 lua 脚本释放锁
     *
     * @param key 释放键值
     * @return {@link Boolean} 是否释放成功
     */
    boolean releaseLock(String key);

    /**
     * 保存緩存
     *
     * @param key      键值
     * @param value    保存值
     * @param expire   过期时间
     * @param timeUnit 时间单位
     */
    void set(String key, Object value, long expire, TimeUnit timeUnit);

    /**
     * hash 缓存
     *
     * @param key      目录键值
     * @param hashKey  哈希键值
     * @param value    数据
     * @param expire   过期时间
     * @param timeUnit 过期单位
     */
    void setForHash(String key, String hashKey, Object value, long expire, TimeUnit timeUnit);

    /**
     * 删除指定缓存
     *
     * @param key 键值
     */
    void remove(String key);

    /**
     * 批量删除缓存
     *
     * @param keys 键值
     */
    void remove(Collection<String> keys);

    /**
     * 删除哈希缓存中的指定缓存
     *
     * @param key     键值
     * @param hashKey 哈希键值
     */
    void removeForHash(String key, String hashKey);

    /**
     * 获取值
     *
     * @param key 键值
     * @param <T> 泛型
     * @return 返回泛型对象
     */
    <T> T get(String key);

    /**
     * 获取不存在就执行 supplier 进行保存并获取
     *
     * @param key      键值
     * @param expire   过期时间
     * @param timeUnit 过期单位
     * @param supplier 覆盖函数
     * @param <T>      泛型
     * @return 返回泛型对象
     */
    <T> T getIf(String key, long expire, TimeUnit timeUnit, Supplier<T> supplier);

    /**
     * 获取 hash 数据
     *
     * @param key     目录键值
     * @param hashKey 哈希键值
     * @param <T>     泛型
     * @return 返回泛型对象
     */
    <T> T getByHash(String key, String hashKey);

    /**
     * 获取 key 下的 hash map
     *
     * @param key 目录键值
     * @param <T> 返回泛型
     * @return {@link Map} key: hashKey, value: 值
     */
    <K, T> Map<K, T> mapByKey(String key);

    /**
     * 获取不存在就执行 supplier 进行保存并获取
     *
     * @param key      键值
     * @param hashKey  哈希键值
     * @param expire   过期时间
     * @param timeUnit 过期单位
     * @param supplier 覆盖函数
     * @param <T>      泛型
     * @return 返回泛型对象
     */
    <T> T getByHashIf(String key, String hashKey, long expire, TimeUnit timeUnit, Supplier<T> supplier);

    /**
     * 是否存在缓存 key
     *
     * @param key 缓存 key
     * @return {@link Boolean} 存在结果
     */
    boolean isHasKey(String key);

    /**
     * 是否存在缓存 hash key
     *
     * @param key     缓存 key
     * @param hashKey 哈希键值
     * @return {@link Boolean} 存在结果
     */
    boolean isHasHashKey(String key, String hashKey);

    /**
     * 异步 保存緩存
     *
     * @param key      键值
     * @param value    保存值
     * @param expire   过期时间
     * @param timeUnit 时间单位
     */
    void asyncSet(String key, Object value, long expire, TimeUnit timeUnit, ThreadPoolTaskExecutor threadPoolTaskExecutor);

    /**
     * 异步 hash 缓存
     *
     * @param key      目录键值
     * @param hashKey  哈希键值
     * @param value    数据
     * @param expire   过期时间
     * @param timeUnit 过期单位
     */
    void asyncSetForHash(String key, String hashKey, Object value, long expire, TimeUnit timeUnit, ThreadPoolTaskExecutor threadPoolTaskExecutor);

    /**
     * 异步 删除指定缓存
     *
     * @param key 键值
     */
    void asyncRemove(String key, ThreadPoolTaskExecutor threadPoolTaskExecutor);

    /**
     * 异步 删除哈希缓存中的指定缓存
     *
     * @param key     键值
     * @param hashKey 哈希键值
     */
    void asyncRemoveForHash(String key, String hashKey, ThreadPoolTaskExecutor threadPoolTaskExecutor);

    /**
     * 获取 key 的剩余时间
     *
     * @param key 键值
     * @return {@link Long} 剩余时间
     */
    long getExpireTime(String key);

    /**
     * 获取列表
     *
     * @param key 列表键值
     * @param <T> 任意泛型对象
     * @return {@link List}
     */
    <T> List<T> list(String key);

    /**
     * 加到列表右侧
     *
     * @param key   列表 key
     * @param value 加入的参数
     */
    void rpush(String key, Object value);

    /**
     * 加到列表左侧
     *
     * @param key   列表 key
     * @param value 加入的参数
     */
    void lpush(String key, Object value);

    /**
     * 右出栈
     *
     * @param key 出栈 key
     */
    <T> T rpop(String key);

    /**
     * 左出栈
     *
     * @param key 出栈 key
     */
    <T> T lpop(String key);
}
