package com.yeziji.service.cache.impl;

import cn.hutool.core.map.MapUtil;
import com.yeziji.service.cache.RedisService;
import com.yeziji.utils.NanoIdUtils;
import com.yeziji.utils.ThreadPoolUtils;
import com.yeziji.utils.expansion.Opt2;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * redis 服务实现类
 *
 * @author hwy
 * @since 2023/11/26 17:23
 **/
@Slf4j
@Component
public class RedisServiceImpl implements RedisService {
    /**
     * 官方提供的Lua解锁脚本
     * <p><a href="http://redisdoc.com/string/set.html"> 官方文档 </a></p>
     */
    private final static String UNLOCK_LUA_SCRIPT = "if redis.call(\"get\",KEYS[1]) == ARGV[1] " +
            "then " +
            "    return redis.call(\"del\",KEYS[1]) " +
            "else " +
            "    return 0 " +
            "end ";
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public boolean lock(String key, long expire, int retryCount, long sleepMillis) {
        boolean setResult = setRedisLock(key, expire);
        while ((!setResult) && retryCount-- > 0) {
            setResult = setRedisLock(key, expire);
            if (!setResult) {
                try {
                    log.info("lock failed, retrying...{}", retryCount);
                    Thread.sleep(sleepMillis);
                } catch (InterruptedException e) {
                    return false;
                }
            } else {
                return true;
            }
        }
        return setResult;
    }

    @Override
    public boolean releaseLock(String key) {
        // 释放锁的时候，有可能因为持锁之后方法执行时间大于锁的有效期，此时有可能已经被另外一个线程持有锁，所以不能直接删除
        try {
            Object releaseValue = this.get(key);
            if (releaseValue != null && StringUtils.isBlank(releaseValue.toString())) {
                return false;
            }
            List<String> keys = new ArrayList<>();
            keys.add(key);
            // 使用 lua 脚本删除 redis 中匹配 value 的 key，可以避免由于方法执行时间过长而 redis 锁自动过期失效的时候误删其他线程的锁
            RedisScript<Boolean> script = new DefaultRedisScript<>(UNLOCK_LUA_SCRIPT, Boolean.class);
            Boolean execute = redisTemplate.execute(script, keys, releaseValue);
            return execute != null && execute;
        } catch (Exception e) {
            log.error("release lock occured an exception", e);
        }
        return false;
    }

    @Override
    public void set(String key, Object value, long expire, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, expire, timeUnit);
    }

    @Override
    public void setForHash(String key, String hashKey, Object value, long expire, TimeUnit timeUnit) {
        redisTemplate.opsForHash().put(key, hashKey, value);
        redisTemplate.expire(key, expire, timeUnit);
    }

    @Override
    public void remove(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public void remove(Collection<String> keys) {
        redisTemplate.delete(keys);
    }

    @Override
    public void removeForHash(String key, String hashKey) {
        redisTemplate.opsForHash().delete(key, hashKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) redisTemplate.opsForValue().get(key);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getIf(String key, long expire, TimeUnit timeUnit, Supplier<T> supplier) {
        Object obj = this.get(key);
        if (obj == null) {
            T data = supplier.get();
            set(key, data, expire, timeUnit);
            return data;
        }
        return (T) obj;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getByHash(String key, String hashKey) {
        return (T) redisTemplate.opsForHash().get(key, hashKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <K, T> Map<K, T> mapByKey(String key) {
        Map<Object, Object> entries = redisTemplate.opsForHash().entries(key);
        if (MapUtil.isEmpty(entries)) {
            return MapUtil.newHashMap();
        }
        return entries.entrySet().stream()
                .filter(entry -> {
                    K eK = this.<K>getOrNull(entry.getKey());
                    T eV = this.<T>getOrNull(entry.getValue());
                    return eK != null && eV != null;
                })
                .collect(
                        Collectors.toMap(
                                entry -> (K) entry.getKey(),
                                entry -> (T) entry.getValue()
                        )
                );
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getByHashIf(String key, String hashKey, long expire, TimeUnit timeUnit, Supplier<T> supplier) {
        Object obj = this.getByHash(key, hashKey);
        if (obj == null) {
            T data = supplier.get();
            setForHash(key, hashKey, data, expire, timeUnit);
            return data;
        }
        return (T) obj;
    }

    @Override
    public boolean isHasKey(String key) {
        return Boolean.TRUE.equals(redisTemplate.hasKey(key));
    }

    @Override
    public boolean isHasHashKey(String key, String hashKey) {
        return redisTemplate.opsForHash().get(key, hashKey) != null;
    }

    @Override
    public void asyncSet(String key, Object value, long expire, TimeUnit timeUnit, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        ThreadPoolUtils.execute(() -> set(key, value, expire, timeUnit), threadPoolTaskExecutor);
    }

    @Override
    public void asyncSetForHash(String key, String hashKey, Object value, long expire, TimeUnit timeUnit, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        ThreadPoolUtils.execute(() -> setForHash(key, hashKey, value, expire, timeUnit), threadPoolTaskExecutor);
    }

    @Override
    public void asyncRemove(String key, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        ThreadPoolUtils.execute(() -> remove(key), threadPoolTaskExecutor);
    }

    @Override
    public void asyncRemoveForHash(String key, String hashKey, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        ThreadPoolUtils.execute(() -> removeForHash(key, hashKey), threadPoolTaskExecutor);
    }

    @Override
    public long getExpireTime(String key) {
        return Optional.ofNullable(redisTemplate.getExpire(key)).orElse(0L);
    }

    @Override
    public <T> List<T> list(String key) {
        List<Object> range = Opt2.nullElse(redisTemplate.opsForList().range(key, 0, -1), new ArrayList<>());
        return range.stream()
                .map(obj ->
                        Optional.ofNullable(this.getOrNull(obj)).map(o -> (T) o).orElse(null)
                ).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public void rpush(String key, Object value) {
        redisTemplate.opsForList().rightPush(key, value);
    }

    @Override
    public void lpush(String key, Object value) {
        redisTemplate.opsForList().leftPush(key, value);
    }

    @Override
    public <T> T rpop(String key) {
        return this.getOrNull(redisTemplate.opsForList().rightPop(key));
    }

    @Override
    public <T> T lpop(String key) {
        return this.getOrNull(redisTemplate.opsForList().leftPop(key));
    }

    /**
     * 强转，失败则返回 null
     *
     * @param obj 任意对象
     * @return {@link T} 泛型对象
     * @param <T> 任意泛型
     */
    private <T> T getOrNull(Object obj) {
        try {
            return (T) obj;
        } catch (Exception e) {
            log.error("instance error: {}", e.getMessage());
        }
        return null;
    }

    /**
     * 追加锁
     *
     * @param key    键值
     * @param expire 过期时间
     * @return {@link Boolean} 是否追加成功，如果追加失败说明锁仍然被持有
     */
    private boolean setRedisLock(String key, long expire) {
        try {
            // 设置锁成功，返回'OK',否则返回null
            String naoId = NanoIdUtils.randomNaoId(18);
            return Boolean.TRUE.equals(redisTemplate.opsForValue().setIfAbsent(key, naoId, expire, TimeUnit.MILLISECONDS));
        } catch (Exception e) {
            log.error("set redis occured an exception", e);
        }
        return false;
    }
}
