package com.yeziji.common;

import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.service.IService;
import com.yeziji.utils.expansion.Asserts;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

/**
 * 自定义 flex IService 实现类
 *
 * @author hwy
 * @since 2023/11/29 13:37
 **/
public class IServiceImpl<M extends BaseMapper<T>, T extends CommonEntity> implements IService<T> {
    @Autowired
    protected M mapper;

    @Override
    public BaseMapper<T> getMapper() {
        return this.mapper;
    }

    /**
     * 根据 id 获取对象，如果对象不存在抛出指定的异常
     *
     * @param id             主键 id
     * @param commonErrorMsg 异常信息
     * @return {@link T} 实体类
     */
    public T getById(Serializable id, CommonErrorEnum commonErrorMsg) {
        return Asserts.notNull(this.getById(id), commonErrorMsg);
    }
}
