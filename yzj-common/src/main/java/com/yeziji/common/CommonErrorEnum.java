package com.yeziji.common;

import com.yeziji.utils.expansion.MessageFormat2;

/**
 * 公共异常信息
 *
 * @author hwy
 * @since 2023/09/01 18:05
 **/
public interface CommonErrorEnum extends CommonEnum {
    String UNKNOWN_SYSTEM_ERROR = "system-error.unknown-exception";
    String SYSTEM_ERROR = "system-error.throw-exception";
    String LOGIN_ERROR = "system-error.login-failed";
    String VALIDATE_FAILED = "system-error.validate-params-failed";
    String DATA_IS_NON_NULL = "system-error.params-is-not-found";
    String USER_INFO_EXCEPTION = "system-error.user-info-exception";
    String TOKEN_EXPIRED = "system-error.token-is-expired";
    String UNKNOWN_USER = "system-error.unknown-user";
    String FILE_SIZE_TOO_LARGE = "system-error.file-size-too-large";
    String UNAUTHORIZED = "system-error.unauthorized";
    String FORBIDDEN = "system-error.forbidden";

    /**
     * 异常枚举允许指定返回内容与系统日志内容不一致
     *
     * @return {@link String}
     */
    String getShowMsg();

    /**
     * 不同地区声明信息不同
     *
     * @return {@link String} 地区的声明信息
     */
    default String getShowLangMsg(Object... objects) {
        return MessageFormat2.language(this.getShowMsg(), objects);
    }
}
