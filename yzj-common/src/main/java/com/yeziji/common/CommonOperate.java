package com.yeziji.common;

import com.yeziji.utils.DataUtils;
import com.yeziji.utils.expansion.Lists2;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 通用操作对象
 *
 * @author hwy
 * @since 2023/09/22 22:47
 **/
@Data
@SuperBuilder
@NoArgsConstructor
public class CommonOperate {
    /**
     * 自增 id
     */
    @NotNull(message = "必须指定操作对象", groups = {UpdateOperate.class})
    protected Long id;

    /**
     * 删除与恢复数据
     */
    protected Boolean isDelete;

    /**
     * 判断是否存在主键
     */
    public boolean isHasId() {
        return this.id != null;
    }

    /**
     * 转换对象
     *
     * @param targetClass 目标类
     * @param <T>         T 目标泛型
     * @return {@link T} 指定的目标对象
     */
    public <T> T convert(@Nonnull Class<T> targetClass) {
        return DataUtils.convertBean(this, targetClass);
    }

    /**
     * 批量转换对象
     *
     * @param sourceList  原列表
     * @param targetClass 目标类
     * @param <O>         转换初始泛型
     * @param <T>         转换目标泛型
     * @return {@link List} 转换结果
     */
    public static <O, T> List<T> converts(List<O> sourceList, @Nonnull Class<T> targetClass) {
        if (Lists2.isEmpty(sourceList)) {
            return Lists2.empty();
        }

        return Lists2.transforms(sourceList, source -> DataUtils.convertBean(source, targetClass));
    }

    /**
     * 是否删除状态
     *
     * @return {@link Boolean}
     */
    public boolean isDelete() {
        return this.isDelete != null && this.isDelete;
    }

    interface UpdateOperate {
    }
}
