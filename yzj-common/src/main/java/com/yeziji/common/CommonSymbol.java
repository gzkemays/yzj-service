package com.yeziji.common;

/**
 * 公用符号
 *
 * @author gzkemays
 * @since 2022/1/18 23:25
 */
public interface CommonSymbol {
    String EMPTY = "";
    String SPACE = " ";
    String COMMA = ",";
    String CN_COMMA = "，";
    String FULL_STOP = ".";
    String CN_FULL_STOP = "。";
    String CAESURA_SIGN = ";";
    String CN_CAESURA_SIGN = "；";
    String COLON = ":";
    String CN_COLON = "：";
    String DOLLAR = "$";
    String CN_DOLLAR = "￥";
    String EXCLAMATORY_MARK = "!";
    String CN_EXCLAMATORY_MARK = "！";
    String NUMBER_SIGN = "#";
    String PIES_SIGN = "%";
    String TOP_ARROW = "^";
    String STAR_SIGN = "*";
    String DOUBLE_STAR_SIGN = "**";
    String AND_SYMBOL = "&";
    String RIGHT_BRACKETS = "(";
    String CN_RIGHT_BRACKETS = "（";
    String LEFT_BRACKETS = ")";
    String CN_LEFT_BRACKETS = "）";
    String MIDDLE_RIGHT_BRACKETS = "[";
    String CN_MIDDLE_RIGHT_BRACKETS = "【";
    String MIDDLE_LEFT_BRACKETS = "]";
    String CN_MIDDLE_LEFT_BRACKETS = "】";
    String BIG_RIGHT_BRACKETS = "{";
    String BIG_LEFT_BRACKETS = "{";
    String HORIZONTAL_BAR = "-";
    String CN_SINGLE_HORIZONTAL_BAR = "—";
    String CN_HORIZONTAL_BAR = "——";
    String BOTTOM_HORIZONTAL_BAR = "_";
    String EQUALS_SIGN = "=";
    String ADD_SIGN = "+";
    String SINGLE_COLON = "'";
    String DOUBLE_COLON = "\"";
    String CN_DOUBLE_RIGHT_COLON = "“";
    String CN_DOUBLE_LEFT_COLON = "”";
    String RIGHT_DIAGONAL_BAR = "\\";
    String LEFT_DIAGONAL_BAR = "/";
    String VERTICAL_BAR = "|";
    String LESS_THAN_SIGN = "<";
    String LEFT_ANGLE_QUOTES = "《";
    String RIGHT_ANGLE_QUOTES = "》";
    String GREATER_THAN_SIGN = ">";
    String TILDE = "~";
    String ESC_POINT = "`";
    String CN_ESC_POINT = "·";
    String QUESTION = "?";
    String UTF8 = "UTF-8";
    String GBK2312 = "GBK2312";
    String GB2312 = "GB2312";
    String LINE = "\\n";
}
