package com.yeziji.common;

import com.yeziji.constant.SystemCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 公共异常
 *
 * <p>除了特定的异常码，如令牌异常，其余都是未知异常 -1；401、403 是为了适配 security 的响应码；如果需要正常返回响应码，可以使用 {@link #SYSTEM_THROW_ERROR_BUT_SUCCESS}</p>
 *
 * @author hwy
 * @since 2023/09/01 21:05
 **/
@Getter
@AllArgsConstructor
public enum CommonErrorMsg implements CommonErrorEnum {
    SYSTEM_UNKNOWN_EXCEPTION(SystemCode.getSuccessCode(), UNKNOWN_SYSTEM_ERROR, UNKNOWN_SYSTEM_ERROR),
    SYSTEM_THROW_ERROR_BUT_SUCCESS(SystemCode.getSuccessCode(), SYSTEM_ERROR, "system-show-error.throw-exception"),
    SYSTEM_VALIDATE_FAILED(SystemCode.getErrorCode(), VALIDATE_FAILED, "system-show-error.validate-params-failed"),
    SYSTEM_THROW_ERROR(SystemCode.getErrorCode(), SYSTEM_ERROR, "system-show-error.throw-exception"),
    REQUIRED_DATA_IS_NULL(SystemCode.getErrorCode(), DATA_IS_NON_NULL, "system-show-error.params-is-not-found"),
    FILE_SIZE_IS_TOO_LARGE(SystemCode.getErrorCode(), FILE_SIZE_TOO_LARGE, "system-show-error.file-size-too-large"),
    UNKNOWN_LOGIN_STATUS(SystemCode.getErrorCode(), UNKNOWN_USER, "system-show-error.unknown-user"),
    TOKEN_IS_NULL(SystemCode.getTokenErrorCode(), USER_INFO_EXCEPTION, "system-show-error.user-info-exception"),
    TOKEN_IS_EXPIRED(SystemCode.getTokenErrorCode(), TOKEN_EXPIRED, "system-show-error.token-is-expired"),
    UNAUTHORIZED(SystemCode.UNAUTHORIZED.getCode(), SystemCode.UNAUTHORIZED.getDesc(), "system-show-error.unauthorized"),
    FORBIDDEN(SystemCode.FORBIDDEN.getCode(), SystemCode.FORBIDDEN.getDesc(), "system-show-error.forbidden"),
    PAGE_NOT_FOUND(SystemCode.PAGE_NOT_FOUND.getCode(), SystemCode.PAGE_NOT_FOUND.getDesc(), "system-show-error.page-is-not-found"),
    ;
    private final int code;
    private final String desc;
    private final String showMsg;
}
