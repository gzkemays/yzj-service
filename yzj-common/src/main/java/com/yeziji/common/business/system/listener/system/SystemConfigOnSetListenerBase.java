package com.yeziji.common.business.system.listener.system;

import com.mybatisflex.annotation.SetListener;
import com.yeziji.common.base.ListenerBase;
import com.yeziji.common.business.system.entity.SystemConfigEntity;
import com.yeziji.constant.SecuritySal;

/**
 * query set 监听器
 *
 * @author hwy
 * @since 2023/11/30 14:43
 **/
public class SystemConfigOnSetListenerBase extends ListenerBase<SystemConfigEntity> implements SetListener {
    @Override
    public Object onSet(Object data, String property, Object value) {
        return super.decryptValue(SystemConfigEntity.class, property, value, SecuritySal.SystemSalt.CONFIG_SALT);
    }
}
