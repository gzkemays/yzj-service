package com.yeziji.common.business.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.common.business.system.entity.SystemRolePermissionRelationEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统角色权限关联表 映射层。
 *
 * @author system
 * @since 2023-11-13
 */
@Mapper
public interface SystemRolePermissionRelationMapper extends BaseMapper<SystemRolePermissionRelationEntity> {

}
