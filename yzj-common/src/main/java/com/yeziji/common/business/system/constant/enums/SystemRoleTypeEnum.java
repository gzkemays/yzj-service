package com.yeziji.common.business.system.constant.enums;

import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Lists2;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统角色类型枚举
 *
 * @author hwy
 * @since 2023/12/10 16:53
 **/
@Getter
@AllArgsConstructor
public enum SystemRoleTypeEnum implements CommonEnum {
    PROHIBITED(-99, "虚拟无权角色"),
    VIRTUAL(-1, "虚拟角色"),
    ADMIN(0, "管理员用户"),
    NORMAL(1, "普通用户"),
    INSIDER(2, "内部用户"),
    MEMBER(3, "会员用户"),
    ;
    private final int code;
    private final String desc;

    public static SystemRoleTypeEnum getByDesc(String desc) {
        return Lists2.filterFirstOpt(values(), item -> item.getDesc().equals(desc)).orElse(null);
    }
}
