package com.yeziji.common.business.system.constant.enums;

import com.yeziji.common.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统邮箱类型
 *
 * @author hwy
 * @since 2023/12/05 17:00
 **/
@Getter
@AllArgsConstructor
public enum SystemEmailTypeEnum implements CommonEnum {
    REGISTER_VERIFY_CODE(0, "注册验证码",
            "<div style=\"width: 200px;border: 1px solid black;padding: 10px;position: relative;text-align: center;\">\n"
                    + "<span>\n"
                    + " 验证码：<span style=\"font-weight: 800;color:red;\"> #{CODE} </span>\n"
                    + "</span>\n"
                    + "</div>"),
    ;
    private final int code;
    private final String desc;
    private final String htmlText;

    /**
     * 获取验证码的 text
     *
     * @param code 指定验证码
     * @return 验证码 html 文本
     */
    public static String getVerifyCodeText(String code) {
        return REGISTER_VERIFY_CODE.htmlText.replace("#{CODE}", code);
    }
}
