package com.yeziji.common.business.system.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 * 系统角色权限关联表 表定义层。
 *
 * @author system
 * @since 2023-11-29
 */
public class SystemRolePermissionRelationTableDef extends TableDef {

    /**
     * 系统角色权限关联表
     */
    public static final SystemRolePermissionRelationTableDef SYSTEM_ROLE_PERMISSION_RELATION = new SystemRolePermissionRelationTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 角色 id
     */
    public final QueryColumn ROLE_ID = new QueryColumn(this, "role_id");

    /**
     * 删除标识
     */
    public final QueryColumn IS_DELETE = new QueryColumn(this, "is_delete");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 权限 id
     */
    public final QueryColumn PERMISSION_ID = new QueryColumn(this, "permission_id");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, ROLE_ID, PERMISSION_ID, IS_DELETE, CREATE_TIME, UPDATE_TIME};

    public SystemRolePermissionRelationTableDef() {
        super("", "system_role_permission_relation");
    }

}