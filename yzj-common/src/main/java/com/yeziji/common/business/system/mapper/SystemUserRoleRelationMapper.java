package com.yeziji.common.business.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.common.business.system.entity.SystemUserRoleRelationEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户角色关联表 映射层。
 *
 * @author system
 * @since 2023-11-13
 */
@Mapper
public interface SystemUserRoleRelationMapper extends BaseMapper<SystemUserRoleRelationEntity> {

}
