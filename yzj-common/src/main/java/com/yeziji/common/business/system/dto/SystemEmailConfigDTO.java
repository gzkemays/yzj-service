package com.yeziji.common.business.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统邮箱配置
 *
 * @author hwy
 * @since 2023/11/30 14:49
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemEmailConfigDTO {
    /**
     * 邮箱域名
     */
    private String host;

    /**
     * 发送的来源
     */
    private String internetAddress;

    /**
     * 邮箱账号
     */
    private String username;

    /**
     * 邮箱密码
     */
    private String password;
}
