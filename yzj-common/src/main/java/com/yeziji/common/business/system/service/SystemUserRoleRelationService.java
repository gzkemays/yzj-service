package com.yeziji.common.business.system.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.common.business.system.dto.SystemRoleReleasesUrlDTO;
import com.yeziji.common.business.system.dto.SystemUserRoleDTO;
import com.yeziji.common.business.system.entity.SystemUserRoleRelationEntity;

import java.util.List;

/**
 * 系统用户角色关联表 服务层。
 *
 * @author system
 * @since 2023-11-13
 */
public interface SystemUserRoleRelationService extends IService<SystemUserRoleRelationEntity> {
    /**
     * 绑定用户角色
     *
     * @param userRoleDTO 用户角色对象
     * @return {@link Boolean} 绑定结果
     */
    boolean bindUserRole(SystemUserRoleDTO userRoleDTO);

    /**
     * 获取系统角色放行地址列表
     *
     * @return {@link List} 系统角色放行地址列表
     */
    List<SystemRoleReleasesUrlDTO> listSystemRoleReleaseUrls();

    /**
     * 获取用户的角色放行地址列表
     *
     * @return {@link List} 用户的角色放行地址列表
     */
    List<SystemRoleReleasesUrlDTO> listUserSystemRoleReleaseUrls(Long userId);

    /**
     * 根据角色 id 获取用户 id 列表
     *
     * @return {@link List} 用户 id 列表
     */
    List<Long> listUserIdsByRoleId(Long roleId);

    /**
     * 根据用户 id 获取角色信息
     *
     * @param userId 用户 id
     * @return {@link SystemUserRoleDTO}
     */
    SystemUserRoleDTO getAsUserRoleByUserId(Long userId);
}
