package com.yeziji.common.business.system.listener.system;

import com.mybatisflex.annotation.InsertListener;
import com.yeziji.common.base.ListenerBase;
import com.yeziji.common.business.system.entity.SystemConfigEntity;
import com.yeziji.constant.SecuritySal;

/**
 * 系统配置新增时监听器
 *
 * @author hwy
 * @since 2023/11/30 14:30
 **/
public class SystemConfigOnInsertListenerBase extends ListenerBase<SystemConfigEntity> implements InsertListener {
    @Override
    public void onInsert(Object data) {
        super.encryptionColumns(super.convertObj(data), SecuritySal.SystemSalt.CONFIG_SALT);
    }
}
