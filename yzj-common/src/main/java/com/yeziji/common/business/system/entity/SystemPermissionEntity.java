package com.yeziji.common.business.system.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 系统权限表 实体类。
 *
 * @author system
 * @since 2024-12-11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table("system_permission")
public class SystemPermissionEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = 3009279259797682606L;

    /**
     * 权限类型：SystemPermissionTypeEnum
     */
    private Integer type;

    /**
     * 父级 id
     */
    private Long parentId;

    /**
     * 唯一 key
     */
    private String key;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 显示名称（建议为 i18n 的 key）
     */
    private String showName;

    /**
     * 权限参数：可以是路径也可以是json字符串
     */
    private String params;

    /**
     * 优先级
     */
    private Integer order;

    public SystemPermissionEntity(String key) {
        this.key = key;
    }
}
