package com.yeziji.common.business.system.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.common.business.system.dto.SystemRolePermissionDTO;
import com.yeziji.common.business.system.entity.SystemRolePermissionRelationEntity;

import java.io.Serializable;

/**
 * 系统角色权限关联表 服务层。
 *
 * @author system
 * @since 2023-11-13
 */
public interface SystemRolePermissionRelationService extends IService<SystemRolePermissionRelationEntity> {
    /**
     * 追加权限至角色当中
     *
     * @return {@link Boolean} 追加结果
     */
    boolean addRolePermission(SystemRolePermissionDTO systemRolePermissionDTO);

    /**
     * 根据 id 删除角色权限
     *
     * <p>跟 {@link #removeById(Serializable)} 一样的，但是要清除下用户的权限缓存</p>
     *
     * @param id 角色权限 id
     * @return {@link Boolean} 删除结果
     */
    boolean removeRolePermissionById(Long id);
}
