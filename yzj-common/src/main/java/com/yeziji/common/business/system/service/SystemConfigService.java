package com.yeziji.common.business.system.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.common.business.system.constant.SystemConfigTypeInterface;
import com.yeziji.common.business.system.constant.enums.SystemConfigTypeEnum;
import com.yeziji.common.business.system.dto.SystemEmailConfigDTO;
import com.yeziji.common.business.system.entity.SystemConfigEntity;

import java.util.function.Function;

/**
 * 系统配置表 服务层。
 *
 * @author system
 * @since 2023-11-13
 */
public interface SystemConfigService extends IService<SystemConfigEntity> {
    /**
     * 保存 email 配置
     *
     * @return {@link Boolean} 保存结果
     */
    boolean saveEmailConfig(SystemEmailConfigDTO emailConfigDTO);

    /**
     * 获 email 配置
     *
     * @return {@link SystemEmailConfigDTO} 邮箱配置
     */
    SystemEmailConfigDTO getEmailConfig();

    /**
     * 根据配置类型获取配置
     *
     * @param configTypeEnum 配置枚举
     * @return {@link SystemConfigEntity} 配置类
     */
    SystemConfigEntity getByTypeEnum(SystemConfigTypeEnum configTypeEnum);

    /**
     * 保存配置
     *
     * @param config 配置泛型
     * @param typeInterface 配置通用接口
     * @return {@link Boolean} 保存结果
     * @param <T> 任意泛型
     */
    <T> boolean saveConfig(T config, SystemConfigTypeInterface typeInterface);

    /**
     * 根据 type 获取配置
     *
     * @param typeInterface type 泛型接口
     * @return {@link T} 任意泛型
     * @param <T> 任意泛型
     */
    <T> T getConfigByTypeAndConvertFn(SystemConfigTypeInterface typeInterface, Function<String, T> function);

    /**
     * 根据配置编码获取配置
     *
     * @param typeInterface 配置通用接口
     * @return {@link SystemConfigEntity}
     */
    SystemConfigEntity getByCode(SystemConfigTypeInterface typeInterface);
}
