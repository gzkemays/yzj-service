package com.yeziji.common.business.platform.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 * 平台数据源管理表 表定义层。
 *
 * @author system
 * @since 2024-01-11
 */
public class PlatformDatasourceTableDef extends TableDef {

    /**
     * 平台数据源管理表
     */
    public static final PlatformDatasourceTableDef PLATFORM_DATASOURCE = new PlatformDatasourceTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * jdbc 地址
     */
    public final QueryColumn URL = new QueryColumn(this, "url");

    /**
     * 删除标识
     */
    public final QueryColumn IS_DELETE = new QueryColumn(this, "is_delete");

    /**
     * jdbc 密码
     */
    public final QueryColumn PASSWORD = new QueryColumn(this, "password");

    /**
     * jdbc 账号
     */
    public final QueryColumn USERNAME = new QueryColumn(this, "username");

    /**
     * 是否为指定默认数据源
     */
    public final QueryColumn IS_DEFAULT = new QueryColumn(this, "is_default");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 数据到期时间
     */
    public final QueryColumn EXPIRED_TIME = new QueryColumn(this, "expired_time");

    /**
     * jdbc 配置参数
     */
    public final QueryColumn CONFIG_PARAMS = new QueryColumn(this, "config_params");

    /**
     * 简介
     */
    public final QueryColumn INTRODUCTION = new QueryColumn(this, "introduction");

    /**
     * 平台名称
     */
    public final QueryColumn PLATFORM_NAME = new QueryColumn(this, "platform_name");

    /**
     * 续费金额
     */
    public final QueryColumn RENEWAL_AMOUNT = new QueryColumn(this, "renewal_amount");

    /**
     * 负责人
     */
    public final QueryColumn RESPONSIBLE_PERSON = new QueryColumn(this, "responsible_person");

    /**
     * 负责人联系方式
     */
    public final QueryColumn RESPONSIBLE_PERSON_CONTACT = new QueryColumn(this, "responsible_person_contact");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, PLATFORM_NAME, URL, USERNAME, PASSWORD, CONFIG_PARAMS, INTRODUCTION, RESPONSIBLE_PERSON, RESPONSIBLE_PERSON_CONTACT, RENEWAL_AMOUNT, IS_DEFAULT, EXPIRED_TIME, CREATE_TIME, UPDATE_TIME, IS_DELETE};

    public PlatformDatasourceTableDef() {
        super("", "platform_datasource");
    }

}