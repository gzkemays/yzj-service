package com.yeziji.common.business.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.common.business.system.entity.SystemPermissionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统权限表 映射层。
 *
 * @author system
 * @since 2024-12-11
 */
@Mapper
public interface SystemPermissionMapper extends BaseMapper<SystemPermissionEntity> {

}
