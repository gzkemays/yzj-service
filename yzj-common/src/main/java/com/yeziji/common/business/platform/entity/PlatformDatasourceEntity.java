package com.yeziji.common.business.platform.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 平台数据源管理表 实体类。
 *
 * @author system
 * @since 2024-01-11
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(value = "platform_datasource")
public class PlatformDatasourceEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = -2312523303885447133L;

    /**
     * 平台名称
     */
    private String platformName;

    /**
     * jdbc 地址
     */
    private String url;

    /**
     * jdbc 账号
     */
    private String username;

    /**
     * jdbc 密码
     */
    private String password;

    /**
     * jdbc 配置参数
     */
    private String configParams;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 负责人
     */
    private String responsiblePerson;

    /**
     * 负责人联系方式
     */
    private String responsiblePersonContact;

    /**
     * 续费金额
     */
    private BigDecimal renewalAmount;

    /**
     * 是否为指定默认数据源
     */
    private Boolean isDefault;

    /**
     * 数据到期时间
     */
    private LocalDateTime expiredTime;

}
