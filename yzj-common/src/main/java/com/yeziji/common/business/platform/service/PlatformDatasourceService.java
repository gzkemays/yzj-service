package com.yeziji.common.business.platform.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.common.business.platform.entity.PlatformDatasourceEntity;

/**
 * 平台数据源管理表 服务层。
 *
 * @author system
 * @since 2024-01-11
 */
public interface PlatformDatasourceService extends IService<PlatformDatasourceEntity> {

}
