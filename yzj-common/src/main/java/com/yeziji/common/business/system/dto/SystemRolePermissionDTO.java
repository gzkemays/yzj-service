package com.yeziji.common.business.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * 角色权限对象
 *
 * @author hwy
 * @since 2023/12/04 22:57
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemRolePermissionDTO {
    /**
     * 关联 id
     */
    private Long id;

    /**
     * 角色 id
     */
    @NotNull(message = "必须指定角色", groups = {AddPermission.class})
    private Long roleId;

    /**
     * 权限 id
     */
    @NotNull(message = "必须选择绑定的权限", groups = {AddPermission.class})
    private Long permissionId;

    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空", groups = {Save.class})
    private String roleName;

    /**
     * 一个角色可以放行多个接口
     */
    @NotEmpty(message = "绑定放行接口不能为空", groups = {Save.class, AddPermission.class})
    private Collection<String> releaseUrls;

    /**
     * 创建
     */
    public interface Save {
    }

    /**
     * 追加权限
     */
    public interface AddPermission {
    }
}
