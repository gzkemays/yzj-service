package com.yeziji.common.business.system.listener.system;

import com.mybatisflex.annotation.UpdateListener;
import com.yeziji.common.base.ListenerBase;
import com.yeziji.common.business.system.entity.SystemConfigEntity;
import com.yeziji.constant.SecuritySal;
import lombok.extern.slf4j.Slf4j;

/**
 * 更新监听器，跟 insert 类似
 *
 * @author hwy
 * @since 2023/11/30 14:45
 **/
@Slf4j
public class SystemConfigOnUpdateListenerBase extends ListenerBase<SystemConfigEntity> implements UpdateListener {
    @Override
    public void onUpdate(Object obj) {
        super.encryptionColumns(super.convertObj(obj), SecuritySal.SystemSalt.CONFIG_SALT);
    }
}
