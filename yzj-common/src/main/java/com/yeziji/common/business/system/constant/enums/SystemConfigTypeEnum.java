package com.yeziji.common.business.system.constant.enums;

import com.yeziji.common.business.system.constant.SystemConfigTypeInterface;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统配置枚举
 *
 * @author hwy
 * @since 2023/11/30 15:15
 **/
@Getter
@AllArgsConstructor
public enum SystemConfigTypeEnum implements SystemConfigTypeInterface {
    EMAIL(0, "系统邮箱配置"),
    ;
    private final int code;
    private final String desc;
}
