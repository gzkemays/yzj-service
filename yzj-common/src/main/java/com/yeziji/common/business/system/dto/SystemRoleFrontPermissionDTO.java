package com.yeziji.common.business.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 系统角色前端权限对象
 *
 * @author hwy
 * @since 2024/11/27 16:24
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemRoleFrontPermissionDTO {
    /**
     * 角色 id
     */
    private Long roleId;

    /**
     * 权限信息
     */
    private List<SystemPermissionDTO> permissions;
}
