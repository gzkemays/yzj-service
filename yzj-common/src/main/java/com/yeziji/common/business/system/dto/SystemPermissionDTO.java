package com.yeziji.common.business.system.dto;

import com.yeziji.common.business.system.constant.enums.SystemPermissionTypeEnum;
import com.yeziji.utils.expansion.Opt2;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统前端权限权限数据对象
 *
 * @author hwy
 * @since 2024/11/22 16:21
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemPermissionDTO implements Serializable {
    private static final long serialVersionUID = -8210675868809650247L;

    /**
     * 自增 id
     */
    private Long id;

    /**
     * 前端权限类型（默认菜单前端权限）
     *
     * @see SystemPermissionTypeEnum
     */
    private SystemPermissionTypeEnum type;

    /**
     * 父级 id
     */
    private Long parentId;

    /**
     * 前端权限 key（唯一）
     */
    private String key;

    /**
     * 前端权限名称（建议 i18n 的 key）
     */
    private String name;

    /**
     * 前端权限参数
     * <p>任意前端权限参数都会转化为字符串</p>
     */
    private String params;

    /**
     * 前端权限优先级
     */
    private Integer order;

    /**
     * 子前端权限
     */
    private List<SystemPermissionDTO> children;

    /**
     * 追加子级
     *
     * @param child 子级对象
     */
    public void addChild(SystemPermissionDTO child) {
        if (child != null) {
            this.children = Opt2.nullElse(this.children, new ArrayList<>());
            this.children.add(child);
        }
    }
}
