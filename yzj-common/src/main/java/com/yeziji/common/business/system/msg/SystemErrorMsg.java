package com.yeziji.common.business.system.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author hwy
 * @since 2023/12/15 17:37
 **/
@Getter
@AllArgsConstructor
public enum SystemErrorMsg implements CommonErrorEnum {
    SYSTEM_ROLE_IS_EXISTS(ERROR, "system-error.role-is-exists", "system-show-error.role-is-exists"),
    SYSTEM_ROLE_IS_NOT_EXISTS(ERROR, "system-error.role-is-not-exists", "system-show-error.role-is-not-exists"),
    SYSTEM_CONFIG_IS_NOT_EXISTS(ERROR, "system-error.config-is-not-exists=", "system-show-error.config-is-not-exists="),
    SYSTEM_USER_ROLE_IS_BOUND(ERROR, "system-error.user-role-is-bound", "system-show-error.user-role-is-bound"),
    SYSTEM_ROLE_PERMISSION_IS_NOT_EXISTS(ERROR, "system-error.role-permission-is-not-exists", "system-show-error.role-permission-is-not-exists"),
    SYSTEM_PERMISSION_URLS_IS_EMPTY(ERROR, "system-error.permission-urls-is-empty", "system-show-error.permission-urls-is-empty"),
    SYSTEM_PERMISSION_IS_NOT_EXISTS(ERROR, "system-error.permission-is-not-exists", "system-show-error.permission-is-not-exists"),
    SYSTEM_EVENT_KEY_IS_DUPLICATE(ERROR, "system-error.event-key-is-duplicate", "system-show-error.event-key-is-duplicate"),
    ;
    private final int code;
    private final String desc;
    private final String showMsg;
}
