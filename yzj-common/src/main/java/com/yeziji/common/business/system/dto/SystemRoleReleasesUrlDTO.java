package com.yeziji.common.business.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 系统角色放行地址数据对象
 *
 * @author hwy
 * @since 2023/11/29 17:18
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemRoleReleasesUrlDTO {
    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 放行的地址
     */
    private List<String> releaseUrls;
}
