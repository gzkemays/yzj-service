package com.yeziji.common.business.system.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.common.business.system.entity.SystemPermissionEntity;

import java.util.Set;

/**
 * 系统权限表 服务层。
 *
 * @author system
 * @since 2024-12-11
 */
public interface SystemPermissionService extends IService<SystemPermissionEntity> {
    /**
     * 根据 params 获取权限路径
     *
     * @return {@link Set} 权限路径集合
     */
    Set<String> listAsPathOfParams();
}
