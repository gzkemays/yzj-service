package com.yeziji.common.business.system.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;


/**
* 系统权限表 表定义层。
*
* @author system
* @since 2024-12-11
*/
public class SystemPermissionTableDef extends TableDef {

    private static final long serialVersionUID = 1L;

    /**
     * 系统权限表
     */
    public static final SystemPermissionTableDef SYSTEM_PERMISSION = new SystemPermissionTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 唯一 key
     */
    public final QueryColumn KEY = new QueryColumn(this, "key");

    /**
     * 权限名称
     */
    public final QueryColumn NAME = new QueryColumn(this, "name");

    /**
     * 权限类型：SystemPermissionTypeEnum
     */
    public final QueryColumn TYPE = new QueryColumn(this, "type");

    /**
     * 优先级
     */
    public final QueryColumn ORDER = new QueryColumn(this, "order");

    /**
     * 权限参数：可以是路径也可以是json字符串
     */
    public final QueryColumn PARAMS = new QueryColumn(this, "params");

    /**
     * 父级 id
     */
    public final QueryColumn PARENT_ID = new QueryColumn(this, "parent_id");

    /**
     * 显示名称（建议为 i18n 的 key）
     */
    public final QueryColumn SHOW_NAME = new QueryColumn(this, "show_name");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, TYPE, PARENT_ID, KEY, NAME, SHOW_NAME, PARAMS, ORDER, CREATE_TIME, UPDATE_TIME};

    public SystemPermissionTableDef() {
        super("", "system_permission");
    }

    private SystemPermissionTableDef(String schema, String name, String alisa) {
        super(schema, name, alisa);
    }

    public SystemPermissionTableDef as(String alias) {
        String key = getNameWithSchema() + "." + alias;
        return getCache(key, k -> new SystemPermissionTableDef("", "system_permission", alias));
    }

}