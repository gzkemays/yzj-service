package com.yeziji.common.business.system.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 系统角色权限关联表 实体类。
 *
 * @author system
 * @since 2023-11-13
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(value = "system_role_permission_relation")
public class SystemRolePermissionRelationEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = 5529753283434345736L;

    /**
     * 角色 id
     */
    private Long roleId;

    /**
     * 权限 id
     */
    private Long permissionId;

}
