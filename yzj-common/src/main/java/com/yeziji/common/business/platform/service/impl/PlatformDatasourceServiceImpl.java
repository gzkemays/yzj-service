package com.yeziji.common.business.platform.service.impl;

import com.yeziji.common.IServiceImpl;
import com.yeziji.common.business.platform.entity.PlatformDatasourceEntity;
import com.yeziji.common.business.platform.mapper.PlatformDatasourceMapper;
import com.yeziji.common.business.platform.service.PlatformDatasourceService;
import org.springframework.stereotype.Service;

/**
 * 平台数据源管理表 服务层实现。
 *
 * @author system
 * @since 2024-01-11
 */
@Service
public class PlatformDatasourceServiceImpl extends IServiceImpl<PlatformDatasourceMapper, PlatformDatasourceEntity> implements PlatformDatasourceService {

}
