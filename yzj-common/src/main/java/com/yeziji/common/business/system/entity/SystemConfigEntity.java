package com.yeziji.common.business.system.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.annotation.ShouldEncryption;
import com.yeziji.common.CommonEntity;
import com.yeziji.common.business.system.listener.system.SystemConfigOnInsertListenerBase;
import com.yeziji.common.business.system.listener.system.SystemConfigOnSetListenerBase;
import com.yeziji.common.business.system.listener.system.SystemConfigOnUpdateListenerBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 系统配置表 实体类。
 *
 * @author system
 * @since 2023-11-13
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(value = "system_config",
        onInsert = SystemConfigOnInsertListenerBase.class,
        onUpdate = SystemConfigOnUpdateListenerBase.class,
        onSet = SystemConfigOnSetListenerBase.class)
public class SystemConfigEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = 7454890220080131544L;

    /**
     * 配置类型
     */
    private Integer configType;

    /**
     * 配置值
     */
    @ShouldEncryption
    private String configValue;

}
