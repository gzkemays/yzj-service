package com.yeziji.common.business.system.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 * 系统配置表 表定义层。
 *
 * @author system
 * @since 2023-11-29
 */
public class SystemConfigTableDef extends TableDef {

    /**
     * 系统配置表
     */
    public static final SystemConfigTableDef SYSTEM_CONFIG = new SystemConfigTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 删除标识
     */
    public final QueryColumn IS_DELETE = new QueryColumn(this, "is_delete");

    /**
     * 配置类型
     */
    public final QueryColumn CONFIG_TYPE = new QueryColumn(this, "config_type");

    /**
     * 创建时间
     */
    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");

    /**
     * 更新时间
     */
    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 配置值
     */
    public final QueryColumn CONFIG_VALUE = new QueryColumn(this, "config_value");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, CONFIG_TYPE, CONFIG_VALUE, IS_DELETE, CREATE_TIME, UPDATE_TIME};

    public SystemConfigTableDef() {
        super("", "system_config");
    }

}