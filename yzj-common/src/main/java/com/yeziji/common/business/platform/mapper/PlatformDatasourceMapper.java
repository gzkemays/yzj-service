package com.yeziji.common.business.platform.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yeziji.common.business.platform.entity.PlatformDatasourceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 平台数据源管理表 映射层。
 *
 * @author system
 * @since 2024-01-11
 */
@Mapper
public interface PlatformDatasourceMapper extends BaseMapper<PlatformDatasourceEntity> {

}
