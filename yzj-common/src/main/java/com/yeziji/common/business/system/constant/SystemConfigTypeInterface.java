package com.yeziji.common.business.system.constant;

/**
 * 约束通用系统配置类型接口
 *
 * @author hwy
 * @since 2024/02/19 11:57
 **/
public interface SystemConfigTypeInterface {
    /**
     * 必须 getter code
     */
    int getCode();

    /**
     * 必须 getter desc
     */
    String getDesc();
}
