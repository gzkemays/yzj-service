package com.yeziji.common.business.system.service.impl;

import com.yeziji.common.IServiceImpl;
import com.yeziji.common.business.system.constant.enums.SystemRoleTypeEnum;
import com.yeziji.common.business.system.entity.SystemRoleEntity;
import com.yeziji.common.business.system.entity.table.SystemRoleTableDef;
import com.yeziji.common.business.system.mapper.SystemRoleMapper;
import com.yeziji.common.business.system.service.SystemRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统角色表 服务层实现。
 *
 * @author system
 * @since 2023-11-13
 */
@Slf4j
@Service
public class SystemRoleServiceImpl extends IServiceImpl<SystemRoleMapper, SystemRoleEntity> implements SystemRoleService {

    @Override
    public SystemRoleEntity getRoleByType(SystemRoleTypeEnum systemRoleTypeEnum) {
        if (systemRoleTypeEnum.getCode() < 0) {
            return null;
        }
        
        SystemRoleEntity systemRoleEntity =
                this.queryChain()
                        .select(SystemRoleTableDef.SYSTEM_ROLE.ID)
                        .where(SystemRoleEntity::getRoleName).eq(systemRoleTypeEnum.getDesc())
                        .one();
        if (systemRoleEntity == null) {
            log.warn("{} 角色不存在，进行初始化", systemRoleTypeEnum.getDesc());
            systemRoleEntity = SystemRoleEntity.builder().roleName(systemRoleTypeEnum.getDesc()).build();
            this.save(systemRoleEntity);
        }
        return systemRoleEntity;
    }
}
