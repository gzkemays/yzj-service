package com.yeziji.common.business.system.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 系统角色表 实体类。
 *
 * @author system
 * @since 2023-11-13
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(value = "system_role")
public class SystemRoleEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = 3476998228370283043L;

    /**
     * 角色名称
     */
    private String roleName;

}
