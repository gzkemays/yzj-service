package com.yeziji.common.business.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.yeziji.common.CommonSymbol;
import com.yeziji.common.business.system.dto.SystemEmailConfigDTO;
import com.yeziji.constant.VariousStrPool;
import com.yeziji.utils.DataUtils;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.StringJoiner;

/**
 * 系统配置转换器
 *
 * @author hwy
 * @since 2023/11/30 14:53
 **/
public class SystemConfigConvertor {
    private final String BASIC_TEXT;

    // prohibited public construction
    private SystemConfigConvertor(String basicTxt) {
        this.BASIC_TEXT = basicTxt;
    }

    public static SystemConfigConvertor init(String text) {
        return new SystemConfigConvertor(text);
    }

    /**
     * 构造 email 配置
     *
     * @return {@link SystemEmailConfigDTO} 系统 email 配置
     */
    public SystemEmailConfigDTO buildEmailConfig() {
        return this.buildConfig(SystemEmailConfigDTO.class);
    }

    /**
     * 构建任意泛型的配置
     *
     * @param clazz 泛型 class
     * @return {@link T} 泛型对象
     * @param <T> 任意泛型
     */
    public <T> T buildConfig(Class<T> clazz) {
        if (StrUtil.isBlank(BASIC_TEXT)) {
            return null;
        }

        return BeanUtil.toBean(DataUtils.getQueryByUrl(BASIC_TEXT), clazz, CopyOptions.create().setIgnoreNullValue(false));
    }

    /**
     * obj 转换为指定的配置构造
     *
     * @param obj 任意对象
     * @return {@link String} className?fieldName1=fieldValue1&fieldName2=fieldValue2&..&
     */
    @SneakyThrows
    public static String objConvertToStr(Object obj) {
        if (obj == null) {
            return VariousStrPool.EMPTY;
        }

        // 构造 className?fieldName1=fieldValue1&fieldName2=fieldValue2&..& 字符串
        Class<?> clazz = obj.getClass();
        StringJoiner strJoiner = new StringJoiner(CommonSymbol.AND_SYMBOL, clazz.getName() + CommonSymbol.QUESTION, VariousStrPool.EMPTY);
        Field[] fields = ReflectUtil.getFieldsDirectly(clazz, true);
        for (Field declaredField : fields) {
            declaredField.setAccessible(true);
            Object value = declaredField.get(obj);
            if (value != null) {
                strJoiner.add(String.format("%s=%s", declaredField.getName(), value));
            }
        }
        return strJoiner.toString();
    }

    public static void main(String[] args) {
        String str = objConvertToStr(SystemEmailConfigDTO.builder()
                .host("smtp.qq.com")
                .username("yezijigroup@qq.com")
                .password("wfxfkeqjltascijd")
                .internetAddress("椰子鸡工作室 <yezijigroup@qq.com>")
                .build());
        System.out.println(str);
        System.out.println(DataUtils.getQueryByUrl(str));
        Map<String, String> queryByUrl = DataUtils.getQueryByUrl(str);
        System.out.println(BeanUtil.toBean(queryByUrl, SystemEmailConfigDTO.class, CopyOptions.create().setIgnoreNullValue(false)));
    }
}
