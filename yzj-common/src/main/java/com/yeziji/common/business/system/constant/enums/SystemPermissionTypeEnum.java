package com.yeziji.common.business.system.constant.enums;

import com.mybatisflex.annotation.EnumValue;
import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Lists2;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统权限类型
 *
 * @author hwy
 * @since 2024/11/22 16:13
 **/
@Getter
@AllArgsConstructor
public enum SystemPermissionTypeEnum implements CommonEnum {
    MENU(0, "菜单"),
    BUTTON(1, "按钮"),
    DATA(2, "数据"),
    API(3, "接口");

    @EnumValue
    private final int code;
    private final String desc;

    public static SystemPermissionTypeEnum getByCode(Integer code) {
        return Lists2.filterFirstOpt(values(), item -> item.getCode() == code).orElse(null);
    }

    /**
     * 菜单、按钮和数据权限属于前端资源
     *
     * @param code 类型编码
     * @return {@link Boolean}
     */
    public static boolean isFrontSource(Integer code) {
        SystemPermissionTypeEnum typeEnum = getByCode(code);
        if (typeEnum == null) {
            return false;
        }

        return typeEnum == MENU ||
                typeEnum == BUTTON ||
                typeEnum == DATA;
    }

    /**
     * 接口权限属于后端资源
     *
     * @param code 类型编码
     * @return {@link Boolean}
     */
    public static boolean isBackedSource(Integer code) {
        SystemPermissionTypeEnum typeEnum = getByCode(code);
        if (typeEnum == null) {
            return false;
        }

        return typeEnum == API;
    }
}
