package com.yeziji.common.business.system.entity;

import com.mybatisflex.annotation.Table;
import com.yeziji.common.CommonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 系统用户角色关联表 实体类。
 *
 * @author system
 * @since 2023-11-13
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(value = "system_user_role_relation")
public class SystemUserRoleRelationEntity extends CommonEntity implements Serializable {
    private static final long serialVersionUID = -7637979669451203613L;

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 角色 id
     */
    private Long roleId;

}
