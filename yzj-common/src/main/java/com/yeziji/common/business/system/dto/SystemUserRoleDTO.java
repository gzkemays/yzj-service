package com.yeziji.common.business.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 用户角色对象
 *
 * @author hwy
 * @since 2023/12/06 23:21
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemUserRoleDTO {
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户 id
     */
    @NotNull(message = "必须指定用户", groups = {Bind.class})
    private Long userId;

    /**
     * 角色 id
     */
    @NotNull(message = "必须指定对应的角色", groups = {Bind.class})
    private Long roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 绑定用户角色
     */
    public interface Bind {
    }
}
