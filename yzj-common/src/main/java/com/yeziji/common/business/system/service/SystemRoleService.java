package com.yeziji.common.business.system.service;

import com.mybatisflex.core.service.IService;
import com.yeziji.common.business.system.constant.enums.SystemRoleTypeEnum;
import com.yeziji.common.business.system.entity.SystemRoleEntity;

/**
 * 系统角色表 服务层。
 *
 * @author system
 * @since 2023-11-13
 */
public interface SystemRoleService extends IService<SystemRoleEntity> {
    /**
     * 根据类型获取角色
     *
     * <p>如果角色不存在会进行初始化</p>
     *
     * @param systemRoleTypeEnum 角色类型枚举
     * @return {@link SystemRoleEntity}
     */
    SystemRoleEntity getRoleByType(SystemRoleTypeEnum systemRoleTypeEnum);
}
