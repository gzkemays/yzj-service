package com.yeziji.common.business.system.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

/**
 * 系统用户角色关联表 表定义层。
 *
 * @author system
 * @since 2023-11-29
 */
public class SystemUserRoleRelationTableDef extends TableDef {

    /**
     * 系统用户角色关联表
     */
    public static final SystemUserRoleRelationTableDef SYSTEM_USER_ROLE_RELATION = new SystemUserRoleRelationTableDef();

    /**
     * 自增 id
     */
    public final QueryColumn ID = new QueryColumn(this, "id");

    /**
     * 角色 id
     */
    public final QueryColumn ROLE_ID = new QueryColumn(this, "role_id");

    /**
     * 用户 id, 可能是微信, 也可能是 member
     */
    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    /**
     * 删除标识
     */
    public final QueryColumn IS_DELETE = new QueryColumn(this, "is_delete");


    public final QueryColumn CREATE_TIME = new QueryColumn(this, "create_time");


    public final QueryColumn UPDATE_TIME = new QueryColumn(this, "update_time");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ID, USER_ID, ROLE_ID, IS_DELETE, CREATE_TIME, UPDATE_TIME};

    public SystemUserRoleRelationTableDef() {
        super("", "system_user_role_relation");
    }

}