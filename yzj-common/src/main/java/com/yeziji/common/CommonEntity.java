package com.yeziji.common;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.yeziji.constant.IntBoolean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * 基本父类
 *
 * @author hwy
 * @since 2023/08/30 23:36
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CommonEntity implements Serializable {
    private static final long serialVersionUID = 7231704916347878355L;

    /**
     * 自增主键 id
     */
    @Id(keyType = KeyType.Auto)
    protected Long id;

    /**
     * 删除标识
     */
    @Column(isLogicDelete = true)
    protected Integer isDelete;

    /**
     * 创建时间
     */
    @Column(onInsertValue = "now()")
    protected Date createTime;

    /**
     * 更新时间
     */
    @Column(onUpdateValue = "now()")
    protected Date updateTime;

    /**
     * 是否逻辑删除
     *
     * @return {@link Boolean} true 已删除
     */
    public boolean isDelete() {
        return isDelete != null && IntBoolean.isTruth(this.isDelete);
    }
}
