package com.yeziji.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;

/**
 * 通用的平台信息
 *
 * @author hwy
 * @since 2024/09/18 15:02
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Platform {
    /**
     * 平台的名称
     */
    @Builder.Default
    private String platformName = "yzj-platform";

    /**
     * 平台的地区信息
     */
    @Builder.Default
    private Locale platformLocale = Locale.CHINA;
}
