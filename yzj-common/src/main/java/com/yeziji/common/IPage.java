package com.yeziji.common;

import com.mybatisflex.core.paginate.Page;
import lombok.NoArgsConstructor;

/**
 * 自定义分页
 *
 * <p>Flex pageNumber == 0 会抛出异常</p>
 *
 * @author hwy
 * @since 2023/09/05 2:27
 **/
@NoArgsConstructor
public class IPage<T> extends Page<T> {

    private static final long serialVersionUID = 8378191897966864384L;

    public IPage(long current, long batchSize) {
        super(current, batchSize);
    }

    @Override
    public long getPageNumber() {
        long pageNumber = super.getPageNumber();
        return pageNumber <= 0L ? 1 : pageNumber;
    }

    @Override
    public void setPageNumber(long pageNumber) {
        super.setPageNumber(pageNumber <= 0L ? 1 : pageNumber);
    }

    public static <T> IPage<T> defaultPage() {
        return new IPage<>(0, 10);
    }
}
