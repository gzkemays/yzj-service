package com.yeziji.common;

import com.yeziji.utils.expansion.MessageFormat2;

/**
 * 枚举公共接口
 *
 * @author hwy
 * @since 2023/08/30 0:37
 **/
public interface CommonEnum {
    /**
     * 通用状态码（成功）
     */
    int SUCCESS = 0;

    /**
     * 通用状态码（错误）
     */
    int ERROR = -1;

    /**
     * 枚举必须重写 code
     *
     * @return {@link Integer} 枚举码
     */
    int getCode();

    /**
     * 枚举必须说明其作用
     *
     * @return {@link String} 说明作用
     */
    String getDesc();

    /**
     * 不同地区声明信息不同
     *
     * @return {@link String} 地区的声明信息
     */
    default String getLangDesc(Object... objects) {
        return MessageFormat2.language(this.getDesc(), objects);
    }
}
