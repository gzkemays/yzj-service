package com.yeziji.common;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * 公用数据源配置
 *
 * @author hwy
 * @since 2023/11/14 0:30
 **/
public class CommonDataSourceConfig {
    /**
     * 获取初始化阿里数据源信息
     *
     * @return {@link DruidDataSource} 数据源信息
     */
    public static DruidDataSource getInitDruidDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        // 配置初始值、最大活跃数和最小空闲数配置
        druidDataSource.setMinIdle(5);
        druidDataSource.setInitialSize(5);
        druidDataSource.setMaxActive(20);
        // 申请连接时检测空闲时间
        druidDataSource.setTestWhileIdle(true);
        // 关闭申请以及归还时的检测连接（如果设置为 true 会调用 validationQuery 检测连接是否有效）
        druidDataSource.setTestOnBorrow(false);
        druidDataSource.setTestOnReturn(false);
        // 开启异步初始化策略
        druidDataSource.setAsyncInit(true);
        // 设置异常和空闲检测时间
        druidDataSource.setTimeBetweenConnectErrorMillis(60 * 1000L);
        druidDataSource.setTimeBetweenEvictionRunsMillis(30 * 1000L);
        // 如果超过 minIdle 并空闲时间超过 60 秒就要被回收
        druidDataSource.setMinEvictableIdleTimeMillis(60 * 1000L);
        // 如果超过 30 分钟就直接强制回收
        // druidDataSource.setMaxEvictableIdleTimeMillis(30 * 60 * 1000L);
        // 自动回收超时连接
        druidDataSource.setKeepAlive(true);
        druidDataSource.setMaxWait(60 * 1000L);
        druidDataSource.setRemoveAbandoned(true);
        druidDataSource.setRemoveAbandonedTimeoutMillis(3 * 60 * 1000L);
        // 任意物理连接超过 1 小时都要被清除
//        druidDataSource.setPhyTimeoutMillis(60 * 60 * 1000L);
        // 设置预编译缓存
        druidDataSource.setPoolPreparedStatements(true);
        druidDataSource.setMaxOpenPreparedStatements(20);
        // 开启统计
        druidDataSource.setUseGlobalDataSourceStat(true);
        return druidDataSource;
    }
}
