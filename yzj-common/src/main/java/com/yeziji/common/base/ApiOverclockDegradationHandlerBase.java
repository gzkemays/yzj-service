package com.yeziji.common.base;

import com.yeziji.common.CommonResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Api 超频降级处理
 *
 * @author hwy
 * @since 2024/02/02 20:39
 **/
public interface ApiOverclockDegradationHandlerBase {
    /**
     * 封禁
     *
     * @param request 请求
     * @param response 响应
     * @return {@link CommonResult}
     */
    CommonResult<Object> ban(HttpServletRequest request, HttpServletResponse response);

    /**
     * 允许间隔
     *
     * @param request 请求
     * @param response 响应
     * @return {@link CommonResult}
     */
    CommonResult<Object> interval(HttpServletRequest request, HttpServletResponse response);
}
