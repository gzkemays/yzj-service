package com.yeziji.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 * 用户在线基础类
 *
 * @author hwy
 * @since 2023/11/12 14:35
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserOnlineBase {
    /**
     * 登录 ip
     */
    private String ip;

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 账号
     */
    private String username;

    /**
     * 登录时间
     */
    private Date loginTime;
}
