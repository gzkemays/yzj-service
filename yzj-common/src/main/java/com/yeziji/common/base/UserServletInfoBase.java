package com.yeziji.common.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;

/**
 * 用户 servlet 基础信息
 *
 * @author hwy
 * @since 2023/12/14 11:13
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserServletInfoBase {
    /**
     * ip
     */
    private String ip;

    /**
     * 当前栈名称
     */
    private String mdc;

    /**
     * servlet 访问地址
     */
    private String path;

    /**
     * servlet 携带的 token
     */
    private String token;

    /**
     * servlet 通知使用的地区
     */
    private Locale locale;
}
