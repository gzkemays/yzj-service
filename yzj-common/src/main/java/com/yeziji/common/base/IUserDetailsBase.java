package com.yeziji.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 通用的用户信息
 *
 * @author hwy
 * @since 2024/03/15 14:20
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class IUserDetailsBase {
    /**
     * 用戶 id
     */
    protected Long userId;

    /**
     * 账号
     */
    protected String username;

    /**
     * 密码
     */
    protected String password;

    /**
     * 指定数据源键值
     */
    protected String dataSourceKey;

    /**
     * 是否为管理员
     */
    protected boolean isAdmin;
}
