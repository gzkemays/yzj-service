package com.yeziji.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * excel 表头
 *
 * @author hwy
 * @since 2023/07/21 11:59
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcelHeader {
    /**
     * 默认导出表头
     */
    @AliasFor("headers")
    String[] value() default {};

    /**
     * 异常时导出的表头
     */
    String[] errorHeaders() default {};

    /**
     * TODO: 允许动态配置多个分组导入
     *
     */
    ExcelHeaderGroup[] groups() default {};
}
