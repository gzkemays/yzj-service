package com.yeziji.annotation;

import com.yeziji.constant.VariousStrPool;

import java.lang.annotation.*;

/**
 * 指定要加密的字段
 *
 * @author hwy
 * @since 2023/09/01 21:48
 **/
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ShouldEncryption {
    /**
     * 加密盐值
     *
     * @return {@link String} 盐值
     */
    String salt() default VariousStrPool.EMPTY;
}
