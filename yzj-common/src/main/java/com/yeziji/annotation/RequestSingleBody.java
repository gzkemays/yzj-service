package com.yeziji.annotation;

import java.lang.annotation.*;

/**
 * 单个请求体注解
 *
 * @author hwy
 * @since 2023/09/02 15:20
 **/
@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestSingleBody {
    String value();

    boolean required() default true;

    String defaultValue() default "\n\t\t\n\t\t\n\ue000\ue001\ue002\n\t\t\t\t\n";
}
