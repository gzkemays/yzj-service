package com.yeziji.annotation;

import org.springframework.core.annotation.AliasFor;

/**
 * excel 表头分组
 *
 * @author hwy
 * @since 2023/07/21 12:07
 **/
public @interface ExcelHeaderGroup {
    /**
     * 当前分组的头部
     */
    @AliasFor("headers")
    String[] value();

    /**
     * 异常时导出的表头
     */
    String[] errorHeaders() default {};

    /**
     * 允许分组的 class
     */
    Class<?>[] groups();
}
