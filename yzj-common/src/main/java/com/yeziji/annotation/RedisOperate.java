package com.yeziji.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Redis 的存储或删除的操作
 *
 * @author hwy
 * @since 2023/09/25 15:07
 **/
@Repeatable(RemoveRedis.class)
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface RedisOperate {
    /**
     * redis 目录 key
     */
    String key();

    /**
     * 支持哈希
     */
    String hashKey() default "";

    /**
     * 是否异步执行
     */
    boolean async() default false;

    /**
     * 保存时间或删除延时时间
     */
    long time() default 0L;

    /**
     * 时间单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 是否覆盖缓存
     */
    boolean override() default false;

    /**
     * 是否将参数填充至入参方法
     */
    boolean fillRunnableArgs() default false;

    /**
     * 操作 redis 之前的操作
     */
    Class<?> beforeRunnable() default void.class;

    /**
     * 调用的方法
     */
    String beforeMethod() default "";

    /**
     * 是否被容器管理
     */
    boolean beforeIsSpring() default false;

    /**
     * 是否调用入参执行后续方法
     */
    boolean afterUsingParams() default true;

    /**
     * 操作 redis 之后执行的操作
     */
    Class<?> afterRunnable() default void.class;

    /**
     * 调用的方法
     */
    String afterMethod() default "";

    /**
     * 是否被容器管理
     */
    boolean afterIsSpring() default true;

    /**
     * 指定返回的数值
     */
    boolean isReturn() default true;
}
