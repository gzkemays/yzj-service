package com.yeziji.annotation;

import com.yeziji.common.business.system.constant.enums.SystemPermissionTypeEnum;
import com.yeziji.common.business.system.constant.enums.SystemRoleTypeEnum;

import java.lang.annotation.*;

/**
 * 自动装配权限注解
 *
 * @author hwy
 * @since 2024/05/07 10:54
 **/
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AutowiredRolePermission {
    /**
     * 必须申明自动绑定的权限类型
     */
    SystemPermissionTypeEnum type();

    /**
     * 权限 key
     * <p>如果 type 为前端资源: 菜单、数据以及按钮时必须指定 key; 如果是接口資源可以由系统生成，但是 name 必须是不重复的</p>
     */
    String key() default "";

    /**
     * 权限名称
     */
    String name() default "";

    /**
     * 权限显示的名称
     * <p>一般如果是前端资源菜单或按钮时，要指定对应前端 i18n 的 key</p>
     */
    String showName() default "";

    /**
     * 排序，默認為 0
     */
    int order() default 0;

    /**
     * 自动绑定的角色类型
     */
    SystemRoleTypeEnum[] roleTypes() default {SystemRoleTypeEnum.NORMAL};
}