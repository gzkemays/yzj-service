package com.yeziji.annotation;

import java.lang.annotation.*;

/**
 * excel 数据
 *
 * @author hwy
 * @since 2023/07/21 12:01
 **/
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcelParam {
    /**
     * 对应映射表头的下标位置
     */
    int headerIndex() default -1;

    /**
     * 对应映射的表头，如果不填写默认就是根据表头数组顺序进行赋值
     */
    String header() default "";

    /**
     * 如果为 null 导出的默认值
     */
    String ifNullable() default "";

    /**
     * TODO: 忽略分组，允许根据不同的表头进行分组
     */
    Class<?>[] ignoreGroup() default {};

    /**
     * TODO: 所属分组
     */
    Class<?>[] groups() default {};

    /**
     * 当设置为 true 后，若不是触发 {@link ExcelHeader#errorHeaders()} 映射就不会追加
     */
    boolean successIgnore() default false;
}
