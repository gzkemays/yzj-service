package com.yeziji.annotation;

import com.yeziji.common.base.ApiOverclockDegradationHandlerBase;
import com.yeziji.config.support.handler.DefaultApiOverclockDegradationHandler;
import com.yeziji.constant.ApiLockMonitorMode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 接口锁
 * <p>指定用户短时间内的访问接口频率</p>
 *
 * @author hwy
 * @since 2024/02/02 20:32
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface ApiLock {
    /**
     * 默认监听参数
     */
    ApiLockMonitorMode[] monitors() default {ApiLockMonitorMode.IP};

    /**
     * 允许访问间隔(ms)
     */
    long acceptInterval() default 50;

    /**
     * 封禁时长(ms)
     */
    long banTime() default 50;

    /**
     * 超频降级行为
     */
    Class<? extends ApiOverclockDegradationHandlerBase> downgrade() default DefaultApiOverclockDegradationHandler.class;
}
