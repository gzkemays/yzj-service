package com.yeziji.annotation;

import com.yeziji.config.support.handler.AutowiredPermissionHandler;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用自动装配控制器
 * <p>
 *     启用之后会默认扫描指定 basePackages 下的 @Controller/@ResetController, 如果没指定 basePackages 就默认扫描全部
 * </p>
 * @author hwy
 * @since 2024/04/30 15:54
 **/
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({AutowiredPermissionHandler.class})
public @interface EnableAutowiredPermission {
    /**
     * 兼容 default setter
     */
    String[] value() default {};

    /**
     * 扫描的路径
     * <p>如果为空默认扫描全部控制器</p>
     */
    String[] basePackages() default {};
}
