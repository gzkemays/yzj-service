package com.yeziji.annotation;

import java.lang.annotation.*;

/**
 * redis 分布式锁
 *
 * @author hwy
 * @since 2023/06/20 11:48
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RedisLock {
    enum LockFailAction {
        GIVE_UP, CONTINUE;
    }

    /**
     * 锁的资源，redis的key
     */
    String lockKey() default "";

    /**
     * 持锁时间,单位毫秒
     */
    long keepMills() default 30000;

    /**
     * 获取锁失败返回的消息，一般返回响应信息
     *
     * @see com.yeziji.common.CommonResult
     */
    String failMsg() default "请勿频繁操作";

    /**
     * 当获取失败时候动作
     */
    LockFailAction action() default LockFailAction.CONTINUE;

    /**
     * 重试的间隔时间,设置GIVE_UP忽略此项
     * 单位:毫秒
     */
    long sleepMills() default 500;

    /**
     * 重试次数
     */
    int retryTimes() default 3;

    /**
     * 锁的 key 前缀
     */
    boolean useKeyPrefix() default true;

    /**
     * 拼接密钥信息
     */
    boolean concatTokenMsg() default false;

    /**
     * 是否从参数中获取属性值来拼接key(场景如:方式重复下单)
     */
    boolean concatArgs() default false;

    /**
     * 第几个参数获取key
     */
    int indexOfArgs() default 0;

    /**
     * 获取key的字段名
     */
    String fieldName() default "";

    /**
     * 方法入参是一个对象还是一个值(如String、Integer等 可以直接作为key的)
     */
    boolean isObject() default false;
}
