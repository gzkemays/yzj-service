package com.yeziji.utils;

import java.util.concurrent.TimeUnit;

/**
 * 时间工具类
 *
 * @author hwy
 * @since 2023/11/26 17:27
 **/
public class TimeUtils {
    /**
     * 获取指定倍数的毫秒
     *
     * @param multiple 倍数
     * @param timeUnit 时间单位
     * @return {@link Long} 毫秒级的时间
     */
    public static long getMillisTime(final int multiple, TimeUnit timeUnit) {
        return multiple * getMillisTime(timeUnit);
    }

    /**
     * 获取指定时间单位的毫秒(默认倍数为 1)
     *
     * @param timeUnit 时间单位
     * @return {@link Long} 毫秒级的时间
     */
    public static long getMillisTime(TimeUnit timeUnit) {
        switch (timeUnit) {
            case DAYS:
                return 86400000L;
            case HOURS:
                return 3600000L;
            case MINUTES:
                return 60000L;
            case SECONDS:
                return 10000L;
            default:
                return 0L;
        }
    }
}
