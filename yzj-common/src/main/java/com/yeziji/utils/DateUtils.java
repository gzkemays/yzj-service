package com.yeziji.utils;

import com.yeziji.constant.VariousStrPool;

/**
 * 日期工具类
 *
 * @author hwy
 * @since 2023/11/12 17:43
 **/
public class DateUtils {
    /**
     * 格式化时间
     *
     * <p>实现 ms \ s \ min \ hour 的字符串输出</p>
     *
     * @param time 时间戳
     * @return {@link String} 1ms\10s\20min\1h
     */
    public static String formatTime(long time) {
        if (time < 1000) {
            return time + VariousStrPool.Time.EZ_MILLIS;
        } else if (time < 60 * 1000) {
            return (time / 1000) + VariousStrPool.Time.EZ_SECONDS;
        } else if (time < 60 * 60 * 1000) {
            return (time / (60 * 1000)) + VariousStrPool.Time.EZ_MINUTE;
        } else {
            long hours = time / (60 * 60 * 1000);
            long minutes = (time % (60 * 60 * 1000)) / (60 * 1000);
            return hours + VariousStrPool.Time.EZ_HOUR + minutes + VariousStrPool.Time.EZ_MINUTE;
        }
    }

    /**
     * 格式化时间
     *
     * <p>实现 s \ min \ hour 的字符串输出</p>
     *
     * @param time 时间戳
     * @return {@link String} 10s\20min\1h
     */
    public static String formatSecondTime(long time) {
        if (time < 60) {
            return time + VariousStrPool.Time.EZ_SECONDS;
        } else if (time < 60 * 60) {
            return (time / 60) + VariousStrPool.Time.EZ_MINUTE;
        } else {
            long hours = time / (60 * 60);
            long minutes = (time % (60 * 60)) / 60;
            return hours + VariousStrPool.Time.EZ_HOUR + minutes + VariousStrPool.Time.EZ_MINUTE;
        }
    }
}
