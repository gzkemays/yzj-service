package com.yeziji.utils.expansion;

import cn.hutool.extra.spring.SpringUtil;
import com.yeziji.common.context.OnlineContext;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.context.MessageSource;

/**
 * 格式化 MessageFormat
 *
 * @author hwy
 * @since 2023/12/07 18:49
 **/
public class MessageFormat2 {
    /**
     * 简易版 {@link java.text.MessageFormat#format(String, Object...)}
     * <p>该方法的目的是为了防止数值大于 1000 时导致 1,000 与 Redis 自动生产的 key 不一致问题</p>
     *
     * @param pattern   替换字段
     * @param arguments 替换值
     * @return {@link String} 替换结果
     */
    public static String format(String pattern, Object... arguments) {
        if (arguments.length > 0) {
            for (int i = 0; i < arguments.length; i++) {
                pattern = pattern.replace(String.format("{%s}", i), String.valueOf(arguments[i]));
            }
        }
        return pattern;
    }

    /**
     * logback 日志格式化
     *
     * @param pattern   替换字段
     * @param arguments 替换值
     * @return {@link String} 替换结果
     */
    public static String formatTuple(String pattern, Object... arguments) {
        FormattingTuple formattingTuple = MessageFormatter.arrayFormat(pattern, arguments);
        return formattingTuple.getMessage();
    }

    /**
     * 根据地区来构造对应的语言，如果找不到，返回默认值
     *
     * @param msg    传入信息（可以是配置的地区语言，也可能是自定义的）
     * @param msgArr 赋值参数
     * @return {@link String}
     */
    public static String language(String msg, Object... msgArr) {
        String message = msg;
        try {
            message = Lazy.MESSAGE_SOURCE.getMessage(msg, msgArr, OnlineContext.getLocale());
        } catch (Exception e) {
            // ignored
        }
        return message;
    }

    private static class Lazy {
        private static final MessageSource MESSAGE_SOURCE = SpringUtil.getBean(MessageSource.class);
    }
}
