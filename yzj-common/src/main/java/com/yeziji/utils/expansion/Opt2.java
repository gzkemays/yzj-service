package com.yeziji.utils.expansion;

import com.yeziji.constant.VariousStrPool;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Optional 简化工具包
 *
 * @author hwy
 * @since 2023/09/07 1:19
 **/
public class Opt2 {
    /**
     * Optional.orElse
     *
     * @param data   字符串
     * @param orElse null 值
     * @return {@link T} 如果为 null 就返回对应的期望值
     */
    public static <T> T nullElse(T data, T orElse) {
        return Optional.ofNullable(data).orElse(orElse);
    }

    /**
     * 字符串 null 为 empty
     *
     * @param data 字符串
     * @return {@link String} 如果为 null 就返回空字符串
     */
    public static String nullEmpty(String data) {
        return nullElse(data, VariousStrPool.EMPTY);
    }

    public static <T> Collection<T> nullEmpty(Collection<T> list) {
        return nullElse(list, Lists2.empty());
    }

    /**
     * 如果布尔值为 null 则返回 true
     *
     * @param data 传入的布尔值
     * @return {@link Boolean} orElse True
     */
    public static Boolean nullTrue(Boolean data) {
        return nullElse(data, true);
    }

    /**
     * 如果布尔值为 null 则返回 false
     *
     * @param data 传入的布尔值
     * @return {@link Boolean} orElse false
     */
    public static Boolean nullFalse(Boolean data) {
        return nullElse(data, false);
    }

    /**
     * 不为空就执行消费者行为
     *
     * @param data     数据
     * @param consumer 消费者行为
     * @param <T>      指定入参泛型
     */
    public static <T> void notNullThen(T data, @Nonnull Consumer<T> consumer) {
        Optional.ofNullable(data).ifPresent(consumer);
    }
}
