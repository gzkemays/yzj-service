package com.yeziji.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 货币工具类
 *
 * @author hwy
 * @since 2023/09/09 0:43
 **/
public class MoneyUtils {
    /**
     * 1000
     */
    private static final BigDecimal THOUSAND = BigDecimal.valueOf(1000);
    /**
     * 100
     */
    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);

    /**
     * 元转分
     *
     * <p>1元 = 100分</p>
     *
     * @return {@link Integer} 获取分钱
     */
    public static Integer yuanToCent(final BigDecimal yuan) {
        return yuan.multiply(ONE_HUNDRED).intValue();
    }

    /**
     * 分转元
     *
     * @param cent 分
     * @return {@link Integer} 取元
     */
    public static BigDecimal centToYuan(final BigDecimal cent) {
        return centToYuan(cent, 2);
    }

    /**
     * 分转元
     *
     * @param cent  分
     * @param scale 保留小数点
     * @return {@link Integer} 取元
     */
    public static BigDecimal centToYuan(final BigDecimal cent, int scale) {
        return cent.divide(ONE_HUNDRED, scale, RoundingMode.HALF_UP);
    }

    /**
     * 根据比例计算提成
     *
     * @param amount 总金额[单位: 分]
     * @param bonus  比例[千分之N]
     * @return {@link BigDecimal} 四舍五入后的 BigDecimal 结果
     */
    public static BigDecimal computeAmountBonus(int amount, Integer bonus) {
        // 如果没设置比例就返回总金额
        if (bonus == null) {
            return BigDecimal.valueOf(amount);
        }

        // 四舍五入: 取 2 位小数
        return BigDecimal.valueOf(amount)
                .multiply(BigDecimal.valueOf(bonus).divide(THOUSAND, 2, RoundingMode.HALF_UP));
    }
}
