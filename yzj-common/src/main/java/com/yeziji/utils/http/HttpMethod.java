package com.yeziji.utils.http;

import java.util.ArrayList;
import java.util.List;

/**
 * HTTP 基本参数
 *
 * @author gzkemays
 * @since 2022/1/14 20:25
 */
public interface HttpMethod {
  String GET = "GET";
  String POST = "POST";
  String PUT = "PUT";
  String HEAD = "HEAD";
  String DELETE = "DELETE";

  List<String> METHODS =
      new ArrayList<String>(5) {
        {
          add(GET);
          add(POST);
          add(PUT);
          add(HEAD);
          add(DELETE);
        }
      };
}
