package com.yeziji.utils.http;

/**
 * Http send 请求配置
 *
 * @author gzkemays
 * @since 2022/1/14 22:14
 */
public class YzjSendConfig {
    boolean redirect;
    boolean cookie;
    boolean document;
    boolean json;
    boolean postJson;
    boolean postForm;

    public static YzjSendConfigBuilder start() {
        return new YzjSendConfigBuilder();
    }

    public static final class YzjSendConfigBuilder {
        boolean redirect;
        boolean cookie;
        boolean document;
        boolean json;
        boolean postJson;
        boolean postForm;

        private YzjSendConfigBuilder() {
        }

        public YzjSendConfigBuilder postJson() {
            this.postJson = true;
            return this;
        }

        public YzjSendConfigBuilder postForm() {
            this.postForm = true;
            return this;
        }


        public YzjSendConfigBuilder redirect() {
            this.redirect = true;
            return this;
        }

        public YzjSendConfigBuilder cookie() {
            this.cookie = true;
            return this;
        }

        public YzjSendConfigBuilder document() {
            this.document = true;
            return this;
        }

        public YzjSendConfigBuilder json() {
            this.json = true;
            return this;
        }

        public YzjSendConfig end() {
            YzjSendConfig yzjSendConfig = new YzjSendConfig();
            yzjSendConfig.document = this.document;
            yzjSendConfig.redirect = this.redirect;
            yzjSendConfig.json = this.json;
            yzjSendConfig.cookie = this.cookie;
            yzjSendConfig.postForm = this.postForm;
            yzjSendConfig.postJson = this.postJson;
            return yzjSendConfig;
        }
    }
}
