package com.yeziji.utils.http;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Map;

/**
 * http 请求结果
 *
 * @author gzkemays
 * @since 2022/1/14 20:42
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YzjHttpResult {
    private String result;
    private String cookie;
    private Map<String, String> cookieStatus;
    private String redirectUrl;
    private Document document;
    private JSONObject json;

    public Document getDocument() {
        if (document == null) {
            return Jsoup.parse(result);
        }
        return document;
    }
}
