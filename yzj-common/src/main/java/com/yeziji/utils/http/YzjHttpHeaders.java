package com.yeziji.utils.http;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Yzj 请求头部
 *
 * @author gzkemays
 * @since 2022/4/15 19:48
 */
@Data
public class YzjHttpHeaders {
    String key;
    String value;
    List<String> keys;
    List<String> values;

    public static YzjHttpHeadersBuilder builder() {
        return new YzjHttpHeadersBuilder();
    }

    public static final class YzjHttpHeadersBuilder {
        String key;
        String value;
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();

        private YzjHttpHeadersBuilder() {
        }

        public YzjHttpHeadersBuilder keys(String... key) {
            this.keys.addAll(Arrays.asList(key));
            return this;
        }

        public YzjHttpHeadersBuilder values(String... value) {
            this.values.addAll(Arrays.asList(value));
            return this;
        }

        public YzjHttpHeadersBuilder key(String key) {
            this.key = key;
            return this;
        }

        public YzjHttpHeadersBuilder value(String value) {
            this.value = value;
            return this;
        }

        public YzjHttpHeaders build() {
            YzjHttpHeaders yzjHttpHeaders = new YzjHttpHeaders();
            yzjHttpHeaders.setKey(key);
            yzjHttpHeaders.setValue(value);
            if (!CollectionUtils.isEmpty(keys) && !CollectionUtils.isEmpty(values)) {
                if (keys.size() != values.size()) {
                    throw new IllegalArgumentException("键值长度必须与值长度相同，并且请自行确保顺序对应。");
                }
            }
            yzjHttpHeaders.setKeys(keys);
            yzjHttpHeaders.setValues(values);
            return yzjHttpHeaders;
        }
    }
}
