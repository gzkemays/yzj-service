package com.yeziji.utils;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Nanoid 工具包
 *
 * @author gzkemays
 * @since 2022/3/28 23:39
 */
public class NanoIdUtils {
    public static final SecureRandom DEFAULT_NUMBER_GENERATOR = new SecureRandom();
    public static final char[] DEFAULT_ALPHABET =
            "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%^&*()+=".toCharArray();
    public static final char[] DEFAULT_ALPHABET_NOT_SYMBOL =
            "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    public static final int DEFAULT_SIZE = 21;

    private NanoIdUtils() {
    }

    public static String randomNanoId() {
        return randomNanoId(DEFAULT_NUMBER_GENERATOR, DEFAULT_ALPHABET, DEFAULT_SIZE);
    }

    public static String randomNotSymbolNaoId() {
        return randomNanoId(DEFAULT_NUMBER_GENERATOR, DEFAULT_ALPHABET_NOT_SYMBOL, DEFAULT_SIZE);
    }

    public static String randomNaoId(int num) {
        num = num <= 0 ? DEFAULT_SIZE : num;
        return randomNanoId(DEFAULT_NUMBER_GENERATOR, DEFAULT_ALPHABET, num);
    }

    public static String randomNanoId(Random random, char[] alphabet, int size) {
        if (random == null) {
            throw new IllegalArgumentException("random cannot be null.");
        } else if (alphabet == null) {
            throw new IllegalArgumentException("alphabet cannot be null.");
        } else if (alphabet.length != 0 && alphabet.length < 256) {
            if (size <= 0) {
                throw new IllegalArgumentException("size must be greater than zero.");
            } else {
                int mask = (2 << (int) Math.floor(Math.log(alphabet.length - 1) / Math.log(2.0D))) - 1;
                int step = (int) Math.ceil(1.6D * (double) mask * (double) size / (double) alphabet.length);
                StringBuilder idBuilder = new StringBuilder();
                while (true) {
                    byte[] bytes = new byte[step];
                    random.nextBytes(bytes);
                    for (int i = 0; i < step; ++i) {
                        int alphabetIndex = bytes[i] & mask;
                        if (alphabetIndex < alphabet.length) {
                            idBuilder.append(alphabet[alphabetIndex]);
                            if (idBuilder.length() == size) {
                                return idBuilder.toString();
                            }
                        }
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("alphabet must contain between 1 and 255 symbols.");
        }
    }

    public static String randomNotSymbolNaoId(int num) {
        num = num <= 0 ? DEFAULT_SIZE : num;
        return randomNanoId(DEFAULT_NUMBER_GENERATOR, DEFAULT_ALPHABET_NOT_SYMBOL, num).replaceAll("-", "").replaceAll("_", "");
    }

    public static void main(String[] args) {
        // nyPPc2k8uZ#7^8MHb+R%VmZawXWR21@
        System.out.println(NanoIdUtils.randomNotSymbolNaoId(32));
    }
}
