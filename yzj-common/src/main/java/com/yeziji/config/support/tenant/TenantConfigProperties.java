package com.yeziji.config.support.tenant;

import com.yeziji.config.support.tenant.base.TenantConfigMeta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * 租户配置
 *
 * @author hwy
 * @since 2024/01/11 15:19
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "yzj.tenant")
public class TenantConfigProperties {
    /**
     * 是否启用
     */
    private boolean enabled;

    /**
     * 通过读取文件的形式加载配置
     */
    private String dbPath;

    /**
     * 如果配置为 true，在每次初始化读取配置时都会刷新配置文件
     *
     * <p>可以先在配置中设置对应的参数，然后设置为 true，生成指定的文件；然后再将配置的参数删除</p>
     */
    private boolean flushPath = false;

    /**
     * 中心数据源
     *
     * <p>中心数据源：用于读取注册数据源, <font color='red'>必须存在 platform_datasource 表</font></p>
     */
    private Map<String, String> centerDatasource;

    /**
     * 租户配置参数
     *
     * <p>配置该属性后，默认连接 commonDatasource 数据源</p>
     */
    private TenantConfigMeta tenantMeta;
}
