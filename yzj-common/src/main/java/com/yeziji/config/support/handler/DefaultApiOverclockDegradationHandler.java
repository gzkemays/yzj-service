package com.yeziji.config.support.handler;

import com.yeziji.common.CommonResult;
import com.yeziji.common.base.ApiOverclockDegradationHandlerBase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 默认超频行为
 *
 * @author hwy
 * @since 2024/03/22 11:03
 **/
public class DefaultApiOverclockDegradationHandler implements ApiOverclockDegradationHandlerBase {
    @Override
    public CommonResult<Object> ban(HttpServletRequest request, HttpServletResponse response) {
        return CommonResult.failed("system-api-lock-show-error.access-denied");
    }

    @Override
    public CommonResult<Object> interval(HttpServletRequest request, HttpServletResponse response) {
        return CommonResult.failed("system-api-lock-show-error.dont-visit-frequently");
    }
}
