package com.yeziji.config.support.redis;

/**
 * Redis 过期处理器
 *
 * @author hwy
 * @since 2024/11/29 10:38
 **/
public interface RedisExpiredHandler {
    /**
     * 监听键值
     * <p>aa:bb:*</p>
     * @return {@link String} 监听的键值
     */
    String getKey();

    /**
     * 处理 key 的过期行为
     */
    void handler(String key);
}
