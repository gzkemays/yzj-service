package com.yeziji.config.support.tenant;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 租户配置
 *
 * @author hwy
 * @since 2024/01/11 15:35
 **/
@Configuration
@EnableConfigurationProperties(TenantConfigProperties.class)
@ConditionalOnProperty(name = "yzj.tenant.enabled")
public class TenantConfig {
    private final TenantConfigProperties tenantConfigProperties;

    public TenantConfig(TenantConfigProperties tenantConfigProperties) {
        this.tenantConfigProperties = tenantConfigProperties;
    }


}
