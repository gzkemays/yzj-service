package com.yeziji.config.support.tenant.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 租户配置属性
 *
 * @author hwy
 * @since 2024/01/11 15:26
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TenantConfigMeta {
    /**
     * 平台名称
     */
    private String platform;

    /**
     * 多租户通用数据源
     *
     * <p>通用数据源：各个表中抽出来的通用业务表</p>
     */
    private Map<String, String> commonDatasource;
}
