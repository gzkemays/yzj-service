package com.yeziji.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 线程池配置
 *
 * @author hwy
 * @since 2023/4/9 4:14
 */
@Configuration
public class ThreadPoolConfig {
    int processors = Runtime.getRuntime().availableProcessors();

    @Bean("customExecutor")
    public ThreadPoolTaskExecutor customExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 线程池中的线程名前缀
        executor.setThreadNamePrefix("普通线程池执行--");
        executor.setCorePoolSize(8);
        executor.setMaxPoolSize(16);
        // 设置线程池关闭的时候需要等待所有任务都完成 才能销毁其他的Bean
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住
        executor.setAwaitTerminationSeconds(180);
        // 任务阻塞队列的大小
        executor.setQueueCapacity(20000);
        return executor;
    }

    @Bean("ioTaskExecutor")
    public ThreadPoolTaskExecutor ioTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 线程池中的线程名前缀
        executor.setThreadNamePrefix("IO 密集型线程池执行--");
        executor.setCorePoolSize(processors + 1);
        executor.setMaxPoolSize(processors * 2 + 1);
        // 设置线程池关闭的时候需要等待所有任务都完成 才能销毁其他的Bean
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住
        executor.setAwaitTerminationSeconds(300);
        // 任务阻塞队列的大小
        executor.setQueueCapacity(20000);
        return executor;
    }

    @Bean("cpuTaskExecutor")
    public ThreadPoolTaskExecutor cpuTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 线程池中的线程名前缀
        executor.setThreadNamePrefix("CPU 密集型线程池执行--");
        executor.setCorePoolSize(processors * 2 + 1);
        executor.setMaxPoolSize(processors * 4 + 1);
        // 设置线程池关闭的时候需要等待所有任务都完成 才能销毁其他的Bean
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住
        executor.setAwaitTerminationSeconds(120);
        // 任务阻塞队列的大小
        executor.setQueueCapacity(20000);
        return executor;
    }
}
