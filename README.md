# 椰子鸡开发模块

> 模块一切依旧服务于 SpringBoot，ORM 框架底层还是 Mybatis，搭配 Mybatis-Flex 充当 jdbc 工具实现数据库业务。

该项目是为了**尽量让程序避免分布式**
，降低服务器成本。这样做的原因是因为市面上很多优秀的项目以及完善好的功能需要独立地去搭建部署实现交互。\
类似于配置中心(apollo/nacos)、网关(spring-cloud-gateway)、运维中心(devops)、日志中心(elk)和自动化部署(jenkins)
等等……一系列功能对于一些大型项目也许是必需品，但是对于普遍的小型项目来说就显得有点臃肿且复杂。并且部署起来虽说不麻烦，但是实际维护成本却是大大的增加。\
那么如果将它们的功能进行简化，虽说性能上比不上它们，但是只要做到实现小型项目快速开发的同时具备了以上的一系列要用到功能就可以在成本上得到巨大的节约，同时各类想要的功能都能体现在项目中。\
但这不可避免的是一个单机项目功能模块逐渐庞大时，它的内存占用比也会不可避免的增加，所以在实际开发中还是需要选择性的引用。

## 运行服务以（yzj-xxx-service）进行命名
> 主要提供 xxx 服务

### 官方网站服务（yzj-website-service）【DEMO: see yzj-website branch】
> 该服务在本分支中主要负责测试各个模块的使用

## 模块组件（modules）
> 各个服务之间的相互协同，以及集成工具类用模块进行整合

### RPC模块 （yzj-api）
> 如果确实需要拆分服务，那么服务与服务之间的交互接口应该定义在该模块中

### 公共模块 （yzj-common）
> 各类项目的公共配置、辅助工具类和一些通用的服务（redis、rbac 权限增删改等……）

### 安全配置模块（yzj-security）
> 各个 springboot 项目可以引用当前模块进行权限控制；最基础的配置可以看 demo service security 的配置

### 代码生成模块（yzj-code-generator）
> 基于 mybatis-flex 快速生成项目的基本业务代码，不需要每个项目都重复编写生成类。

### 支付模块（yzj-pay-spring-boot-starter）【未开工】
> 基于 IJPay 的二次封装，将多元类的风格转换成一个类，这样就不需要根据不同的支付方式编写多套调用 API 的接口。

### 动态配置模块（yzj-config-spring-boot-starter）【未完善，存在 BUG】
> 基于扫描文件的形式动态注入 spring-boot 配置当中，遵循 restful-api 风格实现动态修改项目的配置。
> 该模块会自带 controller 开放接口，需要开发者手动配置权限。

### 定时任务模块（yzj-job-spring-boot-stater）
> 基于 quartz 实现动态定时任务，启用时需要引入包并在 application 中启用 @EnabledJobAutoConfiguration；

通过调用 @YzjJobBean 定义类，@YzjJob("test") 定义任务名实现快速定义定时任务。该包不会自带 controller 开放接口，需要开发者手动为自己的应用接入。
但是提供了 JobControllerBase 规范类，继承可以直接调用已有的 service 逻辑，开发者也可以直接修改扩展 JobControllerBase 自己的业务逻辑。

### 运维模块 （yzj-devops-spring-boot-starter）【未开工】
> 主要实现记录程序日志、登记响应缓慢的接口、方法栈分析等……（想到啥再研究研究）

### RBAC 建表 DDL
```sql
-- 系统配置表（可以没有）
CREATE TABLE `system_config`
(
    `id`           bigint NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `config_type`  tinyint    DEFAULT NULL COMMENT '配置类型',
    `config_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '配置值',
    `create_time`  datetime   DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`  datetime   DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`    tinyint(1) DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='系统配置表';

-- 系统角色表
CREATE TABLE `system_role`
(
    `id`          bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `role_name`   varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
    `create_time` datetime                                                              DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime                                                              DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`   tinyint(1)                                                            DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='系统角色表';
INSERT INTO system_role (id, role_name)
VALUES (1, '管理员用户');

-- 系统角色权限关联表
CREATE TABLE `system_role_permission_relation`
(
    `id`            bigint NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `role_id`       bigint     DEFAULT NULL COMMENT '角色 id',
    `permission_id` bigint     DEFAULT NULL COMMENT '权限 id',
    `create_time`   datetime   DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime   DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`     tinyint(1) DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='系统角色权限关联表';
INSERT INTO system_role_permission_relation (id, role_id, permission_id)
VALUES (1, 1, 1);

-- 系统用户角色关联表
CREATE TABLE `system_user_role_relation`
(
    `id`          bigint NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `user_id`     bigint NOT NULL COMMENT '用户 id',
    `role_id`     bigint     DEFAULT NULL COMMENT '角色 id',
    `create_time` datetime   DEFAULT CURRENT_TIMESTAMP,
    `update_time` datetime   DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `is_delete`   tinyint(1) DEFAULT '0' COMMENT '删除标识',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `system_user_role_relation_FK` (`user_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='系统用户角色关联表';

-- 系统权限表（新版）
CREATE TABLE `system_permission`
(
    `id`          bigint(20)                              NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `type`        tinyint(4)                              NOT NULL DEFAULT '0' COMMENT '权限类型：SystemPermissionTypeEnum',
    `parent_id`   bigint(20)                              NOT NULL COMMENT '父级 id',
    `key`         varchar(50) COLLATE utf8mb4_general_ci  NOT NULL DEFAULT '' COMMENT '唯一 key',
    `name`        varchar(100) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限名称',
    `show_name`   varchar(100) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '显示名称（建议为 i18n 的 key）',
    `params`      varchar(128) COMMENT '权限参数：可以是路径也可以是json字符串',
    `order`       int(11)                                 NOT NULL DEFAULT '0' COMMENT '优先级',
    `create_time` datetime                                         DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime                                         DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`   tinyint(1)                              NOT NULL DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`),
    UNIQUE KEY `system_permission_unique` (`type`, `key`),
    KEY `system_permission_type_IDX` (`type`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='系统权限表';

-- 系统角色前端权限关联表
CREATE TABLE `system_role_front_permission_relation`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `role_id`             bigint(20) NOT NULL COMMENT '角色 id',
    `front_permission_id` bigint(20) NOT NULL COMMENT '事件权限 id',
    `create_time`         datetime            DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         datetime            DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`           tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`),
    KEY `system_role_front_permission_relation_role_id_IDX` (`role_id`) USING BTREE,
    KEY `system_role_front_permission_relation_front_permission_id_IDX` (`front_permission_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='系统角色前端权限关联表';

-- 定时任务表
CREATE TABLE `job_info`
(
    `id`                 bigint(20)                                                    NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `name`               varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '任务名称',
    `group_name`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务分组名称',
    `invoke_target`      varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '执行目标',
    `invoke_params`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci          DEFAULT NULL COMMENT '执行参数',
    `cron`               varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci           DEFAULT NULL COMMENT 'cron 执行表达式',
    `misfire_policy`     tinyint(4)                                                             DEFAULT '0' COMMENT '执行异常策略： 默认 0 （详见：JobMisfirePolicyEnum）',
    `is_concurrent`      tinyint(4)                                                             DEFAULT '0' COMMENT '是否允许并发',
    `status`             tinyint(4)                                                    NOT NULL DEFAULT '0' COMMENT '任务状态：暂停 0（详见：JobStatusEnum）',
    `responsible_person` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci           DEFAULT NULL COMMENT '负责人',
    `remark`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci          DEFAULT NULL COMMENT '任务备注',
    `create_time`        datetime                                                               DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        datetime                                                               DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`          tinyint(1)                                                    NOT NULL DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `job_info_name_IDX` (`name`) USING BTREE,
    KEY `job_info_group_name_IDX` (`group_name`) USING BTREE,
    KEY `job_info_invoke_target_IDX` (`invoke_target`) USING BTREE,
    KEY `job_info_responsible_person_IDX` (`responsible_person`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='定时任务信息';

-- 定时任务日志表
CREATE TABLE `job_log`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
    `job_id`              bigint(20) NOT NULL                                           DEFAULT '0' COMMENT '日志的任务 id',
    `msg`                 blob COMMENT '日志信息',
    `execute_ip`          varchar(100) COLLATE utf8mb4_general_ci                       DEFAULT NULL COMMENT '执行 ip',
    `execute_flag`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行标识',
    `execute_params`      varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行参数',
    `execute_result_code` tinyint(4)                                                    DEFAULT NULL COMMENT '执行结果码：JobExecuteResultCodeEnum',
    `retry_count`         int(11)                                                       DEFAULT '0' COMMENT '任务重试次数',
    `notice_status`       tinyint(1)                                                    DEFAULT NULL COMMENT '通知状态',
    `execute_time`        datetime                                                      DEFAULT NULL COMMENT '执行时候的时间',
    `create_time`         datetime                                                      DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         datetime                                                      DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_delete`           tinyint(1) NOT NULL                                           DEFAULT '0' COMMENT '逻辑删除标识',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='任务日志';
```

## ~~多平台多租户模式~~
> 指定一个中心数据源库，然后再其数据源下建立该表
```sql
-- yzj_common.platform_datasource definition
CREATE TABLE `platform_datasource` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
  `platform_name` varchar(100) DEFAULT NULL COMMENT '平台名称',
  `url` varchar(255) NOT NULL COMMENT 'jdbc 地址',
  `username` longtext NOT NULL COMMENT 'jdbc 账号',
  `password` longtext NOT NULL COMMENT 'jdbc 密码',
  `config_params` varchar(255) DEFAULT NULL COMMENT 'jdbc 配置参数',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '简介',
  `responsible_person` varchar(255) DEFAULT NULL COMMENT '负责人',
  `responsible_person_contact` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '负责人联系方式',
  `renewal_amount` decimal(12,2) DEFAULT NULL COMMENT '续费金额',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否为指定默认数据源',
  `expired_time` datetime DEFAULT NULL COMMENT '数据到期时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`),
  UNIQUE KEY `platform_datasource_unique` (`platform_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台数据源管理表';
```

> ~~指定一个业务通用数据源库，然后再其数据源下建立该表（任意其他通用业务也可以放置该表中，然后通过中心源来获取指定的 platform_id）~~
```sql
-- xxx_common.system_config definition
CREATE TABLE `system_config` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
                                 `config_type` tinyint(4) DEFAULT NULL COMMENT '配置类型',
                                 `config_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '配置值',
                                 `platform_id` bigint(20) DEFAULT '0' COMMENT '平台 id，0 为全平台通用',
                                 `is_delete` tinyint(1) DEFAULT '0' COMMENT '删除标识',
                                 `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统配置';

-- xxx_common.system_permission definition
CREATE TABLE `system_permission` (
                                     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
                                     `release_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '放行接口( /xx/*)',
                                     `platform_id` bigint(20) DEFAULT NULL COMMENT '平台 id，0 为全平台通用',
                                     `is_delete` tinyint(1) DEFAULT '0' COMMENT '删除标识',
                                     `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                     `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='权限表';

-- xxx_common.system_role definition
CREATE TABLE `system_role` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
                               `role_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '角色名称',
                               `platform_id` bigint(20) DEFAULT '0' COMMENT '平台 id，0 为全平台通用',
                               `is_delete` tinyint(1) DEFAULT '0' COMMENT '删除标识',
                               `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- xxx_common.system_role_permission_relation definition
CREATE TABLE `system_role_permission_relation` (
                                                   `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
                                                   `role_id` bigint(20) DEFAULT NULL COMMENT '角色 id',
                                                   `permission_id` bigint(20) DEFAULT NULL COMMENT '权限 id',
                                                   `platform_id` bigint(20) DEFAULT '0' COMMENT '平台 id，0 为全平台通用',
                                                   `is_delete` tinyint(1) DEFAULT '0' COMMENT '删除标识',
                                                   `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                                   `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='角色权限关联表';

-- xxx_common.system_user_role_relation definition
CREATE TABLE `system_user_role_relation` (
                                             `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增 id',
                                             `user_id` bigint(20) NOT NULL COMMENT '用户 id, 可能是微信, 也可能是 member',
                                             `role_id` bigint(20) DEFAULT NULL COMMENT '角色 id',
                                             `platform_id` bigint(20) DEFAULT NULL COMMENT '平台 id，默认为 0 全平台',
                                             `is_delete` tinyint(1) DEFAULT '0' COMMENT '删除标识',
                                             `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
                                             `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                                             PRIMARY KEY (`id`) USING BTREE,
                                             KEY `system_user_role_relation_FK` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='用户角色关联表';
```