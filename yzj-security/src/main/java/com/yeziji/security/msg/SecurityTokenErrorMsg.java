package com.yeziji.security.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 加密令牌异常信息
 *
 * @author hwy
 * @since 2023/11/12 17:16
 **/
@Getter
@AllArgsConstructor
public enum SecurityTokenErrorMsg implements CommonErrorEnum {
    /**
     * 初始 token 为空
     */
    ORIGINAL_TOKEN_IS_BLANK(ERROR, UNAUTHORIZED, "system-show-error.unauthorized");

    private final int code;
    private final String desc;
    private final String showMsg;
}
