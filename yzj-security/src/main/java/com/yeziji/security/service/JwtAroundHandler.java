package com.yeziji.security.service;

/**
 * 提供给应用可以自定义 JWT 过滤器前后操作
 *
 * @author hwy
 * @since 2024/09/27 17:25
 **/
public interface JwtAroundHandler {
    /**
     * JWT 拦截处理前的操作
     */
    void onBefore();

    /**
     * JWT 拦截异常的操作
     */
    void onException();

    /**
     * JWT 拦截后的操作
     */
    void onComplete();
}
