package com.yeziji.security.service;

import com.yeziji.common.base.UserOnlineBase;
import com.yeziji.security.common.SecurityUserBase;

/**
 * 用户登录服务接口
 *
 * @author hwy
 * @since 2023/12/04 9:33
 **/
public interface DynamicSecurityUserService<T extends UserOnlineBase, U extends SecurityUserBase> {
    /**
     * 登录
     *
     * @param securityUser 继承 SecurityUserBase 类
     * @return {@link T} 可以任意返回参数
     */
    T login(U securityUser);

    /**
     * 注册
     *
     * @param securityUser 继承 SecurityUserBase 类
     * @return {@link Boolean} 是否注册成功
     */
    boolean register(U securityUser);

    /**
     * 登出当前会话信息
     *
     * @return {@link Boolean} 登出结果
     */
    boolean logout();
}
