package com.yeziji.security.service;

import com.yeziji.common.Platform;
import com.yeziji.security.common.logger.LoggerIgnoreInfo;
import org.springframework.security.access.ConfigAttribute;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 动态安全认证服务
 *
 * @author hwy
 * @since 2023/11/12 20:23
 **/
public interface DynamicSecurityAuth {
    /**
     * 获取平台名称
     *
     * @return {@link Platform} 平台信息
     */
    Platform getPlatform();

    /**
     * 获取不用校验的地址
     *
     * @return {@link Set} 忽略的地址集合
     */
    Set<String> getIgnoreUrls();

    /**
     * 获取不用打印日志的地址
     *
     * @return {@link Set} 忽略的地址集合
     */
    Set<LoggerIgnoreInfo> getIgnoreLogsUrls();

    /**
     * 获取地址角色权限的关联集合
     *
     * @return {@link Map} key: path（地址）, value: roles（一个地址可以被多个角色包含）
     */
    Map<String, List<ConfigAttribute>> getPathRoleMap();
}
