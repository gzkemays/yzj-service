package com.yeziji.security;

import com.yeziji.security.common.SecurityDbConfig;
import com.yeziji.security.service.DynamicSecurityDbAuth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * 数据库配置
 *
 * @author hwy
 * @since 2023/11/13 21:36
 **/
@Slf4j
@Configuration
public class DbConfig {
    @Resource
    private SecurityDbConfig securityDbConfig;
    @Resource
    private DynamicSecurityDbAuth dynamicSecurityDbAuth;

    @Bean("dataSource")
    @ConditionalOnMissingBean(name = "dataSource")
    public DataSource getDataSource() {
        return dynamicSecurityDbAuth.getSecurityDataSource(securityDbConfig);
    }
}
