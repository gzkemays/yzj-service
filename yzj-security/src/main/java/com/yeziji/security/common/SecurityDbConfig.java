package com.yeziji.security.common;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 安全数据库常用配置
 *
 * @author hwy
 * @since 2023/11/13 21:38
 **/
@Data
@Component
@ConfigurationProperties(prefix = "spring.datasource.druid")
public class SecurityDbConfig {
    /**
     * 最小有效连接：关掉多余连接，保留有效连接
     */
    private Integer minIdle;

    /**
     * 最大连接池活跃数
     */
    private Integer maxActive;

    /**
     * 获取连接时的最大等待时间，单位为 ms
     */
    private Integer maxWait;

    /**
     * 连接池启动时的初始值
     */
    private Integer initialSize;

    /**
     *  连接在连接池中最小的空闲时间，单位为 ms。
     */
    private Long minEvictableIdleTimeMillis;

    /**
     * 连接在连接池中最大的空闲时间，单位为 ms。
     */
    private Long maxEvictableIdleTimeMillis;

    /**
     *  是否在空闲时检查连接的有效性
     */
    private Boolean testWhileIdle;

    /**
     * 关闭空闲连接检测时间（毫秒）：交给 destroy 线程检测连接空闲时间，大于指定时间就进行物理关闭
     */
    private Long timeBetweenEvictionRunsMillis;

    /**
     * 连接出错后重试时间间隔（毫秒）
     */
    private Long timeBetweenConnectErrorMillis;

    /**
     * 连接出错后再尝试连接的次数
     */
    private Integer connectionErrorRetryAttempts;

    /**
     * 异步初始化策略
     */
    private Boolean asyncInit;

    /**
     * 是否自动回收超时连接
     */
    private Boolean removeAbandoned;

    /**
     * 超时时间（秒）
     */
    private Long removeAbandonedTimeout;

    /**
     * 事务超时时间（秒）
     */
    private Long transactionQueryTimeout;

    /**
     * 数据库链接地址
     */
    private String url;

    /**
     * 数据库账号
     */
    private String username;

    /**
     * 数据库密码
     */
    private String password;

    /**
     * 确保存在数据库登录信息
     *
     * @return {@link Boolean} 判断是否存在数据库登录信息
     */
    public boolean isHasLoginDbInfo() {
        return StrUtil.isNotBlank(url) &&
                StrUtil.isNotBlank(username) &&
                StrUtil.isNotBlank(password);
    }
}
