package com.yeziji.security.common;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

import java.util.Collection;

/**
 * 自定义角色权限消息
 *
 * @author hwy
 * @since 2023/12/04 21:53
 **/
@Data
@NoArgsConstructor
public class CustomerGrantedAuthority implements GrantedAuthority {
    private String authority;

    private Collection<String> releasesUrls;

    public CustomerGrantedAuthority(String role) {
        Assert.hasText(role, "A granted authority textual representation is required");
        this.authority = role;
    }

    public CustomerGrantedAuthority(String authority, Collection<String> releasesUrls) {
        this.authority = authority;
        this.releasesUrls = releasesUrls;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            return obj instanceof CustomerGrantedAuthority && this.authority.equals(((CustomerGrantedAuthority) obj).authority);
        }
    }

    public int hashCode() {
        return this.authority.hashCode();
    }

    public String toString() {
        return this.authority;
    }
}
