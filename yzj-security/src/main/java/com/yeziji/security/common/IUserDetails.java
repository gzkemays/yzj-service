package com.yeziji.security.common;

import com.google.common.collect.Lists;
import com.yeziji.common.base.IUserDetailsBase;
import com.yeziji.utils.expansion.Opt2;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * 自定义的用户详情
 *
 * @author hwy
 * @since 2023/11/09 21:20
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class IUserDetails extends IUserDetailsBase implements UserDetails {
    private static final long serialVersionUID = 6819528325967272929L;

    /**
     * 用户权限
     */
    private Collection<CustomerGrantedAuthority> authorities;

    /**
     * 权限是否为非过期
     */
    private boolean credentialsNonExpired = true;

    /**
     * 账号是否未过期
     */
    private boolean accountNonExpired = false;

    /**
     * 账号是否未锁定
     */
    private boolean accountNonLocked = true;

    /**
     * 用户是否可用
     */
    private boolean enabled = true;

    @Override
    public Collection<CustomerGrantedAuthority> getAuthorities() {
        return Opt2.nullElse(this.authorities, Lists.newArrayList());
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
