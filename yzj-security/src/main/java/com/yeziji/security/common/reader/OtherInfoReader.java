package com.yeziji.security.common.reader;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 读取 Spring 信息配置
 *
 * <p>
 *     会将读取到的信息类似于 spring 配置进行初始化, 存储方式以 key: value 的形式,如: spring.redis.host: 127.0.0.1
 * </p>
 * @author hwy
 * @since 2024/02/17 11:44
 **/
@Data
@Component
@ConfigurationProperties(prefix = "yzj.security.other.info")
public class OtherInfoReader {
    /**
     * 默认的配置信息
     */
    private Map<String, Object> params;
}
