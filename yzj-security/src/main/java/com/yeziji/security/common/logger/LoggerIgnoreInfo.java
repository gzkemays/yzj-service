package com.yeziji.security.common.logger;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 日志忽略信息
 *
 * @author hwy
 * @since 2024/03/04 10:55
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoggerIgnoreInfo {
    /**
     * 忽略主要地址
     */
    private String ignoreUrl;

    /**
     * 忽略的请求地址
     */
    private String ignoreRequestUrl;

    /**
     * 忽略的响应地址
     */
    private String ignoreResponseUrl;
}
