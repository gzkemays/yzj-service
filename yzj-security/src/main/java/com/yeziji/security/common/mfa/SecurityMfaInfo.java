package com.yeziji.security.common.mfa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * security mfa 信息
 *
 * @author hwy
 * @since 2024/11/18 23:29
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecurityMfaInfo {
    /**
     * 用户的 mfa 名称
     */
    private String accountName;

    /**
     * 对应的 secureKey
     */
    private String secureKey;
}
