package com.yeziji.security.common.reader;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 基本信息
 *
 * @author hwy
 * @since 2024/03/25 20:11
 **/
@Data
@Component
@ConfigurationProperties(prefix = "yzj.security")
public class BaseReader {
    /**
     * 如果配置为 true，在每次初始化读取配置时都会刷新配置文件
     *
     * <p>可以先在配置中设置对应的参数，然后设置为 true，生成指定的文件；然后再将配置的参数删除</p>
     */
    private boolean flushPath = false;

    /**
     * 是否启用 redis 缓存，启用后会直接调用项目的 common.redis
     */
    private boolean openRedisCache = false;
}
