package com.yeziji.security.common.reader;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 读取数据库信息
 *
 * @author hwy
 * @since 2023/11/13 22:12
 **/
@Data
@Component
@ConfigurationProperties(prefix = "yzj.security.db.info")
public class DbInfoReader {
    /**
     * 读取根目录
     */
    private String path;

    /**
     * 数据库驱动名称
     */
    private String driverClassName;

    /**
     * 默认的配置信息
     */
    private Map<String, Object> defaultParams;
}
