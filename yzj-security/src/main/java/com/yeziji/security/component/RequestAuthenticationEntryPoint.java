package com.yeziji.security.component;

import cn.hutool.json.JSONUtil;
import com.yeziji.common.CommonResult;
import com.yeziji.common.context.OnlineContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 未登录或登录过期
 *
 * @author hwy
 * @since 2023/09/04 2:28
 **/
@Slf4j
public class RequestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        if (!OnlineContext.isHasSession()) {
            log.info("{} 未登录或登录过期: {} -- {}", OnlineContext.getIp(), request.getRequestURI(), authException.getMessage());
        } else {
            log.info("{} 未登录或登录过期: {} -- {}", OnlineContext.getUserId(), request.getRequestURI(), authException.getMessage());
        }

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Cache-Control", "no-cache");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(CommonResult.unauthorized()));
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().flush();
    }
}