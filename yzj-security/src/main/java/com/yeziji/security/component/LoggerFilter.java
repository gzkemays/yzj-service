package com.yeziji.security.component;

import cn.hutool.core.util.StrUtil;
import com.yeziji.common.CommonErrorMsg;
import com.yeziji.common.CommonResult;
import com.yeziji.common.context.OnlineContext;
import com.yeziji.constant.VariousStrPool;
import com.yeziji.exception.ApiException;
import com.yeziji.security.common.logger.LoggerIgnoreInfo;
import com.yeziji.security.common.logger.LoggerRequestWrapper;
import com.yeziji.security.common.logger.LoggerResponseWrapper;
import com.yeziji.security.service.DynamicSecurityAuth;
import com.yeziji.utils.DateUtils;
import com.yeziji.utils.ResponseUtils;
import com.yeziji.utils.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Nonnull;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * 日志拦截器
 *
 * @author hwy
 * @since 2023/11/12 23:11
 **/
@Slf4j
public class LoggerFilter extends OncePerRequestFilter {
    @Autowired(required = false)
    private DynamicSecurityAuth dynamicSecurityAuth;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    @Nonnull HttpServletResponse response,
                                    @Nonnull FilterChain filterChain) throws ServletException, IOException {
        String mdc = MDC.get(VariousStrPool.System.TRACE_ID);
        String uri = request.getRequestURI();
        try {
            String contentType = request.getContentType();
            if (contentType != null && contentType.contains("multipart/form-data")) {
                filterChain.doFilter(request, response);
                return;
            }
            // 忽略日志地址不打印日志
            if (dynamicSecurityAuth.getIgnoreLogsUrls().stream()
                    .map(LoggerIgnoreInfo::getIgnoreUrl)
                    .filter(StrUtil::isNotBlank)
                    .anyMatch(ignoreUrl -> ServletUtils.urlContains(ignoreUrl, uri))) {
                filterChain.doFilter(request, response);
                return;
            }
            Set<LoggerIgnoreInfo> ignoreLogsUrls = dynamicSecurityAuth.getIgnoreLogsUrls();
            Set<String> ignoreReqUrls = new HashSet<>();
            Set<String> ignoreResUrls = new HashSet<>();
            ignoreLogsUrls.forEach(ignoreInfo -> {
                ignoreReqUrls.add(ignoreInfo.getIgnoreRequestUrl());
                ignoreResUrls.add(ignoreInfo.getIgnoreResponseUrl());
            });
            final boolean isIgnoreReq = ignoreReqUrls.stream().filter(StrUtil::isNotBlank).anyMatch(ignoreUrl -> ServletUtils.urlContains(ignoreUrl, uri));
            final boolean isIgnoreRes = ignoreResUrls.stream().filter(StrUtil::isNotBlank).anyMatch(ignoreUrl -> ServletUtils.urlContains(ignoreUrl, uri));
            // 获取请求和响应的流对象
            LoggerRequestWrapper loggerRequestWrapper = new LoggerRequestWrapper(request);
            LoggerResponseWrapper loggerResponseWrapper = new LoggerResponseWrapper(response);
            // 获取 ip
            final String ip = OnlineContext.getIp();
            // 打印请求
            String reqLoggerFormatter = "[{}: {}] {}=={} 请求: {};";
            if (!isIgnoreReq) {
                reqLoggerFormatter += " 参数: {}";
            }
            if (OnlineContext.isHasSession()) {
                if (isIgnoreReq) {
                    log.info(reqLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, OnlineContext.getUserId(), request.getRequestURI());
                } else {
                    log.info(reqLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, OnlineContext.getUserId(), request.getRequestURI(), loggerRequestWrapper.getBody());
                }
            } else {
                if (isIgnoreReq) {
                    log.info(reqLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, "无登录", request.getRequestURI());
                } else {
                    log.info(reqLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, "无登录", request.getRequestURI(), loggerRequestWrapper.getBody());
                }}
            // 调用开始时间
            long startTime = System.currentTimeMillis();
            // 执行下一步
            filterChain.doFilter(loggerRequestWrapper, loggerResponseWrapper);
            // 调用结束时间
            long endTime = System.currentTimeMillis();
            // 记录总耗时
            String usageTime = DateUtils.formatTime(endTime - startTime);
            // 打印响应
            String resLoggerFormatter = "[{}: {}] {}=={} 请求: {} 结束;";
            if (!isIgnoreRes) {
                resLoggerFormatter += " 响应: {}; 耗时: {}";
            } else {
                resLoggerFormatter += " 耗时: {}";
            }
            if (OnlineContext.isHasSession()) {
                if (isIgnoreRes) {
                    log.info(resLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, OnlineContext.getUserId(), request.getRequestURI(), usageTime);
                } else {
                    log.info(resLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, OnlineContext.getUserId(), request.getRequestURI(), loggerResponseWrapper.getBody(), usageTime);
                }
            } else {
                if (isIgnoreRes) {
                    log.info(resLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, "无登录", request.getRequestURI(), usageTime);
                } else {
                    log.info(resLoggerFormatter, OnlineContext.getPlatform(), mdc, ip, "无登录", request.getRequestURI(), loggerResponseWrapper.getBody(), usageTime);
                }
            }
        } catch (Exception e) {
            log.info("LoggerFilter: Do Error: {}", e.getMessage(), e);
            // 直接写入流, 避免重复抛出异常
            if (!(e.getCause() instanceof ApiException)) {
                ResponseUtils.writeJson(response, CommonResult.failed(CommonErrorMsg.SYSTEM_THROW_ERROR));
            } else {
                throw new ApiException(((ApiException) e.getCause()).getErrorEnum());
            }
        } finally {
            MDC.clear();
        }
    }
}
