package com.yeziji.dynamicConfig.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * 自定义 yzj-config 的 restController 用于动态入参
 *
 * @author hwy
 * @since 2023/12/15 18:13
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
@RequestMapping
public @interface ConfigRestController {
    /**
     * Alias for {@link RequestMapping#name}
     */
    @AliasFor(annotation = RequestMapping.class)
    String name() default "";

    /**
     * Alias for {@link RequestMapping#value}
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};

    /**
     * Alias for {@link RequestMapping#path}
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] path() default {};
}
