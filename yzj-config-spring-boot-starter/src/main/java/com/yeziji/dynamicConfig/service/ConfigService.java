package com.yeziji.dynamicConfig.service;

import com.yeziji.dynamicConfig.base.ConfigMeta;

import java.util.List;

/**
 * 配置接口
 *
 * @author hwy
 * @since 2023/12/13 10:13
 **/
public interface ConfigService {
    /**
     * 更新配置
     *
     * @param configMeta 配置元素
     * @return {@link Boolean} 更新结果
     */
    boolean update(ConfigMeta configMeta);

    /**
     * 删除配置
     *
     * @param configMeta 配置元素
     * @return {@link Boolean} 删除结果
     */
    boolean delete(ConfigMeta configMeta);

    /**
     * 根据键值获取配置元素
     *
     * @param key 键值
     * @return {@link ConfigMeta} 配置元素
     */
    ConfigMeta get(String key);

    /**
     * 获取应用的全部配置
     *
     * @return {@link List} 配置列表
     */
    List<ConfigMeta> listAll();
}
