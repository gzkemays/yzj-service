package com.yeziji.dynamicConfig.msg;

import com.yeziji.common.CommonErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 动态配置错误信息
 *
 * @author hwy
 * @since 2023/12/13 17:10
 **/
@Getter
@AllArgsConstructor
public enum DynamicConfigErrorMsg implements CommonErrorEnum {
    CONFIG_IS_NOT_EXISTS(ERROR, "config-error.is-not-exists", "config-show-error.is-not-exists"),
    ;
    private final int code;
    private final String desc;
    private final String showMsg;
}
