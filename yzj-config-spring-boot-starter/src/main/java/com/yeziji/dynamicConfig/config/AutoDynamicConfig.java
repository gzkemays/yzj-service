package com.yeziji.dynamicConfig.config;

import com.yeziji.dynamicConfig.service.ConfigService;
import com.yeziji.dynamicConfig.service.impl.AppConfigServiceImpl;
import com.yeziji.dynamicConfig.service.impl.ConfigurationPropertiesServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动装配
 *
 * @author hwy
 * @since 2023/12/13 10:12
 **/
@Slf4j
@Configuration
@EnableConfigurationProperties(ConfigProperties.class)
@ConditionalOnProperty(name = "yzj.config.enabled", matchIfMissing = true)
public class AutoDynamicConfig {
    /**
     * 讀取配置
     */
    private final ConfigProperties configProperties;

    public AutoDynamicConfig(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    @Bean("appConfigService")
    public ConfigService appConfigService() {
        return new AppConfigServiceImpl(configProperties.getLog());
    }

    @Bean("configurationPropertiesService")
    public ConfigService configurationPropertiesService() {
        return new ConfigurationPropertiesServiceImpl(configProperties.getLog());
    }
}
