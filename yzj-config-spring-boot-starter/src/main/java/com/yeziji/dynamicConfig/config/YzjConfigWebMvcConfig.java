package com.yeziji.dynamicConfig.config;

import com.yeziji.config.support.resolver.RequestSingleBodyHandlerMethodArgumentResolver;
import com.yeziji.dynamicConfig.annotation.ConfigRestController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * 配置具备的 web mvc config
 *
 * @author hwy
 * @since 2023/12/18 12:04
 **/
@Slf4j
@Configuration
@EnableConfigurationProperties(ConfigProperties.class)
@ConditionalOnProperty(name = "yzj.config.enabled", matchIfMissing = true)
public class YzjConfigWebMvcConfig extends WebMvcConfigurationSupport {
    /**
     * 讀取配置
     */
    private final ConfigProperties configProperties;

    public YzjConfigWebMvcConfig(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    @Override
    public void configurePathMatch(@Nonnull PathMatchConfigurer configurer) {
        configurer.addPathPrefix(configProperties.getApiPrefix(), (c) -> c.isAnnotationPresent(ConfigRestController.class));
    }

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new RequestSingleBodyHandlerMethodArgumentResolver());
        super.addArgumentResolvers(argumentResolvers);
    }
}
