package com.yeziji.dynamicConfig.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 配置属性
 *
 * @author hwy
 * @since 2023/12/13 1:25
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "yzj.config")
public class ConfigProperties {
    /**
     * 是否打印日志
     */
    private Boolean log = true;

    /**
     * 动态 api 前缀
     */
    private String apiPrefix = "yzj-config";

    /**
     * 是否启用
     */
    private Boolean enabled = false;
}
