package com.yeziji.dynamicConfig.controller;

import com.yeziji.annotation.RequestSingleBody;
import com.yeziji.common.CommonResult;
import com.yeziji.dynamicConfig.annotation.ConfigRestController;
import com.yeziji.dynamicConfig.base.ConfigMeta;
import com.yeziji.dynamicConfig.controller.vo.ConfigMetaVO;
import com.yeziji.dynamicConfig.service.ConfigService;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 测试
 * @author hwy
 * @since 2023/12/13 11:03
 **/
@Lazy
@ConfigRestController("/configurationProperties")
public class ConfigurationPropertiesController {
    @Resource(name = "configurationPropertiesService")
    private ConfigService configService;

    @GetMapping("/listAll")
    public CommonResult<List<ConfigMetaVO>> listAll() {
        return CommonResult.success(configService.listAll(), ConfigMeta::convertToVo);
    }

    @PostMapping("/get")
    public CommonResult<ConfigMetaVO> get(@RequestSingleBody("key") String key) {
        return CommonResult.success(configService.get(key), ConfigMeta::convertToVo);
    }

    @PostMapping("/update")
    public CommonResult<Boolean> update(@Validated(ConfigMeta.ConfigurationUpdate.class) @RequestBody ConfigMeta configMeta) {
        return CommonResult.success(configService.update(configMeta));
    }

    @PostMapping("/delete")
    public CommonResult<Boolean> delete(@RequestBody ConfigMeta configMeta) {
        return CommonResult.success(configService.delete(configMeta));
    }
}
