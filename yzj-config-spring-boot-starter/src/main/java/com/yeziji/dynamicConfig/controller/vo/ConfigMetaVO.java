package com.yeziji.dynamicConfig.controller.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * config 属性视图对象
 *
 * @author hwy
 * @since 2023/12/13 17:32
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigMetaVO {
    /**
     * 配置环境
     */
    private String env;

    /**
     * 配置键值
     */
    private String key;

    /**
     * 配置值
     */
    private Object value;
}
