package com.yeziji.codegen.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 项目属性
 *
 * @author hwy
 * @since 2023/12/12 14:27
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectParams {
    /**
     * 项目/模块名称
     */
    private String projectName;

    /**
     * 绝对路径
     */
    private String absolutePath;

    /**
     * 相对路径
     */
    private String relativePath;
}
