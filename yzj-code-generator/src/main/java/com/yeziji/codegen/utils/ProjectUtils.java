package com.yeziji.codegen.utils;

import com.yeziji.codegen.base.ProjectParams;
import com.yeziji.common.CommonSymbol;
import com.yeziji.constant.VariousStrPool;
import com.yeziji.utils.expansion.Lists2;
import com.yeziji.utils.expansion.Str2;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 项目工具类
 *
 * @author hwy
 * @since 2023/12/12 14:27
 **/
public class ProjectUtils {
    /**
     * 获取同模块下的项目
     *
     * @return {@link ProjectParams} 项目内容
     */
    public static List<ProjectParams> getProjects() {
        String userDirPath = System.getProperty(VariousStrPool.System.USER_DIR);
        File file = new File(userDirPath);
        List<File> folders =
                Arrays.stream(Objects.requireNonNull(file.listFiles()))
                        .filter(File::isDirectory)
                        .filter(f -> !f.getName().contains(CommonSymbol.FULL_STOP))
                        .collect(Collectors.toList());
        return Lists2.transforms(
                folders,
                folder -> new ProjectParams(folder.getName(), folder.getAbsolutePath(), Str2.replaceAllToEmpty(folder.getPath(), userDirPath))
        );
    }

    public static void main(String[] args) {
        List<ProjectParams> projects = getProjects();
        System.out.println("projects = " + projects);
    }
}
