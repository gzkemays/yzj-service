package com.yeziji.pay.common.base;

import com.yeziji.pay.constant.enums.TradeTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 订单基础类
 *
 * @author hwy
 * @since 2023/12/18 14:52
 **/
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OrderBase {
    /**
     * 支付订单信息
     */
    private Long payOrderId;

    /**
     * 商品名称
     *
     * <p>这个配置是为了可以动态配置商品名称, 不然就跟订单走</p>
     */
    private String tradeName;

    /**
     * 额外值
     */
    private String attach;

    /**
     * 支付金额, 单位: 分
     */
    private Integer amount;

    /**
     * 交易方式类型
     */
    private TradeTypeEnum tradeTypeEnum;
}
