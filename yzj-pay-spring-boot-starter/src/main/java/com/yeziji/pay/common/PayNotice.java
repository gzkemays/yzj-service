package com.yeziji.pay.common;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * 支付回调通知
 *
 * @author hwy
 * @since 2023/09/09 15:55
 **/
public interface PayNotice {
    /**
     * 根据请求头名称获取请求头信息
     *
     * @param name 名称
     * @return {@link String} 请求头值
     */
    String getHeader(String name);

    /**
     * 根据请求头名称获取请求头信息
     *
     * @param name 名称
     * @return {@link Enumeration} 请求头值
     */
    Enumeration<String> getHeaders(String name);

    /**
     * 获取所有的请求头名称
     *
     * @return {@link Enumeration} 所有请求头值
     */
    Enumeration<String> getHeaderNames();

    /**
     * 获取 http servlet request
     *
     * @return {@link HttpServletRequest} 获取原来的请求 request
     */
    HttpServletRequest getHttpServletRequest();
}
