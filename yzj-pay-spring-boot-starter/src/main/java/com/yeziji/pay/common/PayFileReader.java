package com.yeziji.pay.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件读取基本信息
 *
 * @author hwy
 * @since 2023/12/18 16:24
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayFileReader {
    /**
     * 本地读取
     */
    private String localPath;

    /**
     * 线上拉取
     */
    private String remotePath;

    /**
     * 当本地不存在时自动从线上拉取
     */
    private boolean autoDownload = true;

    public PayFileReader(String localPath, String remotePath) {
        this.localPath = localPath;
        this.remotePath = remotePath;
    }
}
