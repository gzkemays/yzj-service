package com.yeziji.pay.common;

import com.yeziji.pay.common.base.OrderBase;

import java.util.Map;

/**
 * 支付服务，用于规范实现支付的业务接口
 *
 * @author hwy
 * @since 2023/12/18 14:47
 **/
public interface PayService {
    /**
     * 发起支付
     *
     * @param orderBase 订单信息
     * @return {@link Map} 发起支付后, 回调信息用 map 接收
     */
    Map<String, String> doPay(OrderBase orderBase);

    /**
     * 发起退款
     *
     * @param orderBase 订单信息
     * @return {@link Map} 发起退款后，回调信息用 map 接收
     */
    Map<String, String> refund(OrderBase orderBase);

    /**
     * 关闭支付
     *
     * @param orderId 支付订单 id
     * @return {@link Boolean} 关闭结果
     */
    boolean close(Long orderId);

    /**
     * 异常处理
     *
     * @param orderBase 异常的订单
     * @param errorMsg  保存失败消息
     */
    void exceptionHandler(OrderBase orderBase, String errorMsg);

    /**
     * 支付回调
     *
     * @param request http request
     * @return {@link Map} 回调报文, 根据不同的平台可以返回对应需要的报文
     */
    Map<String, String> callback(PayNotice request);

    /**
     * 退款回调
     *
     * @param request http request
     * @return {@link Map} 回调报文, 根据不同的平台可以返回对应需要的报文
     */
    Map<String, String> refundCallback(PayNotice request);

    /**
     * 重新确认订单
     *
     * @param orderBase 支付订单
     * @return {@link Map} 回调报文
     */
    Map<String, Object> retryConfirm(OrderBase orderBase);
}
