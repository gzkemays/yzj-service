package com.yeziji.pay.wechat.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yeziji.pay.config.wechat.WechatMerchantConfig;
import com.yeziji.pay.config.wechat.WechatPayConfig;
import com.yeziji.pay.constant.WechatConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 微信 V3 证书信息
 *
 * @author hwy
 * @since 2023/12/18 16:17
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WechatV3CertificatesBase {
    /**
     * 平台证书序列号
     */
    private String certificateSerialNumber;

    /**
     * 序列号
     */
    private String serialNumber;

    /**
     * 加密信息
     */
    @JsonProperty(WechatConstant.ENCRYPT_CERTIFICATE)
    private EncryptCertificate encryptCertificate;

    /**
     * 商户微信支付信息
     */
    private WechatMerchantConfig wechatMerchantConfig;

    /**
     * 微信目录信息
     */
    private WechatPayConfig wechatPayConfig;

    @Data
    static class EncryptCertificate {
        /**
         * 微信会返回的关联数据
         */
        @JsonProperty(WechatConstant.ASSOCIATED_DATA)
        private String associatedData;

        /**
         * 微信密文
         */
        private String ciphertext;

        /**
         * 临时报文
         */
        private String nonce;
    }
}
