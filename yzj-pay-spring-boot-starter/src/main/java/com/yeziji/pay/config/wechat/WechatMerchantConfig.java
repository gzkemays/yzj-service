package com.yeziji.pay.config.wechat;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信商户基础信息
 *
 * @author hwy
 * @since 2023/12/18 16:21
 **/
@Data
@ConfigurationProperties(prefix = "yzj.pay.wechat")
public class WechatMerchantConfig {
    /**
     * 支持读取文件的形式加载配置
     */
    private String configPath;

    /**
     * 商户 appId
     */
    private String appId;

    /**
     * 商户 appSecurity
     */
    private String appSecurity;

    /**
     * 商户号
     */
    private String merchantNumber;

    /**
     * 商户登录号
     */
    private String merchantLoginUsername;

    /**
     * API 密钥
     */
    private String apiKey;

    /**
     * API V3 密钥
     */
    private String apiKeyV3;

    /**
     * 分发给平台的支付回调接口
     */
    private String payDomain;

    /**
     * 分发给平台的退款回调接口
     */
    private String refundDomain;
}
