package com.yeziji.pay.config;

import com.yeziji.utils.NanoIdUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 支付基本配置
 *
 * @author hwy
 * @since 2023/12/18 16:45
 **/
@Data
@ConfigurationProperties(prefix = "yzj.pay")
public class ConfigProperties {
    /**
     * 是否开启启动输入模式（设置读取文件配置之后，如果文件不存在就会进入输入模式）
     */
    private boolean scanner = true;

    /**
     * 是否启用配置加密
     */
    private boolean encrypt = true;

    /**
     * 默认使用 rsa（优先级最高）
     */
    private boolean rsa = true;

    /**
     * 使用 aes
     */
    private boolean aes = false;

    /**
     * rsa 公钥
     */
    private String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALrL+Nlh+J7qBvID+8B0MtV/IvcFrnED0x3G/OfUZkSn/hRBlzu6Nx+AbvKRSbmbnHv5Y4XWCrYm4j+OuTbMLmUCAwEAAQ==";

    /**
     * rsa 私钥
     */
    private String privateKey = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAusv42WH4nuoG8gP7wHQy1X8i9wWucQPTHcb859RmRKf+FEGXO7o3H4Bu8pFJuZuce/ljhdYKtibiP465NswuZQIDAQABAkEAlmqLT1jx391pjFai0Z2AFaUT3cBocL74sIC8xF2noF4aLrwrIQ/qH7J/DRNFl1VGLhLEvKBobMsgslY9ZwBpYQIhANuO251oq2IRkZGvWM3quUMgJFUX9RL3B/e3SFgG2En9AiEA2c0R1OmOdxBieAji3tym4OrRzqCdRHau99EeCRF/zokCIFPImzPhSm1dCPDQOhIyPUpJTLQQNVxsWmUEUx4MVqM9AiEAy61Jeqk7UD7jRstQlgcUjAssTyXGTGsMrrVBeEuEELkCIFJj1ZVPCycKayJFo/CrLouBVzLn8LCuccvIoIQFH/zX";

    /**
     * aes 盐值
     */
    private String aesSalt = "syK^e#_%K&z(FmVfvtg9qsvs$TZ%(Oiq";

    /**
     * 是否启用
     */
    private Boolean enabled = false;

    public static void main(String[] args) {
        System.out.println(NanoIdUtils.randomNaoId(32));
    }

    /**
     * 查看是否需要加解密
     *
     * @return {@link Boolean} 任意一个为 true
     */
    public boolean isUsingEncrypt() {
        return this.rsa || this.aes;
    }
}
