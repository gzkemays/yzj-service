package com.yeziji.pay.config.wechat;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONWriter;
import com.yeziji.pay.common.PayFileReader;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信支付配置
 *
 * @author hwy
 * @since 2023/12/18 16:22
 **/
@Data
@ConfigurationProperties(prefix = "yzj.pay.wechat.file")
public class WechatPayConfig {
    /**
     * 支持读取文件的形式加载配置
     */
    private String configPath;

    /**
     * 读取 apiclient_key.pem
     */
    private PayFileReader keyFile = new PayFileReader();

    /**
     * 读取 apiclient_cert.pem
     */
    private PayFileReader certFile = new PayFileReader();

    /**
     * 读取 apiclient_cert.p12
     */
    private PayFileReader certP12File = new PayFileReader();

    /**
     * 读取 platform_cert.pem
     * <p>如果为空时每次都会自动续签自生成证书</p>
     */
    private PayFileReader platformCertFile = new PayFileReader();

    public static void main(String[] args) {
        WechatPayConfig wechatPayConfig = new WechatPayConfig();
        wechatPayConfig.setKeyFile(new PayFileReader("aaa", "bbb"));
        System.out.println("wechatPayConfig = " + wechatPayConfig);
        String jsonString = JSONObject.toJSONString(wechatPayConfig, JSONWriter.Feature.IgnoreNonFieldGetter);
        System.out.println("jsonString = " + jsonString);
        WechatPayConfig wechatPayConfig1 = JSONObject.parseObject(jsonString, WechatPayConfig.class);
        System.out.println("wechatPayConfig1 = " + wechatPayConfig1);
    }
}
