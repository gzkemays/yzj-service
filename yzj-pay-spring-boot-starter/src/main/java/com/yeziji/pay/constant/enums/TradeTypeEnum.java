package com.yeziji.pay.constant.enums;

import com.yeziji.common.CommonEnum;
import com.yeziji.utils.expansion.Lists2;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 支付类型枚举
 *
 * @author hwy
 * @since 2023/09/08 23:00
 **/
@Getter
@AllArgsConstructor
public enum TradeTypeEnum implements CommonEnum {
    WECHAT(0, "微信支付"),
    ALIPAY(1, "支付宝"),
    ;
    private final int code;
    private final String desc;

    public static Integer getTradeByCode(final int code) {
        return Lists2.filterFirstOpt(values(), (typeEnum) -> typeEnum.getCode() == code)
                .map(TradeTypeEnum::getCode)
                .orElse(null);
    }

    public static String getMsgByCode(final Integer code) {
        return Lists2.filterFirstOpt(values(), (typeEnum) -> typeEnum.getCode() == code)
                .map(TradeTypeEnum::getDesc)
                .orElse("未知类型");
    }
}
