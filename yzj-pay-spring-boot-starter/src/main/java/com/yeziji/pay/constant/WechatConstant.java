package com.yeziji.pay.constant;

/**
 * 微信常量值别名
 *
 * @author hwy
 * @since 2023/09/08 20:19
 **/
public interface WechatConstant {
    String APP_ID = "appid";
    String SECRET = "secret";
    String GRANT_TYPE = "grant_type";
    String JS_CODE = "js_code";
    String AUTHORIZATION_CODE = "authorization_code";
    String ACCESS_TOKEN = "access_token";
    String CLIENT_CREDENTIAL = "client_credential";
    String MCH_ID = "mchid";
    String OPEN_ID = "openid";
    String WECHAT_PAY_SERIAL = "Wechatpay-Serial";
    String ORIGINAL_TYPE = "original_type";
    String ENCRYPT_CERTIFICATE = "encrypt_certificate";
    String DATA = "data";
    String ASSOCIATED_DATA = "associated_data";
    String PREPAY_ID = "prepay_id";

    //---- 支付回调响应头部信息
    String PAY_TIMESTAMP = "Wechatpay-Timestamp";
    String PAY_NONCE = "Wechatpay-Nonce";
    String PAY_SERIAL = "Wechatpay-Serial";
    String PAY_SIGNATURE = "Wechatpay-Signature";

    //----- 支付回调解密报文
    String OUT_TRADE_NO = "out_trade_no";
    String TRANSACTION_ID = "transaction_id";
    String TRADE_TYPE = "trade_type";
    String TRADE_STATE = "trade_state";
    String TRADE_STATE_DESC = "trade_state_desc";
    String BANK_TYPE = "bank_type";
    String SUCCESS_TIME = "success_time";
    String PAYER_TOTAL = "payer_total";
    String PAYER_CURRENCY = "payer_currency";

    //----- 订单查询报文
    String RETURN_CODE = "return_code";
    String RETURN_MSG = "return_msg";
    String APPID = "appid";
    String SUB_APPID = "sub_appid";
    String SUB_MCH_ID = "sub_mch_id";
    String NONCE_STR = "nonce_str";
    String SIGN = "sign";
    String RESULT_CODE = "result_code";
    String OPENID = "openid";
    String IS_SUBSCRIBE = "is_subscribe";
    String SUB_OPENID = "sub_openid";
    String TOTAL_FEE = "total_fee";
    String FEE_TYPE = "fee_type";
    String CASH_FEE = "cash_fee";
    String CASH_FEE_TYPE = "cash_fee_type";
    String COUPON_ID_LIST = "coupon_id_list";
    String COUPON_TYPE_LIST = "coupon_type_list";
    String COUPON_FEE_LIST = "coupon_fee_list";
    String TIME_END = "time_end";

    //----- 订单状态
    String SUCCESS = "SUCCESS";
    String REFUND = "REFUND";
    String CLOSED = "CLOSED";

    // ---- 退款响应
    String REFUND_ID = "refund_id";
    String STATUS = "status";
    String USER_RECEIVED_ACCOUNT = "user_received_account";
    String CHANNEL = "channel";
    String OUT_REFUND_NO = "out_refund_no";
    String REFUND_STATUS = "refund_status";
}
