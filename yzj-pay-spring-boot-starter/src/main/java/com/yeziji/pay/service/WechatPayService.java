package com.yeziji.pay.service;

import com.yeziji.pay.common.PayService;

/**
 * 微信支付通用接口，在 PayService 的基础上扩展获取的方法
 *
 * @author hwy
 * @since 2023/12/18 15:11
 **/
public interface WechatPayService extends PayService {

}
