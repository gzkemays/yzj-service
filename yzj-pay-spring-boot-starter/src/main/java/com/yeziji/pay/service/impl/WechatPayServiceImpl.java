package com.yeziji.pay.service.impl;

import com.yeziji.pay.common.PayNotice;
import com.yeziji.pay.common.base.OrderBase;
import com.yeziji.pay.service.WechatPayService;

import java.util.Map;

/**
 * 微信支付实现类
 *
 * @author hwy
 * @since 2023/12/18 15:12
 **/
public class WechatPayServiceImpl implements WechatPayService {
    @Override
    public Map<String, String> doPay(OrderBase orderBase) {
        return null;
    }

    @Override
    public Map<String, String> refund(OrderBase orderBase) {
        return null;
    }

    @Override
    public boolean close(Long orderId) {
        return false;
    }

    @Override
    public void exceptionHandler(OrderBase orderBase, String errorMsg) {

    }

    @Override
    public Map<String, String> callback(PayNotice request) {
        return null;
    }

    @Override
    public Map<String, String> refundCallback(PayNotice request) {
        return null;
    }

    @Override
    public Map<String, Object> retryConfirm(OrderBase orderBase) {
        return null;
    }
}
